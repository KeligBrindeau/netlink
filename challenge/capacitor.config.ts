import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.netlink.com',
  appName: 'Netlink',
  webDir: 'dist/apps/client',
  server: {
    androidScheme: 'https'
  }
};

export default config;
