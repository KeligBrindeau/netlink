import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
} from 'typeorm';
  
  
@Entity('analytics_visit')
export class VisitEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: String
    })
    page: string;

    @Column({
        type: 'integer'
    })
    visitDuration: number;


}
  