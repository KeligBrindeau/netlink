import { Controller, Get } from '@nestjs/common';

import { AnalyticsService } from './analytics.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { ClickDto, MouseEventDto, OsDto, SurveyDto, VisitDto } from '@challenge/analytics-kpi';

@Controller()
export class AnalyticsController {
  constructor(private readonly analyticsService: AnalyticsService) {}

  @EventPattern('mouseEvent')
  createUser(@Payload() mouseEventDto : MouseEventDto){
    return this.analyticsService.handleMouseEvent(mouseEventDto);
  }

  @EventPattern('getHeatmapData')
  getHeatmapData(){
    return this.analyticsService.getHeatmapData();
  }

  @EventPattern('saveClick')
  saveClick(@Payload() clickDto: ClickDto){
    return this.analyticsService.saveClick(clickDto);
  }

  @EventPattern('getClick')
  getClick(){
    return this.analyticsService.getClick();
  }

  @EventPattern('saveOs')
  saveOs(@Payload() osDto: OsDto){
    return this.analyticsService.saveOs(osDto);
  }

  @EventPattern('getOs')
  getOs(){
    return this.analyticsService.getOs();
  }

  @EventPattern('saveVisit')
  saveVisit(@Payload() visitDto: VisitDto){
    return this.analyticsService.saveVisit(visitDto);
  }

  @EventPattern('getVisit')
  getVisit(){
    return this.analyticsService.getVisit();
  }

  @EventPattern('saveSurvey')
  saveSurvey(@Payload() surveyDto: SurveyDto){
    return this.analyticsService.saveSurvey(surveyDto);
  }

  @EventPattern('getSurvey')
  getSurvey(){
    return this.analyticsService.getSurvey();
  }
}
