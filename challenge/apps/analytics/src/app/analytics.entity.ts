import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
} from 'typeorm';
  
  
@Entity('analytics_MouseEvents')
export class AnalyticsEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'integer'
    })
    start_x: number;

    @Column({
        type: 'integer'
    })
    start_y: number;

    @Column({
        type: 'integer'
    })
    end_x: number;

    @Column({
        type: 'integer'
    })
    end_y: number;

    @Column({
        type: String,
        nullable: true
    })
    type: string;

    @Column({
        type: 'float'
    })
    duration: number;

    @CreateDateColumn()
    createdOn: Date;
}
  