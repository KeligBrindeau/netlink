import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
} from 'typeorm';
  
  
@Entity('analytics_survey')
export class SurveyEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'integer'
    })
    rate: number;
}