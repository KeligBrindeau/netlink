import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
} from 'typeorm';
  
  
@Entity('analytics_click')
export class ClickEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: String
    })
    page: string;

    @Column({
        type: String
    })
    element: string;

    @CreateDateColumn()
    createdOn: Date;
}
  