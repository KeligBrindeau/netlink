import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnalyticsEntity } from './analytics.entity';
import { AnalyticsController } from './analytics.controller';
import { AnalyticsService } from './analytics.service';
import { ClickEntity } from './click.entity';
import { OsEntity } from './os.entity';
import { VisitEntity } from './visit.entity';
import { SurveyEntity } from './survey.entity';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: "postgres", //process.env.DB_HOST,
      username: "postgres", //process.env.DB_USERNAME,
      password: "postgres", // process.env.DB_PASSWORD,
      synchronize: true,
      logging: true,
      database: "analytics", //process.env.API_ANALYTICS_DB_NAME,
      port: 5432,
      entities: [AnalyticsEntity, ClickEntity, OsEntity, VisitEntity, SurveyEntity]
    }
  ),
  TypeOrmModule.forFeature([AnalyticsEntity]),
  TypeOrmModule.forFeature([ClickEntity]),
  TypeOrmModule.forFeature([OsEntity]),
  TypeOrmModule.forFeature([VisitEntity]),
  TypeOrmModule.forFeature([SurveyEntity]),
],
  controllers: [AppController, AnalyticsController],
  providers: [AppService, AnalyticsService],
})
export class AppModule {}
