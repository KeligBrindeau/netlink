import { ClickDto, MouseEventDto, OsDto, SurveyDto, VisitDto } from '@challenge/analytics-kpi';
import { Injectable } from '@nestjs/common';
import { AnalyticsEntity } from './analytics.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ClickEntity } from './click.entity';
import { OsEntity } from './os.entity';
import { VisitEntity } from './visit.entity';
import { SurveyEntity } from './survey.entity';

@Injectable()
export class AnalyticsService {

    constructor(
        @InjectRepository(AnalyticsEntity)
        private analyticsRepository: Repository<AnalyticsEntity>,

        @InjectRepository(ClickEntity)
        private clickRepository: Repository<ClickEntity>,

        @InjectRepository(OsEntity)
        private osRepository: Repository<OsEntity>,

        @InjectRepository(VisitEntity)
        private visitRepository: Repository<VisitEntity>,

        @InjectRepository(SurveyEntity)
        private surveyRepository: Repository<SurveyEntity>,
        
    ){}
  
    handleMouseEvent(mouseEventDto: MouseEventDto){
        this.analyticsRepository.insert(mouseEventDto);
    }

    getHeatmapData(){
        return this.analyticsRepository.find();
    }

    saveClick(clickDto: ClickDto){
        this.clickRepository.insert(clickDto);
    }

    getClick(){
        return this.clickRepository.find();
    }

    saveOs(osDto: OsDto){
        this.osRepository.insert(osDto);
    }

    getOs(){
        return this.osRepository.find();
    }

    saveVisit(visitDto: VisitDto){
        return this.visitRepository.insert(visitDto);
    }

    getVisit(){
        return this.visitRepository.find();
    }

    saveSurvey(surveyDto: SurveyDto){
        return this.surveyRepository.insert(surveyDto);
    }

    getSurvey(){
        return this.surveyRepository.find();
    }
}
