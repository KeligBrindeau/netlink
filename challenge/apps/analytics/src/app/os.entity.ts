import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
} from 'typeorm';
  
  
@Entity('analytics_os')
export class OsEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: String
    })
    osType: string;

    @CreateDateColumn()
    createdOn: Date;
}
  