import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KanbanEntity } from './kanban.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { UserEntity } from './user.entity';
import { AssignmentEntity } from './assignment.entity';


@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host:  "postgres",
      username: "postgres",
      password: "postgres",
      synchronize: true,
      logging: true,
      database: "kanban",
      port: 5432,
      entities: [KanbanEntity, UserEntity, AssignmentEntity]
    }
  ),
  TypeOrmModule.forFeature([KanbanEntity, UserEntity, AssignmentEntity]),
    ClientsModule.register([
      {
        name: 'KANBAN',
        transport: Transport.TCP,
        options: {
          port: 2007,
          host: 'kanban'
        },
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
