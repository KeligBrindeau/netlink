import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    ManyToOne,
} from 'typeorm';
import { AssignmentEntity } from './assignment.entity';
  
  
@Entity('kanban_tickets')
export class KanbanEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'text',
        nullable: false
    })
    title: string;

    @Column({
        type: 'text',
        nullable: false
    })
    description: string;

    @Column({
        type: 'text',
        nullable: false
    })
    statut: string;

    @ManyToOne(
        type => AssignmentEntity,
        assignment => assignment.id,
    )
    assignment: AssignmentEntity;

}


//FAIRE LA LIAISON AVEC LES MISSIONS DANS UN DEUXIEME TEMPS SI LA CREATION DE TICKET ETC FONCTIONNE

