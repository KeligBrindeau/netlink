import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { KanbanEntity } from './kanban.entity';
import { Repository } from 'typeorm';
import { KanbanDto } from '@challenge/kanban-project';
import { CreateAssignmentDto } from '@challenge/assignment';
import { AssignmentEntity } from './assignment.entity';

@Injectable()
export class AppService {

  constructor(
    @InjectRepository(KanbanEntity)
    private kanbanRepository: Repository<KanbanEntity>,
    
    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,
  ) {}
  
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  async getTickets(id: string){
    const tickets = await this.kanbanRepository.createQueryBuilder('kanban')
    .select()
    .where('kanban.assignment.id = :id', {id})
    .getMany();
    return tickets;
  }

  saveTicket(kanbanDto: KanbanDto){
    return this.kanbanRepository.insert(kanbanDto);
  }

  deleteTicket(id: string){
    return this.kanbanRepository.delete(id);
  }

  async updateTicket(id: string, kanbanDto: KanbanDto){
    const ticket = await this.kanbanRepository.findOne({where: { id } });

    if(!ticket){
      throw new NotFoundException('Ticket not found');
    }
    ticket.statut = kanbanDto.statut;
    return this.kanbanRepository.save(ticket);
  }

  createAssignment(createAssignmentDto: CreateAssignmentDto){
    createAssignmentDto.isRefund = false;
    this.assignmentRepository.insert(createAssignmentDto);
    return null;
  }
}
