import { Controller, Get } from '@nestjs/common';
import { KanbanDto } from '@challenge/kanban-project';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateAssignmentDto } from '@challenge/assignment';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('getTickets')
  handleUserCreated(@Payload() id: string){
    return this.appService.getTickets(id);
  }

  @EventPattern('saveTicket')
  saveTicket(@Payload() kanbanDto : KanbanDto){
    return this.appService.saveTicket(kanbanDto);
  }

  @EventPattern('deleteTicket')
  deleteTicket(@Payload() id: string){
    return this.appService.deleteTicket(id);
  }

  @EventPattern('updateTicket')
  updateTicket(data: {id: string, kanbanDto: KanbanDto}){
    const { id, kanbanDto} = data;
    return this.appService.updateTicket(id, kanbanDto);
  }

  @EventPattern('kanban_create_assignment')
  createAssignment(@Payload() createAssignmentDto: CreateAssignmentDto){
    return this.appService.createAssignment(createAssignmentDto);
  }
}
