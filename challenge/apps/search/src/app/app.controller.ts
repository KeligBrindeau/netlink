import { Controller, Get, UseInterceptors} from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { RemovePasswordInterceptor } from './interceptors/removePassword.interceptor';
import { CreateUserFreelanceDto } from '@challenge/user';
import { UpdateClientDto } from '@challenge/user';
import { UpdateFreelanceDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('search_create_user_client')
  handleUserCreated(@Payload() createUserDTO : CreateUserClientDto){
    this.appService.signup(createUserDTO);
  }

  @EventPattern('search_create_user_freelance')
  handleUserFreelanceCreated(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('search_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }

  @EventPattern('search_update_user')
  updateUser(@Payload() updateUserDto : UpdateClientDto){
    return this.appService.updateUser(updateUserDto);
  }

  @EventPattern('search_update_user_freelance')
  updateUserFreelance(@Payload() updateUserDto : UpdateFreelanceDto){
    return this.appService.updateFreelance(updateUserDto);
  }

  @EventPattern('search_upload_img')
  async uploadImgTest(@Payload()  payload: { file, id: string }){
    const { file, id } = payload;
    return this.appService.uploadImgBase64(file, id);
  }

  //get all users
  @EventPattern('search_get_users')
  @UseInterceptors(RemovePasswordInterceptor)
  handleGetUsers(){
    return this.appService.getFreelances();
  }

  //get user by id
  @EventPattern('search_get_user')
  @UseInterceptors(RemovePasswordInterceptor)
  handleGetUser(@Payload() id: string){
    return this.appService.getUser(id);
  }
}
