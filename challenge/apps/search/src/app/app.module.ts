import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryEntity } from './category.entity';
import { UserEntity } from './user.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: "postgres", // process.env.DB_HOST
      username: "postgres", // process.env.DB_USERNAME
      password: "postgres", // process.env.DB_PASSWORD
      synchronize: true,
      logging: true,
      database: "search",
      port: 5432,
      entities: [UserEntity, CategoryEntity]
    }
  ), 
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([CategoryEntity]),
  ClientsModule.register([
    {
      name: 'SEARCH',
      transport: Transport.TCP,
      options: {
        port: 2003,
        host: 'search'
      },
    },
  ]),
],
  controllers: [AppController, SearchController],
  providers: [AppService, SearchService],
})
export class AppModule {}
