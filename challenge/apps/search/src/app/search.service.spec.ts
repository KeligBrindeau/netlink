import { Test } from '@nestjs/testing';

import { AppService } from './app.service';

import { SearchService } from './search.service'
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { CategoryEntity } from './category.entity';
import { UserRepositoryMock } from './user.repository.mock';



describe('SearchService', () => {
    let service: SearchService;
    let userRepository: Repository<UserEntity>;


    beforeAll(async () => {
        const app = await Test.createTestingModule({
          providers: [
            SearchService,
            {
              provide: getRepositoryToken(UserEntity),
              useClass: Repository,
            //   useValue: {
            //     insert: jest.fn(),
            //   },
            }
          ],
        }).compile();
    
        service = app.get<SearchService>(SearchService);
        userRepository = app.get<Repository<UserEntity>>(
          getRepositoryToken(UserEntity)
        );
    
    });

    describe('search', () => {

        const mockUsers: UserEntity[] = [
            Object.assign(new UserEntity(), {
              id: "0544593f-5c51-453f-9a7d-d7d5ab0c7743",
              email: 'Orlando_Rosenbaum@hotmail.com',
              lastName: 'Rosenbaum',
              firstName: 'Orlando',
            }),
            Object.assign(new UserEntity(), {
              id: "13a77bb1-9fda-4af7-8c98-1600ea5da186",
              email: 'Lauryn43@hotmail.com',
              lastName: 'Yundt',
              firstName: 'Lauryn',
            }),
        ];

        it('should search by skill', async () => {
            const user1 = new UserEntity();
            user1.skills = ['javascript', 'react'];
            user1.city = 'paris';
            user1.job = 'front-end';

            const user2 = new UserEntity();
            user2.skills = ['nodejs', 'express'];
            user2.city = 'lyon';
            user2.job = 'back-end';

            const userRepositoryFindQueryBuilderSpy = jest
            .spyOn(userRepository, 'createQueryBuilder')
            .mockReturnValueOnce({
            andWhere: jest.fn().mockReturnThis(),
            orWhere: jest.fn().mockReturnThis(),
            setParameter: jest.fn().mockReturnThis(),
            getMany: jest.fn().mockImplementation(() => Promise.resolve([user1])),
            } as any);

            const searchData = 'javascript';
            const results = await service.search(searchData);

            expect(userRepositoryFindQueryBuilderSpy).toHaveBeenCalledTimes(1);
            expect(results).toEqual([user1]);

            expect(results).not.toContain(user2);
           
        })

        
    })
})