import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { SearchService } from './search.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { RemovePasswordInterceptor } from './interceptors/removePassword.interceptor';

@Controller()
export class SearchController {
  constructor(private readonly searchService: SearchService) {}

  @EventPattern('search')
  @UseInterceptors(RemovePasswordInterceptor)
  searchUsers(@Payload() data : string){
    return this.searchService.search(data);
  }

}
