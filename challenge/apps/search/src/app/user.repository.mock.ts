import { SelectQueryBuilder } from "typeorm";
import { Test } from '@nestjs/testing';

export class UserRepositoryMock {

    createQueryBuilder(alias?: string): any {
        const queryBuilderMock: any = {};
    
        queryBuilderMock.where = () => queryBuilderMock;
        queryBuilderMock.andWhere = () => queryBuilderMock;
        queryBuilderMock.orWhere = () => queryBuilderMock;
        queryBuilderMock.getMany = () => this.find();
        queryBuilderMock.setParameter = () => queryBuilderMock;
    
        return queryBuilderMock;
    }

    async find(): Promise<any[]> {
      const mockData = [
        {
            id: 1,
            name: 'John Doe',
            skills: ['javascript', 'react'],
            city: 'paris',
            job: 'front-end',
        },
        {
            id: 2,
            name: 'Jane Dupont',
            skills: ['java', 'html'],
            city: 'orlean',
            job: 'fullstack',
        },
      ];
  
      return mockData;
    }
}