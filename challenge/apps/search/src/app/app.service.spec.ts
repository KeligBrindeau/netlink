import { Test } from '@nestjs/testing';

import { AppService } from './app.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { CategoryEntity } from './category.entity';
import * as request from 'supertest';
import { CreateUserClientDto, CreateUserFreelanceDto } from '@challenge/user';

describe('AppService', () => {
  let service: AppService;
  let userRepository: Repository<UserEntity>;
  let categoryRepository: Repository<CategoryEntity>;

  const mockUsers: UserEntity[] = [
    Object.assign(new UserEntity(), {
      id: "0544593f-5c51-453f-9a7d-d7d5ab0c7743",
      email: 'Orlando_Rosenbaum@hotmail.com',
      lastName: 'Rosenbaum',
      firstName: 'Orlando',
    }),
    Object.assign(new UserEntity(), {
      id: "13a77bb1-9fda-4af7-8c98-1600ea5da186",
      email: 'Lauryn43@hotmail.com',
      lastName: 'Yundt',
      firstName: 'Lauryn',
    }),
  ];

  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [
        AppService,
        {
          provide: getRepositoryToken(UserEntity),
          useClass: Repository,
          useValue: {
            insert: jest.fn(),
          },
        },
        {
          provide: getRepositoryToken(CategoryEntity),
          useClass: Repository,
        },
      ],
    }).compile();

    service = app.get<AppService>(AppService);
    userRepository = app.get<Repository<UserEntity>>(
      getRepositoryToken(UserEntity)
    );

  });

  // describe('getData', () => {
  //   it('should return "Hello API"', () => {
  //     expect(service.getData()).toEqual({ message: 'Hello API' });
  //   });
  // });

  // describe('createUser', () => {
  //   it('should return status 201', () => {
  //     const createUserDto = {
  //       email: "test@gmail.com"
  //     }

  //     const insertMock = jest.fn();
  //     userRepository.insert = insertMock;

  //     const response = service.createUser(createUserDto);

  //     expect(insertMock).toHaveBeenCalledWith(createUserDto);
  //     expect(response).toBe(201);
  //   });
  // });

  describe('getUsers', () => {
    it('should return users', async () => {

      const findMock = jest.spyOn(userRepository, 'find').mockResolvedValue(mockUsers);

      const response = await service.getUsers();

      expect(findMock).toHaveBeenCalled();
      expect(response).toEqual(mockUsers);
    })
  })

  describe('getUserById', () => {
    it('should get only one user', async () => {

      const expectedResponse: UserEntity =
        Object.assign(new UserEntity(), {
          id: "13a77bb1-9fda-4af7-8c98-1600ea5da186",
          email: 'Lauryn43@hotmail.com',
          lastName: 'Yundt',
          firstName: 'Lauryn',
        })

      const id = "13a77bb1-9fda-4af7-8c98-1600ea5da186";

      jest.spyOn(userRepository, 'findOne').mockResolvedValue(expectedResponse);

      return await service.getUser(id).then(result => {
        expect(result.id).toEqual(id);
      });

    })

    it('should not find user with wrong id', async () => {
      const id = "13a77bb1-9fda-4af7-8c98-1600ea5freg";

      const expectedResponse: UserEntity =
      Object.assign(new UserEntity(), {
        id: "13a77bb1-9fda-4af7-8c98-1600ea5da186",
        email: 'Lauryn43@hotmail.com',
        lastName: 'Yundt',
        firstName: 'Lauryn',
        // skills: ["php", "html", "css"]
      })

      jest.spyOn(userRepository, 'findOne').mockResolvedValue(expectedResponse);

      return await service.getUser(id).then(result => {
        expect(result.id).not.toEqual(id);
      });

    })

  })

  describe('signup', () => {
    let service: AppService;
    let userRepositoryMock: Repository<UserEntity> & {
      create: jest.Mock<any, any>;
      insert: jest.Mock<any, any>;
    };
    let categoryRepositoryMock: Repository<CategoryEntity> & {
      create: jest.Mock<any, any>;
      insert: jest.Mock<any, any>;
    };

    beforeEach( async () => {
      userRepositoryMock = {
        create: jest.fn(),
        insert: jest.fn(),
        delete: jest.fn(),

      } as unknown as Repository<UserEntity> & {
        create: jest.Mock<any, any>;
        insert: jest.Mock<any, any>;
        delete: jest.Mock<any, any>,
      };

      categoryRepositoryMock = {
        create: jest.fn(),
        insert: jest.fn(),
        delete: jest.fn(),
      } as unknown as Repository<CategoryEntity> & {
        create: jest.Mock<any, any>;
        insert: jest.Mock<any, any>;
        delete: jest.Mock<any, any>,
      };
  
      service = new AppService(userRepositoryMock, categoryRepositoryMock);

      await userRepositoryMock.delete({});
    });

    it('should create a new client', async () => {
      const createUserDTO: CreateUserClientDto = {
        lastName: 'Yundt',
        firstName: 'Lauryn',
        password: 'password123',
        email: 'Lauryn43@hotmail.com',
        isConfirmed: false,
        id: '13a77bb1-9fda-4af7-8c98-1600ea5freg',
      };

      await service.signup(createUserDTO);

      const findOneOptions: FindOneOptions<UserEntity> = {
        where: { firstName: 'Lauryn' }
      };

      const newUser = await userRepository.findOne(findOneOptions);

      expect(newUser).toBeDefined();
      expect(newUser.firstName).toBe('Lauryn');
      expect(newUser.email).toBe('Lauryn43@hotmail.com');
      expect(newUser.isConfirmed).toBe(false);
    });

    it('should create new Freelance', async () => {
      const createUserFreelanceDTO: CreateUserFreelanceDto =  {
        lastName: 'Yundt',
        firstName: 'Lauryn',
        password: 'password123',
        email: 'Lauryn43@hotmail.com',
        isConfirmed: false,
        id: '13a77bb1-9fda-4af7-8c98-1600ea5freg',
        skills: ["php", "java", "html"],
        city: "paris", 
        hourlyRate: 12
      };

      await service.signupFreelance(createUserFreelanceDTO);

      const findOneOptions: FindOneOptions<UserEntity> = {
        where: { firstName: 'Lauryn' }
      };

      const newUser = await userRepository.findOne(findOneOptions);

      expect(newUser).toBeDefined();
      expect(newUser.firstName).toBe('Lauryn');
      expect(newUser.email).toBe('Lauryn43@hotmail.com');
      expect(newUser.isConfirmed).toBe(false);
    })
  });
});
