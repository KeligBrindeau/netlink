import { Injectable } from '@nestjs/common';
import { IsNull, Not, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { CreateUserClientDto } from '@challenge/user';
import { SeedService } from './seed';
import { CategoryEntity } from './category.entity';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';
import { UpdateClientDto } from '@challenge/user';
import { UpdateFreelanceDto } from '@challenge/user';
import fs from 'fs';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}

  signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ['CLIENT'], isConfirmed: false };
    return this.userRepository.insert(user);
  }


  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ['FREELANCE'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async updateUser(updateUserDto: UpdateClientDto) {
    try {
      const user = await this.userRepository.findOneBy({ id: updateUserDto.id });
      if (user) {
        if (updateUserDto.password) {
          const hashedPassword = bcrypt.hashSync(updateUserDto.password, 10);
          const updatedUser = {
            ...updateUserDto,
            password: hashedPassword
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        } else {
          const updatedUser = {
            ...updateUserDto
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        }
      }
    } catch (error) {
      console.error
    }
  }

  async updateFreelance(updateFreelanceDTO: UpdateFreelanceDto) {
    const user = await this.userRepository.findOneBy({ id: updateFreelanceDTO.id });
    if (user) {
      if (updateFreelanceDTO.password) {
        const hashedPassword = bcrypt.hashSync(updateFreelanceDTO.password, 10);
        const updatedUser = {
          ...updateFreelanceDTO,
          password: hashedPassword
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      } else {
        const updatedUser = {
          ...updateFreelanceDTO
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      }
    }
  } catch(error) {
    console.error
  }

  async uploadImgBase64(file: string, id: string) {
    try {
      
      return await this.userRepository
        .createQueryBuilder()
        .update(UserEntity)
        .set({ picture: file })
        .where('id = :id', { id })
        .execute();
  
    } catch (error) {
      console.error
    }
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  seedDatabase(){
    const obj = new SeedService(
      this.userRepository, 
      this.categoryRepository
    );
    obj.seedDatabase();
  }

  getUsers(){
    return this.userRepository.find();
  }

  getUser(id: string){
    return this.userRepository.findOne({where: {id: id}});
  }

  getFreelances(){
    return this.userRepository.find({where: {role: '{FREELANCE}', description: Not(IsNull()), isConfirmed: true, picture: Not(IsNull()) }});  }
}