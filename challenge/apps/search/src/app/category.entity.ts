import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToMany
  } from 'typeorm';
  import { UserEntity } from './user.entity';

  @Entity('search_category')
  export class CategoryEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
      type: String,
      length: 100
    })
    title: string;

    @ManyToMany(() => UserEntity, user => user.job)
    users: UserEntity[];


}
