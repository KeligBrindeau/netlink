import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserEntity } from './user.entity';
import { AssignmentEntity } from './assignment.entity';
import { QuotationEntity } from './quotation.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: "postgres", // process.env.DB_HOST
      username: "postgres", // process.env.DB_USERNAME
      password: "postgres", // process.env.DB_PASSWORD
      synchronize: true,
      logging: true,
      database: "payment", // process.env.API_PAYMENT_DB_NAME
      port: 5432,
      entities: [UserEntity, AssignmentEntity, QuotationEntity]
    }
  ),
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([AssignmentEntity]),
  TypeOrmModule.forFeature([QuotationEntity]),
  ClientsModule.register([
    {
      name: 'PAYMENT',
      transport: Transport.TCP,
      options: {
        port: 5000,
        host: 'payment'
      },
    },
  ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
