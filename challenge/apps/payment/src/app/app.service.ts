import { CreateUserClientDto } from '@challenge/user';
import { Injectable, NotFoundException } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Not, Repository } from 'typeorm';
import { CreateAssignmentDto, UpdateAssignmentDto, UpdateAssignmentRefundDto } from '@challenge/assignment';
import { AssignmentEntity } from './assignment.entity';
import { CreateQuotationDto, UpdateQuotationDto } from '@challenge/quotation';
import { QuotationEntity } from './quotation.entity';
import { SeedService } from './seed';
import { stripeConfig } from './stripe.config';
import { Stripe } from 'stripe';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';
import { UpdateClientDto } from '@challenge/user';
import { UpdateFreelanceDto } from '@challenge/user';
import fs from 'fs';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }
  private readonly stripe: Stripe;

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,

    @InjectRepository(QuotationEntity)
    private quotationRepository: Repository<QuotationEntity>,
  ) {
    this.stripe = new Stripe(stripeConfig.apiSecret, {
      apiVersion: '2022-11-15',
    });
  }

  signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ['CLIENT'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ['FREELANCE'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async updateUser(updateUserDto: UpdateClientDto) {
    try {
      const user = await this.userRepository.findOneBy({ id: updateUserDto.id });
      if (user) {
        if (updateUserDto.password) {
          const hashedPassword = bcrypt.hashSync(updateUserDto.password, 10);
          const updatedUser = {
            ...updateUserDto,
            password: hashedPassword
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        } else {
          const updatedUser = {
            ...updateUserDto
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        }
      }
    } catch (error) {
      console.error
    }
  }

  async updateFreelance(updateFreelanceDTO: UpdateFreelanceDto) {
    const user = await this.userRepository.findOneBy({ id: updateFreelanceDTO.id });
    if (user) {
      if (updateFreelanceDTO.password) {
        const hashedPassword = bcrypt.hashSync(updateFreelanceDTO.password, 10);
        const updatedUser = {
          ...updateFreelanceDTO,
          password: hashedPassword
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      } else {
        const updatedUser = {
          ...updateFreelanceDTO
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      }
    }
  } catch(error) {
    console.error
  }

  async uploadImgBase64(file: string, id: string) {
    try {
      
      return await this.userRepository
        .createQueryBuilder()
        .update(UserEntity)
        .set({ picture: file })
        .where('id = :id', { id })
        .execute();
  
    } catch (error) {
      console.error
    }
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  createAssignment(createAssignmentDto: CreateAssignmentDto) {
    createAssignmentDto.isRefund = false;
    this.assignmentRepository.insert(createAssignmentDto);
    return null;
  }


  createQuotation(createQuotationDto: CreateQuotationDto) {
    createQuotationDto.amount = createQuotationDto.amount *= 10;
    this.quotationRepository.insert(createQuotationDto);
    return null;
  }

  async updateAssignmentApprovePayment(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    const assignment = await this.assignmentRepository.findOne({ where: { id } });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    assignment.status = updateAssignmentDto.status
    return this.assignmentRepository.save(assignment);
  }

  async updateAssignmentRejectPayment(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    const assignment = await this.assignmentRepository.findOne({ where: { id } });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    assignment.status = updateAssignmentDto.status
    return this.assignmentRepository.save(assignment);
  }

  async getQuotationsPerCompany(id: string) {
    const quotations = await this.quotationRepository.createQueryBuilder('quotation')
      .select(['quotation.id', 'quotation.description', 'quotation.amount', 'quotation.nbDays', 'quotation.assignment', 'quotation.freelance', 'quotation.company', 'quotation.status'])
      .leftJoinAndSelect('quotation.freelance', 'freelance')
      .where('quotation.company.id = :id', { id })
      .andWhere('quotation.status IS NULL')
      .getMany();
    return quotations;
  }

  async updateQuotationApprovePayment(id: string, updateQuotationDto: UpdateQuotationDto) {
    const quotation = await this.quotationRepository.findOne({ where: { id } });
    if (!quotation) {
      throw new NotFoundException('Quotation not found');
    }
    quotation.status = updateQuotationDto.status
    return this.quotationRepository.save(quotation);
  }

  async updateQuotationRejectPayment(id: string, updateQuotationDto: UpdateQuotationDto) {
    const quotation = await this.quotationRepository.findOne({ where: { id } });
    if (!quotation) {
      throw new NotFoundException('Quotation not found');
    }
    quotation.status = updateQuotationDto.status
    return this.quotationRepository.save(quotation);
  }

  async getAcceptedQuotation(id: string) {
    const quotations = await this.quotationRepository.createQueryBuilder('quotation')
      .select(['quotation.id', 'quotation.description', 'quotation.amount', 'quotation.nbDays', 'quotation.assignment', 'quotation.company', 'quotation.freelance', 'quotation.status'])
      .leftJoinAndSelect('quotation.assignment', 'assignment')
      .leftJoinAndSelect('quotation.freelance', 'freelance')
      .leftJoinAndSelect('quotation.company', 'company')
      .where('(quotation.freelance.id = :id OR quotation.company.id = :id) AND quotation.status = :status', { id, status: true })
      .getMany();
    return quotations;
  }

  async getPaidAssignment(assignmentId: string) {
    const quotation = await this.quotationRepository.findOne({
      where: { assignment: { id: assignmentId } },
      relations: ["assignment"]
    });
    if (!quotation) {
      throw new NotFoundException('Quotation not found');
    }
    const assignments = await this.assignmentRepository.findOne({
      where: {
        id: quotation.assignment.id,
        updatedAt: Not(IsNull()),
        paymentIntent: Not(IsNull())
      }
    });
    return assignments;
  }

  async createPaymentIntent(id: string, amount: number, currency: string, paymentId: string) {
    const quotation = this.getQuuotationById(id);
    let amountQuotation = (await quotation).amount;
    amountQuotation = amountQuotation * 10;

    const paymentIntent = await this.stripe.paymentIntents.create({
      "amount": amountQuotation,
      currency,
      payment_method_types: ['card'],
      payment_method: paymentId,
      confirm: true,
    });

    const assignmentId = this.getAssignmentById((await quotation).assignment.id);
    this.patchAssignment((await assignmentId).id, paymentIntent.id);
    return paymentIntent;
  }

  async getQuuotationById(id: string) {
    const quotation = await this.quotationRepository.findOne({ where: { id: id }, relations: ["assignment", "company", "freelance"] });
    return quotation;
  }

  async getAssignmentById(id: string) {
    const assignment = await this.assignmentRepository.findOne({ where: { id: id } });
    return assignment;
  }

  async patchAssignment(id: string, paymentIntent: string) {
    const assignment = await this.assignmentRepository.findOne({ where: { id: id } });
    assignment.paymentIntent = paymentIntent;
    assignment.updatedAt = new Date();
    this.assignmentRepository.save(assignment);
    return assignment;
  }

  async refundPaymentIntent(paymentId: string) {

    const refund = await this.stripe.refunds.create({
      payment_intent: paymentId,
    });

    const assignment = await this.findAssignmentByPaymentIntent(paymentId);
    await this.patchAssignmentStatus(assignment.id);

    return refund;
  }

  async findAssignmentByPaymentIntent(paymentIntent: string) {
    const assignment = await this.assignmentRepository.findOne({ where: { paymentIntent: paymentIntent } });
    return assignment;
  }

  async patchAssignmentStatus(id: string) {
    const assignment = await this.assignmentRepository.findOne({ where: { id: id } });
    assignment.status = false;
    this.assignmentRepository.save(assignment);
    return assignment;
  }

  seedDatabase() {
    const obj = new SeedService(
      this.userRepository,
      this.assignmentRepository,
      this.quotationRepository
    );
    obj.seedDatabase();
  }

  async refundAssignment(id: string, updateAssignmentRefundDto: UpdateAssignmentRefundDto) {
    const assignment = await this.assignmentRepository.findOne({ where: { id } });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    assignment.isRefund = updateAssignmentRefundDto.isRefund;
    return this.assignmentRepository.save(assignment);
  }

}
