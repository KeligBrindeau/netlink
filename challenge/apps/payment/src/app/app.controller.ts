import { Body, Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { CreateAssignmentDto, UpdateAssignmentDto, UpdateAssignmentRefundDto } from '@challenge/assignment';
import { CreateQuotationDto, UpdateQuotationDto } from '@challenge/quotation';
import { CreateUserFreelanceDto } from '@challenge/user';
import { UpdateClientDto } from '@challenge/user';
import { UpdateFreelanceDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('payment_create_user_client')
  createUser(@Payload() createUserDTO: CreateUserClientDto) {
    return this.appService.signup(createUserDTO);
  }

  @EventPattern('payment_create_user_freelance')
  createUserFreelance(@Payload() createUserFreelanceDTO: CreateUserFreelanceDto) {
    return this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('payment_update_user')
  updateUser(@Payload() updateUserDto: UpdateClientDto) {
    return this.appService.updateUser(updateUserDto);
  }

  @EventPattern('payment_update_user_freelance')
  updateUserFreelance(@Payload() updateUserDto : UpdateFreelanceDto){
    return this.appService.updateFreelance(updateUserDto);
  }

  @EventPattern('payment_upload_img')
  async uploadImgTest(@Payload()  payload: { file, id: string }){
    const { file, id } = payload;
    return this.appService.uploadImgBase64(file, id);
  }

  @EventPattern('payment_user_confirmed')
  confirmUser(@Payload() id: string) {
    return this.appService.confirmUser(id);
  }

  @EventPattern('payment_create_assignment')
  createAssignment(@Payload() createAssignmentDto: CreateAssignmentDto) {
    return this.appService.createAssignment(createAssignmentDto);
  }

  @EventPattern('payment_create_quotation')
  createQuotation(@Payload() createQuotationDto: CreateQuotationDto) {
    return this.appService.createQuotation(createQuotationDto);
  }

  @EventPattern('payment_stripe_create_payment')
  async createPayment(@Payload() body: { id: string, amount: number, currency: string, paymentId: string }) {
    const { id, amount, currency, paymentId } = body;
    const paymentIntent = await this.appService.createPaymentIntent(id, amount, currency, paymentId);

    return { clientSecret: paymentIntent };
  }


  @EventPattern('payment_refund_payment_intent')
  async refundPaymentIntent(@Payload() body: { payment_intent: string }) {
    const { payment_intent } = body;
    const refund = await this.appService.refundPaymentIntent(payment_intent);

    return { refund };
  }

  @EventPattern('payment_get_quotation')
  async getQuotation(@Payload() body: { id: string }) {
    const { id } = body;
    const quotation = await this.appService.getQuuotationById(id);

    return { quotation };
  }

  @EventPattern('payment_update_approve_assignment')
  paymentUpdateApproveAssignment(data: { id: string; updateAssignmentDto: UpdateAssignmentDto }) {
    const { id, updateAssignmentDto } = data;
    return this.appService.updateAssignmentApprovePayment(id, updateAssignmentDto);
  }

  @EventPattern('payment_update_reject_assignment')
  paymentUpdateRejectAssignment(data: { id: string; updateAssignmentDto: UpdateAssignmentDto }) {
    const { id, updateAssignmentDto } = data;
    return this.appService.updateAssignmentRejectPayment(id, updateAssignmentDto);
  }

  @EventPattern('payment_get_quotation_per_company')
  getQuotationsPerCompany(@Payload() id: string) {
    return this.appService.getQuotationsPerCompany(id);
  }

  @EventPattern('payment_update_approve_quotation')
  paymentUpdateApproveQuotation(data: { id: string; updateQuotationDto: UpdateQuotationDto }) {
    const { id, updateQuotationDto } = data;
    return this.appService.updateQuotationApprovePayment(id, updateQuotationDto);
  }

  @EventPattern('payment_update_reject_quotation')
  paymentUpdateRejectQuotation(data: { id: string; updateQuotationDto: UpdateQuotationDto }) {
    const { id, updateQuotationDto } = data;
    return this.appService.updateQuotationRejectPayment(id, updateQuotationDto);
  }

  @EventPattern('payment_get_accepted_quotation')
  messagingListAcceptedQuotation(@Payload() id: string) {
    return this.appService.getAcceptedQuotation(id);
  }

  @EventPattern('payment_get_paid_assignment')
  getPaidAssignment(@Payload() id: string) {
    return this.appService.getPaidAssignment(id);
  }

  @EventPattern('payment_refund_assignment')
  paymentRefundAssignment(data: { id: string; updateAssignmentRefundDto: UpdateAssignmentRefundDto }) {
    const { id, updateAssignmentRefundDto } = data;
    return this.appService.refundAssignment(id, updateAssignmentRefundDto);
  }
}
