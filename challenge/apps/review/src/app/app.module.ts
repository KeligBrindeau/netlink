import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { ReviewEntity } from './review.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: "postgres", // process.env.DB_HOST
      username: "postgres", // process.env.DB_USERNAME
      password: "postgres", // process.env.DB_PASSWORD
      synchronize: true,
      logging: true,
      database: "review", // process.env.API_REVIEW_DB_NAME,
      port: 5432,
      entities: [UserEntity, ReviewEntity]
    }
  ),
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([ReviewEntity]),
  ClientsModule.register([
    {
      name: 'REVIEW',
      transport: Transport.TCP,
      options: {
        port: 2004,
        host: 'review'
      },
    },
  ]),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
