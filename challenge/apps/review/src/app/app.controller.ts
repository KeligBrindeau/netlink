import { Controller, Get,  UsePipes, UseFilters } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { CreateUserFreelanceDto } from '@challenge/user';
import { UpdateClientDto } from '@challenge/user';
import { UpdateFreelanceDto } from '@challenge/user';
import {CreateReviewFreelanceDto} from '@challenge/review-freelance';
import { RpcExceptionFilter } from './common/rpc-exception.filter';
import { CustomValidationPipe } from './common/CustomValidationPipe';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('review_create_user_client')
  createUser(@Payload() createUserDTO : CreateUserClientDto){
    return this.appService.signup(createUserDTO);
  }

  @EventPattern('review_create_user_freelance')
  createUserFreelance(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    return this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('review_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }

  @EventPattern('review_update_user')
  updateUser(@Payload() updateUserDto : UpdateClientDto){
    return this.appService.updateUser(updateUserDto);
  }

  @EventPattern('review_upload_img')
  async uploadImgTest(@Payload()  payload: { file, id: string }){
    const { file, id } = payload;
    return this.appService.uploadImgBase64(file, id);
  }

  @EventPattern('review_update_user_freelance')
  updateUserFreelance(@Payload() updateUserDto : UpdateFreelanceDto){
    return this.appService.updateFreelance(updateUserDto);
  }

  @EventPattern('create_review')
  @UsePipes(new CustomValidationPipe())
  @UseFilters(new RpcExceptionFilter())
  createReview(@Payload() createReview : CreateReviewFreelanceDto){
    return this.appService.createReview(createReview);
  }

  @EventPattern('get_reviews')
  getReviews(){
    return this.appService.getReviews();
  }

  @EventPattern('get_reviews_by_freelance')
  getReviewsByFreelance(@Payload() id: string){
    return this.appService.getReviewsByFreelance(id);
  }

  @EventPattern('delete_review')
  deleteReview(id: string){
    return this.appService.deleteReview(id);
  }

  @EventPattern('publish_review')
  publishReview(id: string){
    return this.appService.publishReview(id);
  }
}
