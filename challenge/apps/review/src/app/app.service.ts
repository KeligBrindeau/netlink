import { CreateUserClientDto } from '@challenge/user';
import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { ReviewEntity } from './review.entity';
import { SeedService } from './seed';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';
import { UpdateClientDto } from '@challenge/user';
import { UpdateFreelanceDto } from '@challenge/user';
import fs from 'fs';
import { CreateReviewFreelanceDto } from '@challenge/review-freelance';

@Injectable()
export class AppService {

  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(ReviewEntity)
    private reviewRepository: Repository<ReviewEntity>,
  ) { }

  signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ["CLIENT"], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ['FREELANCE'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async updateUser(updateUserDto: UpdateClientDto) {
    try {
      const user = await this.userRepository.findOneBy({ id: updateUserDto.id });
      if (user) {
        if (updateUserDto.password) {
          const hashedPassword = bcrypt.hashSync(updateUserDto.password, 10);
          const updatedUser = {
            ...updateUserDto,
            password: hashedPassword
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        } else {
          const updatedUser = {
            ...updateUserDto
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        }
      }
    } catch (error) {
      console.error
    }
  }

  async updateFreelance(updateFreelanceDTO: UpdateFreelanceDto) {
    const user = await this.userRepository.findOneBy({ id: updateFreelanceDTO.id });
    if (user) {
      if (updateFreelanceDTO.password) {
        const hashedPassword = bcrypt.hashSync(updateFreelanceDTO.password, 10);
        const updatedUser = {
          ...updateFreelanceDTO,
          password: hashedPassword
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      } else {
        const updatedUser = {
          ...updateFreelanceDTO
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      }
    }
  } catch(error) {
    console.error
  }

  async uploadImgBase64(file: string, id: string) {
    try {
      return await this.userRepository
        .createQueryBuilder()
        .update(UserEntity)
        .set({ picture: file })
        .where('id = :id', { id })
        .execute();

    } catch (error) {
      console.error
    }
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  async createReview(createReview: CreateReviewFreelanceDto) {
    let result = {
      status: HttpStatus.BAD_REQUEST,
      message: 'create_review_bad_request',
    };
    try {
      const review = {
        ...createReview,
        isDisplayed: false
      }
      await this.reviewRepository.insert(review);
      result = {
        status: HttpStatus.CREATED,
        message: 'Votre avis a bien été pris en compte !',
      };
    } catch (error) {
      result = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'create_review_bad_request',
      };
    }
    return result
  }

  async getReviews() {
    let result = {
      status: HttpStatus.NOT_FOUND,
      message: 'reviews_not_found',
      datas: null
    };
    try {
      const reviews = await this.reviewRepository.createQueryBuilder('reviews')
        .select(['reviews.id', 'reviews.description', 'reviews.rate', 'reviews.ratedUser', 'reviews.ratingUser'])
        .leftJoinAndSelect('reviews.ratedUser', 'freelance')
        .leftJoinAndSelect('reviews.ratingUser', 'client')
        .where('reviews.isDisplayed = false',)
        .getMany();
      result = {
        status: HttpStatus.CREATED,
        datas: reviews,
        message: null,
      };
    } catch (error) {
      result = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: null,
        datas: null
      };
    }
    return result
  }

  async getReviewsByFreelance(id: string) {
    let result = {
      status: HttpStatus.NOT_FOUND,
      message: 'reviews_not_found',
      datas: null
    };
    try {
      const reviews = await this.reviewRepository.createQueryBuilder('reviews')
        .select(['reviews.id', 'reviews.description', 'reviews.rate', 'reviews.ratedUser', 'reviews.ratingUser'])
        .leftJoinAndSelect('reviews.ratedUser', 'freelance')
        .leftJoinAndSelect('reviews.ratingUser', 'client')
        .where('reviews.ratedUser = :id', {id})
        .andWhere('reviews.isDisplayed = true')
        .getMany();
      result = {
        status: HttpStatus.OK,
        datas: reviews,
        message: null,
      };
    } catch (error) {
      result = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: null,
        datas: null
      };
    }
    return result
  }

  async deleteReview(id: string) {
    let result = {
      status: HttpStatus.BAD_REQUEST,
      message: 'delete_reviews_bad_request',
    };
    try {
      await this.reviewRepository.delete(id)
      result = {
        status: HttpStatus.OK,
        message: "Le commentaire a bien été supprimé",
      };
    } catch (error) {
      result = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: null,
      }
    }
    return result
  }

  async publishReview(id: string) {
    let result = {
      status: HttpStatus.BAD_REQUEST,
      message: 'publish_reviews_bad_request',
    };
    try {
      await this.reviewRepository.update(id, { isDisplayed: true })
      result = {
        status: HttpStatus.OK,
        message: "Le commentaire a bien été publié",
      };
    } catch (error) {
      result = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: null,
      }
    }
    return result
  }

  seedDatabase() {
    const obj = new SeedService(
      this.userRepository,
      this.reviewRepository,
    );
    obj.seedDatabase();
  }


}
