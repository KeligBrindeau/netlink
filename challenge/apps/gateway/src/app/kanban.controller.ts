import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { KanbanService } from './kanban.service';
import { KanbanDto } from '@challenge/kanban-project';


@Controller()
export class KanbanController {

    constructor(
        private readonly kanbanService: KanbanService,
    ) { }

    @Get("getTickets/:id")
    async getData(@Param('id') id: string) {
        return this.kanbanService.getTickets(id);
    }

    @Post("saveTicket")
    async saveTicket(@Body() kanbanDto : KanbanDto){
        return this.kanbanService.saveTicket(kanbanDto);
    }

    @Delete("deleteTicket/:id")
    async deleteTicket(@Param('id') id:string){
        return this.kanbanService.deleteTicket(id);
    }

    @Patch("updateTicket/:id")
    async updateTicket(@Param('id') id:string, @Body() kanbanDto: KanbanDto){
        return this.kanbanService.updateTicket(id, kanbanDto);
    }
}