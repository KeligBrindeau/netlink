import { SetMetadata } from '@nestjs/common';
import { IUserRoles } from '../interfaces/user.interface';

export const Permission = (
    roles: Array<keyof typeof IUserRoles>,
    areAuthorized = true,
) => SetMetadata('permission', { roles, areAuthorized });
