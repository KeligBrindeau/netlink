import { SetMetadata } from '@nestjs/common';
import { applyDecorators } from '@nestjs/common';

export const Authorization = (secured: boolean) => {
    return applyDecorators(
        SetMetadata('secured', secured)
    );
};