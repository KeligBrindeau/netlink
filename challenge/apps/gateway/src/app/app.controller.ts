import { Body, Controller, Get, Post, Options, InternalServerErrorException, Patch, Req, Param, UseInterceptors, UploadedFile, ValidationPipe, HttpException, HttpStatus, BadRequestException, ParseFilePipe, MaxFileSizeValidator, FileTypeValidator, UseGuards, Session, Headers, UnauthorizedException, Delete } from '@nestjs/common';
import { Request } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { AppService } from './app.service';
import { CreateUserClientDto, LoginUserDto, ConfirmUserDto, ConfirmUserResponseDto, CreateUserFreelanceDto, UpdateClientDto, UpdateFreelanceDto } from '@challenge/user';
import { CreateAssignmentDto, UpdateAssignmentDto, UpdateAssignmentRefundDto } from '@challenge/assignment';
import { CreateQuotationDto, UpdateQuotationDto } from '@challenge/quotation';
import { Authorization } from './decorators/authorization.decorator';
import { Permission } from './decorators/permission.decorator';
import { v4 as uuidv4 } from 'uuid';
import * as fs from 'fs';
import { randomBytes } from 'crypto';
import { RemovePasswordInterceptor } from './interceptors/removePassword.interceptor';
import { RemovePasswordInterceptorMessages } from './interceptors/removePasswordMessages.interceptor';
import { CreateMessageDto } from '@challenge/message';
import { diskStorage } from 'multer';
import { CreateReviewFreelanceDto } from '@challenge/review-freelance';
import { log } from 'console';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) { }

  private failedAttempts = 0;
  private readonly MAX_FAILED_ATTEMPTS = 2;
  private readonly DELAY_DURATION = 10000;
  private readonly MAX_STRING_LENGTH = 80;
  private readonly MIN_STRING_LENGTH = 2;
  private readonly MAX_FILE_STRING_LENGTH = 100;
  private readonly MIN_FILE_STRING_LENGTH = 3;

  private delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  generateToken(): string {
    const token = randomBytes(32).toString('hex');
    return token;
  }

  private isSubmissionFileUploadFailed(formData: any): boolean {
    if (
      !formData ||
      formData.length > this.MAX_FILE_STRING_LENGTH ||
      formData.length < this.MIN_FILE_STRING_LENGTH
    ) {
      return true;
    }

    return false;
  }

  private isSubmissionFailed(formData: any): boolean {
    if (
      !formData ||
      formData.length > this.MAX_STRING_LENGTH ||
      formData.length < this.MIN_STRING_LENGTH
    ) {
      return true;
    }

    return false;
  }

  @Post("register-client")
  createUserEvent(@Body() createUserDto: CreateUserClientDto) {
    const user = {
      ...createUserDto,
      id: uuidv4()
    };
    return this.appService.createUserClient(user);
  }

  @Post("register-freelance")
  createUserFreelanceEvent(@Body() createUserFreelanceDto: CreateUserFreelanceDto) {
    const user = {
      ...createUserFreelanceDto,
      id: uuidv4()
    };
    return this.appService.createUserFreelance(user);
  }

  @Get('token')
  @Authorization(true)
  getToken(@Session() session): { token: string } {
    const token = this.generateToken();
    return { token };
  }

  @Get('me/:id')
  @Authorization(true)
  getMe(@Param('id') id: string, @Req() request) {
    return this.appService.getMe(id, request);
  }

  @Patch("update-user")
  @Authorization(true)
  @Permission(['CLIENT', 'ADMIN'])
  updateUserEvent(@Body() updateUserDto: UpdateClientDto,  @Req() request) {
    return this.appService.updateUser(updateUserDto, request);
  }

  @Patch("update-user-freelance")
  @Authorization(true)
  @Permission(['FREELANCE'])
  updateUserFreelanceEvent(@Body() updateUserFreelanceDto: UpdateFreelanceDto, @Req() request) {
    return this.appService.updateUserFreelance(updateUserFreelanceDto, request);
  }

  @Post("upload-img")
  @Authorization(true)
  @UseInterceptors(FileInterceptor('file'))
  async uploadImgBase64(
    @Headers('x-csrf-token') submittedCsrfToken: string,
    @Req() request: Request,
    @UploadedFile(new ParseFilePipe({
      validators: [
        new MaxFileSizeValidator({ maxSize: 2000000 }),
        new FileTypeValidator({ fileType: 'image/*' }),
      ],
    })) file: Express.Multer.File,
    @Body('id') id: string,
  ) {
    const contents = fs.readFileSync(file.path, { encoding: 'base64' });
    return this.appService.uploadImgBase64(contents, id, request);
  }

  @Get('/users/confirm/:link')
  public async confirmUser(
    @Param() params: ConfirmUserDto,
  ): Promise<ConfirmUserResponseDto> {
    return this.appService.confirmUser(params);
  }

  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto) {
    return this.appService.loginUser(loginUserDto);
  }

  @Post('logout')
  @Authorization(true)
  public async logout(@Req() request): Promise<void> {
    return this.appService.logout(request);

  }

  @Post("create-assignment")
  @Authorization(true)
  @Permission(['CLIENT'])
  @UseInterceptors(FileInterceptor('file'))
  async createAssignment(@Headers('x-csrf-token') submittedCsrfToken: string, @Req() request: Request, @Body(ValidationPipe) createAssignmentDto: CreateAssignmentDto, @UploadedFile(new ParseFilePipe({
    validators: [
      new MaxFileSizeValidator({ maxSize: 2000000 }),
      new FileTypeValidator({ fileType: 'application/pdf' }),
    ],
  })) file: Express.Multer.File) {
    const csrfTokenAssignment = 'x-csrf-token';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenAssignment) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      const uniqueId = uuidv4();
      if (submittedCsrfToken) {
        if (!this.isSubmissionFileUploadFailed(createAssignmentDto)) {
          const contents = fs.readFileSync(file.path, { encoding: 'base64' });

          if (!createAssignmentDto) {
            createAssignmentDto = new CreateAssignmentDto();
          }
          createAssignmentDto.id = uniqueId;
          createAssignmentDto.file = contents;
          return this.appService.createAssigment(createAssignmentDto);
        }
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    }
    catch (error) {
      this.failedAttempts++;
      if (this.failedAttempts >= this.MAX_FAILED_ATTEMPTS) {
        await this.delay(this.DELAY_DURATION);
        this.failedAttempts = 0;
      }
      console.error("Une erreur s'est produite lors de la création de l'assignment :", error);
      throw new Error("Le fichier n'est pas un fichier PDF");
    }
  }

  @Post("create-quotation")
  @Authorization(true)
  @Permission(['FREELANCE'])
  async createQuotation(@Headers('x-csrf-token-quotation') submittedCsrfToken: string, @Req() request: Request, @Body(ValidationPipe) createQuotationDto: CreateQuotationDto) {
    const csrfTokenQuotation = 'x-csrf-token-quotation';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenQuotation) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      const uniqueId = uuidv4();
      if (submittedCsrfToken) {
        if (createQuotationDto.description.length >= 3 && createQuotationDto.description.length <= 100 &&
          createQuotationDto.amount !== undefined &&
          typeof createQuotationDto.amount === 'number' &&
          createQuotationDto.nbDays !== undefined &&
          typeof createQuotationDto.nbDays === 'number'
        ) {
          createQuotationDto.id = uniqueId;
          return this.appService.createQuotation(createQuotationDto);
        }
        else {
          throw new BadRequestException("Invalid input data");
        }
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    }
    catch (error) {
      throw new InternalServerErrorException("Une erreur s'est produite lors de la création de la quotation");
    }
  }

  @Get("getFreelancers")
  getUsersFromSeach() {
    return this.appService.getUsersFromSeach();
  }

  // getFreelancer with id GET
  @Get("getFreelancer/:id")
  getUserFromSeach(@Param('id') id: string) {
    return this.appService.getUserFromSeach(id);
  }

  //post payment
  @Post("payment/:id")
  createPayment(@Param('id') id: string, @Body() body: { amount: number, currency: string, payment_method: string }) {
    const { amount, currency, payment_method } = body;

    return this.appService.createPayment(id, amount, currency, payment_method);
  }

  //post refund
  @Post("refund")
  refundPayment(@Body() body: { payment_intent: string }) {
    const { payment_intent } = body;
    return this.appService.refundPayment(payment_intent);
  }

  @Patch('update-assignment-refund/:id')
  @Authorization(true)
  @Permission(['CLIENT'])
  async updateAssignmentRefund(@Headers('x-csrf-token-refund-assignment') submittedCsrfToken: string, @Req() request: Request, @Param("id") id: string, @Body() updateAssignmentRefundDto: UpdateAssignmentRefundDto) {
    const csrfTokenRefundAssignment = 'x-csrf-token-refund-assignment';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenRefundAssignment) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      if (submittedCsrfToken) {
        await this.appService.updateRefundAssignment(id, updateAssignmentRefundDto);
        return updateAssignmentRefundDto;
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    } catch (error) {
      console.error("Une erreur s'est produite lors du remboursement de la mission :", error);
      throw new InternalServerErrorException("Une erreur s'est produite lors du remboursement de la mission.");
    }
  }

  //get quotation by id
  @Get("getQuotation/:id")
  @Authorization(true)
  @Permission(['CLIENT'])
  async getQuotation(@Param('id') id: string) {

    const isSubmissionFailed = this.isSubmissionFailed(id);

    if (isSubmissionFailed) {
      this.failedAttempts++;
    } else {
      this.failedAttempts = 0;
    }

    if (this.failedAttempts >= this.MAX_FAILED_ATTEMPTS) {
      await this.delay(this.DELAY_DURATION);
      this.failedAttempts = 0;
    }
    else {
      return this.appService.getQuotationById(id);
    }

  }

  @Get("get-assignments-per-freelance/:id")
  @Authorization(true)
  @Permission(['FREELANCE'])
  @UseInterceptors(RemovePasswordInterceptor)
  getAssignmentPerUser(@Param('id') id: string) {
    return this.appService.getAssignmentPerUser(id);
  }

  @Patch('update-approve-assignment/:id')
  @Authorization(true)
  @Permission(['FREELANCE'])
  async updateApproveAssignment(@Headers('x-csrf-token-approve-assignment') submittedCsrfToken: string, @Req() request: Request, @Param("id") id: string, @Body() updateAssignmentDto: UpdateAssignmentDto) {
    const csrfTokenApproveAssignment = 'x-csrf-token-approve-assignment';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenApproveAssignment) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      if (submittedCsrfToken) {
        await this.appService.updateApproveAssignment(id, updateAssignmentDto);
        return updateAssignmentDto;
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    } catch (error) {
      console.error("Une erreur s'est produite lors de la mise à jour de la mission :", error);
      throw new InternalServerErrorException("Une erreur s'est produite lors de la mise à jour de l'assignment.");
    }
  }

  @Patch('update-reject-assignment/:id')
  @Authorization(true)
  @Permission(['FREELANCE'])
  async updateRejectAssignment(@Headers('x-csrf-token-reject-assignment') submittedCsrfToken: string, @Req() request: Request, @Param("id") id: string, @Body() updateAssignmentDto: UpdateAssignmentDto) {
    const csrfTokenRejectAssignment = 'x-csrf-token-reject-assignment';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenRejectAssignment) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      if (submittedCsrfToken) {
        await this.appService.updateRejectAssignment(id, updateAssignmentDto);
        return updateAssignmentDto;
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    } catch (error) {
      console.error("Une erreur s'est produite lors de la mise à jour de la mission :", error);
      throw new InternalServerErrorException("Une erreur s'est produite lors de la mise à jour de l'assignment.");
    }
  }

  @Get('get-assignment/:id')
  @Authorization(true)
  @UseInterceptors(RemovePasswordInterceptor)
  async getAssignment(@Param('id') id: string) {
    return this.appService.getAssignment(id);
  }

  @Get("get-quotations-per-company/:id")
  @Authorization(true)
  @Permission(['CLIENT'])
  @UseInterceptors(RemovePasswordInterceptor)
  async getQuotationsPerCompany(@Param('id') id: string) {
    return this.appService.getQuotationsPerCompany(id);
  }

  @Patch('update-approve-quotation/:id')
  @Authorization(true)
  @Permission(['CLIENT'])
  async updateApproveQuotation(@Headers('x-csrf-token-approve-quotation') submittedCsrfToken: string, @Req() request: Request, @Param("id") id: string, @Body() updateQuotationDto: UpdateQuotationDto) {
    const csrfTokenApproveQuotation = 'x-csrf-token-approve-quotation';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenApproveQuotation) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      if (submittedCsrfToken) {
        await this.appService.updateApproveQuotation(id, updateQuotationDto);
        return updateQuotationDto;
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    } catch (error) {
      console.error("Une erreur s'est produite lors de la mise à jour du devis :", error);
      throw new InternalServerErrorException("Une erreur s'est produite lors de la mise à jour du devis.");
    }
  }

  @Patch('update-reject-quotation/:id')
  @Authorization(true)
  @Permission(['CLIENT'])
  async updateRejectQuotation(@Headers('x-csrf-token-reject-quotation') submittedCsrfToken: string, @Req() request: Request, @Param("id") id: string, @Body() updateQuotationDto: UpdateQuotationDto) {
    const csrfTokenRejectQuotation = 'x-csrf-token-reject-quotation';
    try {
      for (var i = 0; i < request.rawHeaders.length; i += 2) {
        if (request.rawHeaders[i] === csrfTokenRejectQuotation) {
          submittedCsrfToken = request.rawHeaders[i + 1];
          break;
        }
      }
      if (submittedCsrfToken) {
        await this.appService.updateRejectQuotation(id, updateQuotationDto);
        return updateQuotationDto;
      } else {
        throw new UnauthorizedException('Token CSRF invalide');
      }
    } catch (error) {
      console.error("Une erreur s'est produite lors de la mise à jour du devis :", error);
      throw new InternalServerErrorException("Une erreur s'est produite lors de la mise à jour du devis.");
    }
  }

  @Get("get-accepted-assignment/:id")
  @Authorization(true)
  @UseInterceptors(RemovePasswordInterceptor)
  async getAcceptedAssignment(@Param('id') id: string) {
    return this.appService.getAcceptedAssignment(id);
  }

  @Get("get-accepted-quotation/:id")
  @Authorization(true)
  @UseInterceptors(RemovePasswordInterceptor)
  async getAcceptedQuotation(@Param('id') id: string) {
    return this.appService.getAcceptedQuotation(id);
  }

  // get paid assignment to display refund button
  @Get("get-paid-assignment/:id")
  @Authorization(true)
  async getPaidAssignment(@Param('id') id: string) {
    return this.appService.getPaidAssignment(id);
  }

  @Get("get-messages-per-assignment/:id")
  @Authorization(true)
  @UseInterceptors(RemovePasswordInterceptorMessages)
  async getMessagesPerAssignment(@Param('id') id: string, @Req() request: Request) {
    return this.appService.getMessagesPerAssignment(id, request);
  }


  @Post('send-message')
  async sendMessage(@Body() createMessageDto: CreateMessageDto) {
    try {
      await this.appService.sendMessage(createMessageDto);
      return createMessageDto;
    } catch (error) {
      console.error("Une erreur s'est produite lors de l'envoi du message :", error);
      throw new InternalServerErrorException("Une erreur s'est produite lors de l'envoi du message.");
    }
  }

  @Post('create-review')
  @Authorization(true)
  @Permission(['CLIENT'])
  async createReview(@Body() createReview: CreateReviewFreelanceDto, @Req() request: Request) {
      return await this.appService.createReviewFreelance(createReview, request)
  }

  @Get('admin/reviews')
  @Authorization(true)
  @Permission(['ADMIN'])
  async getReviews() {
      return await this.appService.getReviews()
  }

  @Delete('admin/delete-review/:id')
  @Authorization(true)
  @Permission(['ADMIN'])
  async deleteReview(@Param("id") id: string) {
      return await this.appService.deleteReview(id);
  }

  @Patch('admin/publish-review/:id')
  @Authorization(true)
  @Permission(['ADMIN'])
  async publishReview(@Param("id") id: string) {
      return await this.appService.publishReview(id);
  }

  @Get('get-reviews/:id')
  async getReviewsByFreelance(@Param("id") id: string) {
    return await this.appService.getReviewsByFreelance(id);
  }
}
