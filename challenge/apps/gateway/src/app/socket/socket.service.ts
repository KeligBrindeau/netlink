import { CreateMessageDto } from '@challenge/message';
import { Inject, Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { lastValueFrom, firstValueFrom } from 'rxjs';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class SocketService {

    constructor(
        @Inject('MESSAGING') private readonly messagingClient: ClientProxy,
    ) {}

    async sendMessage(createMessageDto: CreateMessageDto){
        await lastValueFrom(this.messagingClient.send('messaging_send_message', createMessageDto));
    }
}