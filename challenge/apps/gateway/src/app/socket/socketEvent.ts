import { OnModuleInit } from '@nestjs/common';
import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
import {Server, Socket} from 'socket.io';
import { SocketService } from './socket.service';
import { randomBytes } from 'crypto';

interface Message {
  content: string;
  transmitter: () => string;
  receiver: () => string;
  assignment: () => string;
}

@WebSocketGateway({
  cors: {
    origin: ['https://netlink-git-main-keligbrindeau.vercel.app'],
  },
})

export class SocketEvent implements OnModuleInit {
  constructor(private readonly socketService: SocketService) {}

  private clientTokens: Map<Socket, string> = new Map<Socket, string>();

  @WebSocketServer()
  server: Server;

  generateCsrfToken() {
    const tokenServeur = randomBytes(32).toString('hex');
    return tokenServeur;
  }

  onModuleInit() {
    this.server.on('connection', (socket) => {
      const clientIdToken = this.generateCsrfToken();
      this.clientTokens.set(socket, clientIdToken);
      socket.emit('clientIdToken', { id: socket.id, token: clientIdToken });
      console.log('Connected');
    });
  }

  @SubscribeMessage('newMessage')
  async handleNewMessage(@ConnectedSocket() client: Socket, @MessageBody() message: any) {
    try {
      const csrfTokenMessaging = message.csrfTokenMessaging;
      const clientId = client.id;

      if (!clientId) {
        throw new Error('Client ID is missing');
      }

      const clientIdToken = this.clientTokens.get(client);

      if (!clientIdToken) {
        throw new Error('CSRF Token for client ID is missing');
      }

      if (csrfTokenMessaging !== clientIdToken) {
        throw new Error('CSRF Token is not valid');
      }

      await this.socketService.sendMessage(message);
      this.server.emit('onMessage', {
        msg: 'New Message',
        content: message,
      });
    } catch(error) {
      console.log(error);
    }
  }
}