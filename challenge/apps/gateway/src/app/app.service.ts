import { CreateUserClientDto, LoginUserDto, ConfirmUserDto, CreateUserFreelanceDto, UpdateClientDto, UpdateFreelanceDto } from '@challenge/user';
import { CreateAssignmentDto, UpdateAssignmentDto, UpdateAssignmentRefundDto } from '@challenge/assignment';
import { Inject, Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom, firstValueFrom } from 'rxjs';
import { CreateQuotationDto, UpdateQuotationDto } from '@challenge/quotation';
import { CreateMessageDto } from '@challenge/message';
import { CreateReviewFreelanceDto } from '@challenge/review-freelance';

@Injectable()
export class AppService {

  constructor(
    @Inject('AUTHENTIFICATION') private readonly authentificationClient: ClientProxy,
    @Inject('MESSAGING') private readonly messagingClient: ClientProxy,
    @Inject('SEARCH') private readonly searchClient: ClientProxy,
    @Inject('REVIEW') private readonly reviewClient: ClientProxy,
    @Inject('PAYMENT') private readonly paymentClient: ClientProxy,
    @Inject('KANBAN') private readonly kanbanClient: ClientProxy,
  ){}

  async createUserClient(createUserClientDto: CreateUserClientDto) {
    try {
      const userClientCreateResponse = await lastValueFrom(this.authentificationClient.send('create_user_client', createUserClientDto));
      if (userClientCreateResponse.status !== HttpStatus.CREATED) {
        return new HttpException(userClientCreateResponse.message, userClientCreateResponse.status);
      } else if (userClientCreateResponse.status === HttpStatus.CREATED) {
        await lastValueFrom(this.messagingClient.send('messaging_create_user_client', createUserClientDto));
        await lastValueFrom(this.reviewClient.send('review_create_user_client', createUserClientDto));
        await lastValueFrom(this.paymentClient.send('payment_create_user_client', createUserClientDto));
        this.searchClient.send('search_create_user_client', createUserClientDto).subscribe();
        return userClientCreateResponse;
      }

    } catch (e) {
      return new HttpException(e, 500);
    }
  }

  async createUserFreelance(createUserFreelance: CreateUserFreelanceDto) {
    try {
      const userFreelanceCreateResponse = await lastValueFrom(this.authentificationClient.send('create_user_freelance', createUserFreelance));
      if (userFreelanceCreateResponse.status !== HttpStatus.CREATED) {
        return new HttpException(userFreelanceCreateResponse.message, userFreelanceCreateResponse.status);
      } else if (userFreelanceCreateResponse.status === HttpStatus.CREATED) {
        await lastValueFrom(this.messagingClient.send('messaging_create_user_freelance', createUserFreelance))
        await lastValueFrom(this.reviewClient.send('review_create_user_freelance', createUserFreelance));
        await lastValueFrom(this.paymentClient.send('payment_create_user_freelance', createUserFreelance));
        this.searchClient.send('search_create_user_freelance', createUserFreelance).subscribe();
        return userFreelanceCreateResponse;
      }
    } catch (e) {
      return new HttpException(e, 500);
    }

  }

  async updateUser(updateUser: UpdateClientDto, request) {
    try {
      const userTokenInfo =
        await firstValueFrom(
          this.authentificationClient.send('token_decode', {
            token: request.headers.authorization,
          }),
        );
      if (userTokenInfo.item.userId !== updateUser.id && !userTokenInfo.item.roles.includes("ADMIN")) {
        return new HttpException('Vous ne pouvez pas accéder à cette ressource', 401);
      }

      const userUpdateClientResponse = await lastValueFrom(this.authentificationClient.send('update_user', updateUser));
      if (userUpdateClientResponse.status !== HttpStatus.OK) {
        return new HttpException(userUpdateClientResponse.message, userUpdateClientResponse.status);
      } else if (userUpdateClientResponse.status === HttpStatus.OK) {
        await lastValueFrom(this.messagingClient.send('messaging_update_user', updateUser))
        await lastValueFrom(this.reviewClient.send('review_update_user', updateUser));
        await lastValueFrom(this.paymentClient.send('payment_update_user', updateUser));
        this.searchClient.send('search_update_user', updateUser).subscribe();
        return userUpdateClientResponse;
      }
    } catch (e) {
      return new HttpException(e, 500);
    }

  }

  async updateUserFreelance(updateUserFreelance: UpdateFreelanceDto, request) {
    try {
      const userTokenInfo =
        await firstValueFrom(
          this.authentificationClient.send('token_decode', {
            token: request.headers.authorization,
          }),
        );
      if (userTokenInfo.item.userId !== updateUserFreelance.id && !userTokenInfo.item.roles.includes("ADMIN")) {
        return new HttpException('Vous ne pouvez pas accéder à cette ressource', 401);
      }
      const userUpdateFreelanceResponse = await lastValueFrom(this.authentificationClient.send('update_user_freelance', updateUserFreelance));
      if (userUpdateFreelanceResponse.status !== HttpStatus.OK) {
        return new HttpException(userUpdateFreelanceResponse.message, userUpdateFreelanceResponse.status);
      } else if (userUpdateFreelanceResponse.status === HttpStatus.OK) {
        await lastValueFrom(this.messagingClient.send('messaging_update_user_freelance', updateUserFreelance))
        await lastValueFrom(this.reviewClient.send('review_update_user_freelance', updateUserFreelance));
        await lastValueFrom(this.paymentClient.send('payment_update_user_freelance', updateUserFreelance));
        this.searchClient.send('search_update_user_freelance', updateUserFreelance).subscribe();
        return userUpdateFreelanceResponse;
      }
    } catch (e) {
      return new HttpException(e, 500);
    }

  }

  async uploadImgBase64(file: string, id: string, request) {
    const userTokenInfo =
      await firstValueFrom(
        this.authentificationClient.send('token_decode', {
          token: request.headers.authorization,
        }),
      );
    if (userTokenInfo.item.userId !== id && !userTokenInfo.item.roles.includes("ADMIN")) {
      return new HttpException('Vous ne pouvez pas modifier cette ressource', 401);
    }
    const userUploadImgResponse = await lastValueFrom(this.authentificationClient.send('authentification_upload_img_base_64', { file, id }));
    if (userUploadImgResponse.status !== HttpStatus.OK) {
      return new HttpException(userUploadImgResponse.message, userUploadImgResponse.status);
    } else if (userUploadImgResponse.status === HttpStatus.OK) {
      await lastValueFrom(this.messagingClient.send('messaging_upload_img', { file, id }))
      await lastValueFrom(this.reviewClient.send('review_upload_img', { file, id }));
      await lastValueFrom(this.paymentClient.send('payment_upload_img', { file, id }));
      this.searchClient.send('search_upload_img', { file, id }).subscribe();
      return userUploadImgResponse;
    }
  }

  async getMe(id: string, request) {
    try {
      const userTokenInfo =
        await firstValueFrom(
          this.authentificationClient.send('token_decode', {
            token: request.headers.authorization,
          }),
        );
      if (userTokenInfo.item.userId !== id && !userTokenInfo.item.roles.includes("ADMIN")) {
        return new HttpException('Vous ne pouvez pas accéder à cette ressource', 401);
      }
      const result = await lastValueFrom(this.authentificationClient.send('user_profile', id));
      return result
    } catch (error) {
      return new HttpException(error, 500);
    }
  }

  async confirmUser(params: ConfirmUserDto) {
    const confirmUserResponse = await lastValueFrom(this.authentificationClient.send('user_confirm', { link: params.link, }));
    if (confirmUserResponse.status !== HttpStatus.OK) {
      return new HttpException(confirmUserResponse.message, confirmUserResponse.status);
    } else if (confirmUserResponse.status === HttpStatus.OK) {
      await firstValueFrom(this.messagingClient.send('messaging_user_confirmed', { id: confirmUserResponse.userId }));
      await firstValueFrom(this.reviewClient.send('review_user_confirmed', { id: confirmUserResponse.userId }));
      await firstValueFrom(this.paymentClient.send('payment_user_confirmed', { id: confirmUserResponse.userId }));
      await firstValueFrom(this.searchClient.send('search_user_confirmed', { id: confirmUserResponse.userId }));
      return confirmUserResponse;
    }
  }

  async loginUser(loginUserDto: LoginUserDto) {
    return await lastValueFrom(this.authentificationClient.send('login_user', loginUserDto));
  }

  async logout(request) {
    const userInfo = request.user;

    return await firstValueFrom(
      this.authentificationClient.send('token_destroy', {
        userId: userInfo.id,
      }),
    );
  }

  async createAssigment(createAssignmentDto: CreateAssignmentDto) {
    await lastValueFrom(this.messagingClient.send('messaging_create_assignment', createAssignmentDto));
    await lastValueFrom(this.paymentClient.send('payment_create_assignment', createAssignmentDto));
    await lastValueFrom(this.kanbanClient.send('kanban_create_assignment', createAssignmentDto));
  }

  async createQuotation(createQuotationDto: CreateQuotationDto) {
    await lastValueFrom(this.messagingClient.send('messaging_create_quotation', createQuotationDto));
    await lastValueFrom(this.paymentClient.send('payment_create_quotation', createQuotationDto));
  }

  async getAssignmentPerUser(id: string) {
    return await lastValueFrom(this.messagingClient.send('messaging_get_assignment_per_user', id));
  }

  async updateApproveAssignment(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    await lastValueFrom(this.messagingClient.send('messaging_update_approve_assignment', { id, updateAssignmentDto }));
    await lastValueFrom(this.paymentClient.send('payment_update_approve_assignment', { id, updateAssignmentDto }));
  }

  async updateRejectAssignment(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    await lastValueFrom(this.messagingClient.send('messaging_update_reject_assignment', { id, updateAssignmentDto }));
    await lastValueFrom(this.paymentClient.send('payment_update_reject_assignment', { id, updateAssignmentDto }));
  }

  async getAssignment(id: string) {
    return await lastValueFrom(this.messagingClient.send('messaging_get_assignment', id));
  }

  async updateApproveQuotation(id: string, updateQuotationDto: UpdateQuotationDto) {
    await lastValueFrom(this.messagingClient.send('messaging_update_approve_quotation', { id, updateQuotationDto }));
    await lastValueFrom(this.paymentClient.send('payment_update_approve_quotation', { id, updateQuotationDto }));
  }

  async getQuotationsPerCompany(id: string) {
    return await lastValueFrom(this.paymentClient.send('payment_get_quotation_per_company', id));
  }

  async updateRejectQuotation(id: string, updateQuotationDto: UpdateQuotationDto) {
    await lastValueFrom(this.messagingClient.send('messaging_update_reject_quotation', { id, updateQuotationDto }));
    await lastValueFrom(this.paymentClient.send('payment_update_reject_quotation', { id, updateQuotationDto }));
  }

  async getAcceptedAssignment(id: string) {
    return await lastValueFrom(this.messagingClient.send('messaging_get_accepted_assignment', id));
  }

  async getAcceptedQuotation(id: string) {
    return await lastValueFrom(this.paymentClient.send('payment_get_accepted_quotation', id));
  }

  //get all users
  async getUsersFromSeach() {
    return await lastValueFrom(this.searchClient.send('search_get_users', {}));
  }

  //get user by id
  async getUserFromSeach(id: string) {
    return await lastValueFrom(this.searchClient.send('search_get_user', id));
  }

  //post payment
  async createPayment(id: string, amount: number, currency: string, paymentId: string) {
    return await lastValueFrom(this.paymentClient.send('payment_stripe_create_payment', { id, amount, currency, paymentId }));
  }

  //refund payment
  async refundPayment(payment_intent: string) {
    return await lastValueFrom(this.paymentClient.send('payment_refund_payment_intent', { payment_intent }));
  }

  async updateRefundAssignment(id: string, updateAssignmentRefundDto: UpdateAssignmentRefundDto) {
    await lastValueFrom(this.messagingClient.send('messaging_refund_assignment', { id, updateAssignmentRefundDto }));
    await lastValueFrom(this.paymentClient.send('payment_refund_assignment', { id, updateAssignmentRefundDto }));
  }

  //get quotation by id
  async getQuotationById(id: string) {
    return await lastValueFrom(this.paymentClient.send('payment_get_quotation', { id }));
  }
  async sendMessage(createMessageDto: CreateMessageDto) {
    await lastValueFrom(this.messagingClient.send('messaging_send_message', createMessageDto));
  }

  async sendFileInAssignment(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    await lastValueFrom(this.messagingClient.send('messaging_update_assignment', updateAssignmentDto));
    await lastValueFrom(this.paymentClient.send('payment_update_assignment', updateAssignmentDto));
  }


  async getPaidAssignment(id: string) {
    return await lastValueFrom(this.paymentClient.send('payment_get_paid_assignment', id));
  }

  async getMessagesPerAssignment(id: string, request) {
    try {
      const userTokenInfo =
        await firstValueFrom(
          this.authentificationClient.send('token_decode', {
            token: request.headers.authorization,
          }),
        );
      const assignment = await lastValueFrom(this.messagingClient.send('messaging_get_assignment', id));
      if (assignment) {
        if (assignment.company.id !== userTokenInfo.item.userId && assignment.freelance.id !== userTokenInfo.item.userId) {
          return new HttpException('Vous ne pouvez pas accéder à cette ressource', 401);
        }
      }
      const result = await lastValueFrom(this.messagingClient.send('messaging_get_messages_per_assignment', id ));
      return result;
    }
    catch (error) {
      return new HttpException(error, 500);
    }
  }

  async createReviewFreelance(createReview: CreateReviewFreelanceDto, request) {
    const userTokenInfo =
      await firstValueFrom(
        this.authentificationClient.send('token_decode', {
          token: request.headers.authorization,
        }),
      );
    if (userTokenInfo.item.userId !== createReview.ratingUser) {
      return new HttpException('Vous ne pouvez pas accéder à cette ressource', 401);
    }
    return await lastValueFrom(this.reviewClient.send('create_review', createReview));
  }

  async getReviews() {
    return await lastValueFrom(this.reviewClient.send('get_reviews',  {}));
  }

  async deleteReview(id: string) {
    return await lastValueFrom(this.reviewClient.send('delete_review', {id}));
  }

  async publishReview(id: string) {
    return await lastValueFrom(this.reviewClient.send('publish_review', {id}));
  }

  async getReviewsByFreelance(id: string) {
    return await lastValueFrom(this.reviewClient.send('get_reviews_by_freelance', id));
  }
}
