import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class RemovePasswordInterceptorMessages implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(data => {
        if (Array.isArray(data)) {
          return data.map(item => {
            this.removePassword(item.transmitter);
            this.removePassword(item.receiver);
            return item;
          })
        } else {
          this.removePassword(data.transmitter);
          this.removePassword(data.receiver);
          return data;
        }
      }),
    );
  }

  private removePassword(data: any): any {
    if (data && data.password) {
      delete data.password;
    }
    return data;
  }
}
