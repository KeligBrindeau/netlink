import { CreateAssignmentDto } from '@challenge/assignment';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { MouseEventDto, OsDto, SurveyDto, VisitDto } from '@challenge/analytics-kpi';
import { ClickDto } from '@challenge/analytics-kpi';

@Injectable()
export class AnalyticsService {

  constructor(
    @Inject('ANALYTICS') private readonly analyticsClient: ClientProxy,
  ){}

  async handleMouseData(mouseEventDto: MouseEventDto){
    this.analyticsClient.send('mouseEvent', mouseEventDto).subscribe();
  }

  async getHeatmapData(){
    return await lastValueFrom(this.analyticsClient.send('getHeatmapData', {}));
  }

  async saveClick(clickDto: ClickDto){
    this.analyticsClient.send('saveClick', clickDto).subscribe()
  }

  async getClick(){
    return await lastValueFrom(this.analyticsClient.send('getClick', {}));
  }

  async saveOs(osDto: OsDto){
    this.analyticsClient.send('saveOs', osDto).subscribe();
  }

  async getOs(){
    return await lastValueFrom(this.analyticsClient.send('getOs', {}));
  }

  async saveVisit(visitDto: VisitDto){
    this.analyticsClient.send('saveVisit', visitDto).subscribe();
  }

  async getVisit(){
    return await lastValueFrom(this.analyticsClient.send('getVisit', {}));
  }

  async saveSurvey(surveyDto: SurveyDto){
    this.analyticsClient.send('saveSurvey', surveyDto).subscribe();
  }

  async getSurveyData(){
    return await lastValueFrom(this.analyticsClient.send('getSurvey', {}));
  }

}
