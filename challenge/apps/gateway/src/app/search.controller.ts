import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { SearchService } from './search.service';

@Controller()
export class SearchController {
  constructor(private readonly searchService: SearchService) {}
  private failedAttempts = 0;
  private readonly MAX_FAILED_ATTEMPTS = 2;
  private readonly DELAY_DURATION = 10000;
  private readonly MAX_STRING_LENGTH = 80;
  private readonly MIN_STRING_LENGTH = 2;

  private delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  private isSubmissionFailed(formData: any): boolean {
    if (
      !formData ||
      formData.length > this.MAX_STRING_LENGTH ||
      formData.length < this.MIN_STRING_LENGTH
    ) {
      return true;
    }

    return false;
  }

  @Get('search')
  async getData(@Query('keywords') keywords : string) {

    const isSubmissionFailed = this.isSubmissionFailed(keywords);

    if (isSubmissionFailed) {
      this.failedAttempts++;
    } else {
      this.failedAttempts = 0;
    }

    if (this.failedAttempts >= this.MAX_FAILED_ATTEMPTS) {
      await this.delay(this.DELAY_DURATION);
      this.failedAttempts = 0;
    }
    else{
      return this.searchService.search(keywords);
    }
  }
}
