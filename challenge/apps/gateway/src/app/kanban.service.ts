import { KanbanDto } from '@challenge/kanban-project';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class KanbanService {

  constructor(
    @Inject('KANBAN') private readonly kanbanClient: ClientProxy,
  ){}

  async getTickets(id: string){
    return await lastValueFrom(this.kanbanClient.send('getTickets', id ));
  }

  async saveTicket(kanbanDto: KanbanDto){
    this.kanbanClient.send('saveTicket', kanbanDto).subscribe();
  }

  async deleteTicket(id: string){
    return await lastValueFrom(this.kanbanClient.send('deleteTicket', id));
  }

  async updateTicket(id: string, kanbanDto: KanbanDto){
    return await lastValueFrom(this.kanbanClient.send('updateTicket',{id, kanbanDto}));
  }

}
