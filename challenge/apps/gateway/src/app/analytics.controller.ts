import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { AnalyticsService } from './analytics.service';
import { ClickDto, MouseEventDto, OsDto, SurveyDto, VisitDto } from '@challenge/analytics-kpi';

@Controller()
export class AnalyticsController {
  constructor(private readonly analyticsService: AnalyticsService) {}


  @Post('mouseEvent')
  getData(@Body() MouseEventDTO : MouseEventDto) {
    return this.analyticsService.handleMouseData(MouseEventDTO);
  }

  @Get('mouseEvent')
  getHeatmapData(){
    return this.analyticsService.getHeatmapData();
  }

  @Post('saveClick')
  saveCLick(@Body() clickDto: ClickDto){
    return this.analyticsService.saveClick(clickDto);
  }

  @Get('getClick')
  getClick(){
    return this.analyticsService.getClick();
  }

  @Post('saveOs')
  saveOs(@Body() osDto: OsDto){
    return this.analyticsService.saveOs(osDto);
  }

  @Get('getOs')
  getOs(){
    return this.analyticsService.getOs();
  }

  @Post('saveVisit')
  saveVisit(@Body() visitDto: VisitDto){
    return this.analyticsService.saveVisit(visitDto);
  }

  @Get('getVisit')
  getVisit(){
    return this.analyticsService.getVisit();
  }

  @Post('saveSurvey')
  saveSurvey(@Body() surveyDto: SurveyDto){
    return this.analyticsService.saveSurvey(surveyDto);
  }

  @Get('getSurvey')
  getSurvey(){
    return this.analyticsService.getSurveyData();
  }
}
