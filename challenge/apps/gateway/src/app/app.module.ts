import { Module } from '@nestjs/common';
import { AuthGuard } from './guards/authorization.guard';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';
import { AnalyticsController } from './analytics.controller';
import { AnalyticsService } from './analytics.service';
import { APP_GUARD } from '@nestjs/core';
import { PermissionGuard } from './guards/permission.guard';
import { MulterModule, MulterModuleOptions } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { SocketEvent } from './socket/socketEvent';
import { SocketService } from './socket/socket.service';
import { KanbanController } from './kanban.controller';
import { KanbanService } from './kanban.service';

const multerConfig: MulterModuleOptions = {
  storage: diskStorage({
    destination: './tmp/uploads',
    filename: (req, file, cb) => {
      const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const extension = extname(file.originalname);
      cb(null, `${uniqueSuffix}${extension}`);
    },
  }),

  limits: {
    fileSize: 10 * 1024 * 1024,
  },
};
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'AUTHENTIFICATION',
        transport: Transport.TCP,
        options: {
          port: 2001,
          host: 'authentification'
        }
      }
    ]),
    ClientsModule.register([
      {
        name: 'MESSAGING',
        transport: Transport.TCP,
        options: {
          port: 2002,
          host: 'messaging'
        }
      }
    ]),
    ClientsModule.register([
      {
        name: 'SEARCH',
        transport: Transport.TCP,
        options: {
          port: 2003,
          host: 'search'
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'REVIEW',
        transport: Transport.TCP,
        options: {
          port: 2004,
          host: 'review'
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'PAYMENT',
        transport: Transport.TCP,
        options: {
          port: 5000,
          host: 'payment'
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'ANALYTICS',
        transport: Transport.TCP,
        options: {
          port: 5001,
          host: 'analytics'
        }
      },
    ]),
    ClientsModule.register([
      {
        name: 'KANBAN',
        transport: Transport.TCP,
        options: {
          port: 2007,
          host: 'kanban'
        },
      },
    ]),
    MulterModule.register(multerConfig),
  ],
  controllers: [AppController, SearchController, AnalyticsController, KanbanController],
  providers: [
    AppService, 
    SearchService,
    AnalyticsService,
    SocketEvent,
    SocketService,
    KanbanService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: PermissionGuard,
    },
  ],
})
export class AppModule {}
