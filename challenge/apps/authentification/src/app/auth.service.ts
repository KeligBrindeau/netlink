import { Injectable, HttpException, HttpStatus, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DataSource } from 'typeorm';
import { UserEntity } from './user.entity';
import { CreateUserClientDto, CreateUserFreelanceDto, LoginUserDto, UpdateClientDto, UpdateFreelanceDto } from '@challenge/user';
import { SeedService } from './seed';
import { JwtService } from '@nestjs/jwt';
import { AuthPayload } from './interfaces/auth-payload.interface';
import * as bcrypt from 'bcrypt';
import { UserLinkEntity } from './user-link.entity';
import { lastValueFrom } from 'rxjs';
import { IUserConfirmResponse } from './interfaces/user-confirm-response.interface';
import { ITokenResponse } from './interfaces/token-response.interface';
import { TokenEntity } from './token.entity';
import { IUpdateUserResponse } from './interfaces/update-user-response.interface';
import fs from 'fs';
import { extname } from 'path';

@Injectable()
export class AuthService {
  private tokenRepository: Repository<TokenEntity>;

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    @InjectRepository(UserLinkEntity)
    private userLinkRepository: Repository<UserLinkEntity>,
    private readonly jwtService: JwtService,
    @Inject('MAILER') private readonly mailerServiceClient: ClientProxy,
    dataSource: DataSource
  ) {
    this.tokenRepository = dataSource.getRepository(TokenEntity);
  }

  /* Fonction pour l'inscription */
  // Inscription client
  async signupClient(createUserClientDTO: CreateUserClientDto) {
    try {
      const emailExist = await this.searchUser(createUserClientDTO.email)
      if (emailExist) {
        return { status: HttpStatus.BAD_REQUEST, message: 'Cette adresse mail est déjà utilisée.' };
      }
      const hashedPassword = bcrypt.hashSync(createUserClientDTO.password, 10);
      const user = {
        ...createUserClientDTO,
        password: hashedPassword,
        role: ["CLIENT"],
        isConfirmed: false
      };
      await this.userRepository.insert(user);
      this.confirmAccount(user);
      return { status: HttpStatus.CREATED, message: 'Votre compte a bien été créé. Vous devez le confirmer avec l\'email que vous allez recevoir' };
    } catch (error) {
      return { status: HttpStatus.INTERNAL_SERVER_ERROR, message: 'Une erreur s\'est produite lors de l\'insertion de l\'utilisateur.' };
    }
  }

  // Inscription freelance
  async signupFreelance(createUserFreelancetDTO: CreateUserFreelanceDto) {
    try {
      const emailExist = await this.searchUser(createUserFreelancetDTO.email)
      if (emailExist) {
        return { status: HttpStatus.BAD_REQUEST, message: 'Cette adresse mail est déjà utilisée.' };
      }
      const hashedPassword = bcrypt.hashSync(createUserFreelancetDTO.password, 10);
      const user = {
        ...createUserFreelancetDTO,
        password: hashedPassword,
        role: ["FREELANCE"],
        isConfirmed: false
      };
      await this.userRepository.insert(user);
      this.confirmAccount(user);
      return { status: HttpStatus.CREATED, message: 'Votre compte a bien été créé. Vous devez le confirmer avec l\'email que vous allez recevoir' };
    } catch (error) {
      return { status: HttpStatus.INTERNAL_SERVER_ERROR, message: 'Une erreur s\'est produite lors de l\'insertion de l\'utilisateur.' };
    }
  }

  async updateUser(updateUserDTO: UpdateClientDto): Promise<IUpdateUserResponse> {
    let result: IUpdateUserResponse;
    if (updateUserDTO.id) {
      try {
        const user = await this.userRepository.findOneBy({ id: updateUserDTO.id });
        if (user) {
          if (user.email !== updateUserDTO.email) {
            const emailExist = await this.searchUser(updateUserDTO.email)
            if (emailExist) {
              return {
                status: HttpStatus.BAD_REQUEST,
                message: 'Cette adresse mail est déjà utilisée.',
                errors: null
              };
            }
          }

          if (updateUserDTO.password) {
            const hashedPassword = bcrypt.hashSync(updateUserDTO.password, 10);
            const updatedUser = {
              ...updateUserDTO,
              password: hashedPassword
            };
            await this.userRepository.save(Object.assign(user, updatedUser));
          } else {
            const updatedUser = {
              ...updateUserDTO
            };
            await this.userRepository.save(Object.assign(user, updatedUser));
          }

          result = {
            status: HttpStatus.OK,
            message: 'Les infos de l\'utilisateur ont bien été mises à jour',
            errors: null,
          };
          return result;
        } else {
          result = {
            status: HttpStatus.NOT_FOUND,
            message: 'user_update_by_id_not_found',
            errors: null
          };
        }
      } catch (error) {
        result = {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'erreur lors de la mise à jour des infos de l\'utilisateur',
          errors: error
        };
      }
    } else {
      result = {
        status: HttpStatus.BAD_REQUEST,
        message: 'user_update_by_id_bad_request',
        errors: null,
      };
    }
    return result;
  }

  async updateFreelance(updateFreelanceDTO: UpdateFreelanceDto): Promise<IUpdateUserResponse> {
    let result: IUpdateUserResponse;
    if (updateFreelanceDTO.id) {
      try {
        const user = await this.userRepository.findOneBy({ id: updateFreelanceDTO.id });
        if (user) {
          if (user.email !== updateFreelanceDTO.email) {
            const emailExist = await this.searchUser(updateFreelanceDTO.email)
            if (emailExist) {
              return {
                status: HttpStatus.BAD_REQUEST,
                message: 'Cette adresse mail est déjà utilisée.',
                errors: null
              };
            }
          }

          if (updateFreelanceDTO.password) {
            const hashedPassword = bcrypt.hashSync(updateFreelanceDTO.password, 10);
            const updatedUser = {
              ...updateFreelanceDTO,
              password: hashedPassword
            };
            await this.userRepository.save(Object.assign(user, updatedUser));
          } else {
            const updatedUser = {
              ...updateFreelanceDTO
            };
            await this.userRepository.save(Object.assign(user, updatedUser));
          }

          result = {
            status: HttpStatus.OK,
            message: 'Les infos de l\'utilisateur ont bien été mises à jour',
            errors: null,
          };
          return result;
        } else {
          result = {
            status: HttpStatus.NOT_FOUND,
            message: 'user_update_by_id_not_found',
            errors: null
          };
        }
      } catch (error) {
        result = {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'erreur lors de la mise à jour des infos de l\'utilisateur',
          errors: error
        };
      }
    } else {
      result = {
        status: HttpStatus.BAD_REQUEST,
        message: 'user_update_by_id_bad_request',
        errors: null,
      };
    }
    return result;
  }

  // Vérifie si adresse mail deja utilisée
  async searchUser(mail: string) {
    return await this.userRepository.findOneBy({ email: mail });
  }

  async getUserById(param): Promise<UserEntity> {
    const user = await this.userRepository.findOne({
      where: { id: param.id },
    });
    return user;
  }

  /* Fonctions pour la confirmation du compte */
  // Envoi du mail pour avec le lien de confirmation de compte 
  async confirmAccount(user: CreateUserClientDto | CreateUserFreelanceDto) {
    const userLink = await this.createUserLink(
      user.id,
    );
    const validationLink = `https://netlink-git-main-keligbrindeau.vercel.app//confirm/${userLink.link}`
    return await lastValueFrom(this.mailerServiceClient
      .send('mail_send', {
        to: user.email,
        subject: 'Confirmation de compte',
        html: `<center>
              <b>Bonjour, merci de bien vouloir confirmer votre adresse mail pour accéder à Netlink.</b><br>
              Pour ce faire, cliquez sur le lien suivant<br>
              <a href="${validationLink}"><b>Confirmer l'adresse mail</b></a>
              </center>`,
      }));
  }

  async createUserLink(id: string): Promise<UserLinkEntity> {
    const newUserLink = new UserLinkEntity();
    newUserLink.user_id = id;
    const createdUserLink = await this.userLinkRepository.save(newUserLink);

    return createdUserLink;
  }

  public async getUserLink(link: string): Promise<UserLinkEntity[]> {
    return this.userLinkRepository
      .createQueryBuilder()
      .where('link = :link', { link })
      .andWhere('is_used = false')
      .getMany();
  }
  

  public async confirmUser(confirmParams: {
    link: string;
  }): Promise<IUserConfirmResponse> {
    let result: IUserConfirmResponse;

    if (confirmParams) {
      const userLink = await this.getUserLink(confirmParams.link);
      if (userLink && userLink[0]) {
        const userId = userLink[0].user_id;
        await this.userRepository.update(userId, { isConfirmed: true });
        await this.userLinkRepository.update(userLink[0].id, {
          is_used: true,
        });
        result = {
          status: HttpStatus.OK,
          message: 'user_confirmed',
          errors: null,
          userId: userId
        };
      } else {
        result = {
          status: HttpStatus.NOT_FOUND,
          message: 'Cet utilisateur n\'a pas été trouvé ou a déjà été confirmé',
          errors: null,
        };
      }
    } else {
      result = {
        status: HttpStatus.BAD_REQUEST,
        message: 'user_confirm_bad_request',
        errors: null,
      };
    }
    return result;
  }

  /* Fonctions pour la connexion */
  async login(loginUserDto: LoginUserDto): Promise<ITokenResponse> {
    let result: ITokenResponse;
    const checkUserCredential = await this.checkUserCredential(loginUserDto.email, loginUserDto.password);
    if (checkUserCredential) {
      const user = await this.userRepository.findOneBy({ email: loginUserDto.email });

      if (user.isConfirmed) {
        const payload: AuthPayload = {
          userId: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          roles: user.role,
          // picture: user.picture,
          birthDate: user.birthDate,
          status: user.status,
          skills: user.skills,
          job: user.job,
          description: user.description,
          city: user.city,
          hourlyRate: user.hourlyRate
        };

        try {
          const token: string = await this.generateJwtToken(payload);
          const newToken = new TokenEntity();
          newToken.userId = user.id;
          newToken.token = token;
          await this.tokenRepository.save(newToken);
          result = {
            status: HttpStatus.CREATED,
            message: 'token_create_success',
            token: token
          };
        } catch (e) {
          result = {
            status: HttpStatus.BAD_REQUEST,
            message: 'token_create_bad_request',
            token: null
          };
        }
      } else {
        result = {
          status: HttpStatus.BAD_REQUEST,
          message: 'Le compte n\'est pas encore confirmé',
          token: null
        };
      }
    } else {
      result = {
        status: HttpStatus.BAD_REQUEST,
        message: 'Email ou mot de passe incorrect',
        token: null
      };
    }
    return result;
  }

  async checkUserCredential(mail: string, password: string): Promise<boolean> {
    const user = await this.userRepository.findOneBy({ email: mail });

    if (!user) {
      return false
    } else if (user) {
      const valid_password = await bcrypt.compare(password, user.password);
      if (valid_password) return true
    } else {
      return false
    }
  }

  async generateJwtToken(payload: AuthPayload): Promise<string> {
    const jwt = this.jwtService.signAsync(payload, {
      expiresIn: process.env.JWT_EXPIRATION,
      secret: process.env.JWT_SECRET,
    },);
    return jwt
  }

  async decodeToken(token: string): Promise<{ userId: string }> {
    const result = {
      userId: null,
      roles: null
    };

    const tokenToDecode = await this.tokenRepository.findOneBy({ token });
    if (tokenToDecode) {
      try {
        const tokenData = this.jwtService.decode(tokenToDecode.token) as {
          exp: number;
          userId: string;
          roles: string[];
        };

        // if (tokenData.exp * 1000 > Date.now()) {
        result.userId = tokenData.userId;
        result.roles = tokenData.roles;
        // }
      } catch (e) { }
    }
    return result;
  }

  public async deleteTokenForUserId(userId: string): Promise<TokenEntity[]> {
    const tokens = await this.tokenRepository.findBy({ userId });
    return this.tokenRepository.remove(tokens);
  }

  decodeBase64Image(base64Data) {
    const matches = base64Data.match(/^data:image\/([a-zA-Z0-9+/]+);base64,(.+)$/);
    if (matches.length !== 3) {
      throw new Error('Données encodées en base64 invalides.');
    }

    const decodedImage = Buffer.from(matches[2], 'base64'); // Décodage de la chaîne base64
    return {
      type: matches[1], // Type MIME de l'image
      data: decodedImage // Données binaires décodées
    };
  }


  async me(id: string) {
    const user = await this.userRepository.findOne({ where: { id: id } });
    delete user.password;

    if (user.picture) {
      user.picture = user.picture.split(';base64,').pop();
    }
    
    const result = {
      status: HttpStatus.OK,
      user: user
    };

    return result;
  }

  async uploadImgBase64(file: string, id: string) {
    let result = {
      status: HttpStatus.BAD_REQUEST,
      message: 'upload_img_bad_request',
      img: null
    };
    try {
      
      await this.userRepository
        .createQueryBuilder()
        .update(UserEntity)
        .set({ picture: file })
        .where('id = :id', { id })
        .execute();
  
      result = {
        status: HttpStatus.OK,
        message: 'L\'image a bien été enregistrée',
        img: file
      };
    } catch (error) {
      result = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'upload_img_internal_server_error',
        img: null
      };
    }
    return result;
  }

  seedDatabase() {
    const obj = new SeedService(
      this.userRepository,
    );
    obj.seedDatabase();
  }

}
