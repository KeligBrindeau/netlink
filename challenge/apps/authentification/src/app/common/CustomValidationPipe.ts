import { ValidationPipe } from '@nestjs/common';
import { HttpException, HttpStatus } from '@nestjs/common';

export class CustomValidationPipe extends ValidationPipe {
    constructor() {
        super({
            transform: true,
            whitelist: true,
            forbidNonWhitelisted: true,
            validationError: {
                target: false,
                value: false,
            },
            exceptionFactory: (errors) => {
                const messages = errors.map((error) => Object.values(error.constraints)).flat();
                const formattedMessage = messages.join('\n');
                return new HttpException({message : formattedMessage}, HttpStatus.BAD_REQUEST);
            },
        });
    }
}
