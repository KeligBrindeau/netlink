export interface IUpdateUserResponse {
    status: number;
    message: string;
    errors: { [key: string]: any } | null;
}