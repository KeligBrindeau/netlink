export interface AuthPayload {
    userId: string;
    firstName: string;
    lastName: string;
    email: string;
    roles: string[];
    // picture: string,
    birthDate: Date | null;
    status: Boolean | null;
    skills: string[];
    job: string;
    description: string;
    city: string;
    hourlyRate: number | null;
}