import { HttpException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserClientDto, UpdateClientDto } from '@challenge/user';
import { CreateAssignmentDto, UpdateAssignmentDto, UpdateAssignmentRefundDto } from '@challenge/assignment';
import { AssignmentEntity } from './assignment.entity';
import { CreateQuotationDto, UpdateQuotationDto } from '@challenge/quotation';
import { QuotationEntity } from './quotation.entity';
import { MessageEntity } from './message.entity';
import { SeedService } from './seed';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';
import { CreateMessageDto } from '@challenge/message';
import { io, Socket } from 'socket.io-client';
import { UpdateFreelanceDto } from '@challenge/user';
import fs from 'fs';

@Injectable()
export class AppService {
  socket: any;
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,

    @InjectRepository(QuotationEntity)
    private quotationRepository: Repository<QuotationEntity>,

    @InjectRepository(MessageEntity)
    private messageRepository: Repository<MessageEntity>,
  ) {
  }


  async signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ["CLIENT"], isConfirmed: false };
    return await this.userRepository.insert(user);
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ["FREELANCE"], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async updateUser(updateUserDto: UpdateClientDto) {
    try {
      const user = await this.userRepository.findOneBy({ id: updateUserDto.id });
      if (user) {
        if (updateUserDto.password) {
          const hashedPassword = bcrypt.hashSync(updateUserDto.password, 10);
          const updatedUser = {
            ...updateUserDto,
            password: hashedPassword
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        } else {
          const updatedUser = {
            ...updateUserDto
          };
          return await this.userRepository.save(Object.assign(user, updatedUser));
        }
      }
    } catch (error) {
      console.error
    }
  }

  async updateFreelance(updateFreelanceDTO: UpdateFreelanceDto) {
    const user = await this.userRepository.findOneBy({ id: updateFreelanceDTO.id });
    if (user) {
      if (updateFreelanceDTO.password) {
        const hashedPassword = bcrypt.hashSync(updateFreelanceDTO.password, 10);
        const updatedUser = {
          ...updateFreelanceDTO,
          password: hashedPassword
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      } else {
        const updatedUser = {
          ...updateFreelanceDTO
        };
        return await this.userRepository.save(Object.assign(user, updatedUser));
      }
    }
  } catch(error) {
    console.error
  }

  async uploadImgBase64(file: string, id: string) {
    try {
      
      return await this.userRepository
        .createQueryBuilder()
        .update(UserEntity)
        .set({ picture: file })
        .where('id = :id', { id })
        .execute();
  
    } catch (error) {
      console.error
    }
  }

  createAssignment(createAssignmentDto: CreateAssignmentDto) {
    createAssignmentDto.isRefund = false;
    this.assignmentRepository.insert(createAssignmentDto);
    return null;
  }

  createQuotation(createQuotationDto: CreateQuotationDto) {
    createQuotationDto.amount = createQuotationDto.amount *= 10;
    this.quotationRepository.insert(createQuotationDto);
    return null;
  }

  seedDatabase() {
    const obj = new SeedService(
      this.userRepository,
      this.assignmentRepository,
      this.quotationRepository,
      this.messageRepository
    );
    obj.seedDatabase();
  }


  sendMessage(createMessageDto: CreateMessageDto) {
    const message = new CreateMessageDto();
    message.content = createMessageDto.content;
    message.transmitter = createMessageDto.transmitter;
    message.receiver = createMessageDto.receiver;
    message.assignment = createMessageDto.assignment;
    message.timestamp = createMessageDto.timestamp;

    return this.messageRepository.insert(message);
  }

  async getAssignmentPerUser(id: string) {
    const assignments = await this.assignmentRepository.createQueryBuilder('assignment')
      .select(['assignment.id', 'assignment.description', 'assignment.file', 'assignment.company', 'assignment.freelance', 'assignment.status'])
      .leftJoinAndSelect('assignment.company', 'company')
      .where('assignment.freelance.id = :id', { id })
      .andWhere('assignment.status IS NULL')
      .getMany();
    return assignments;
  }

  async updateAssignmentApproveMessaging(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    const assignment = await this.assignmentRepository.findOne({ where: { id } });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    assignment.status = updateAssignmentDto.status
    return this.assignmentRepository.save(assignment);
  }

  async updateAssignmentRejectMessaging(id: string, updateAssignmentDto: UpdateAssignmentDto) {
    const assignment = await this.assignmentRepository.findOne({ where: { id } });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    assignment.status = updateAssignmentDto.status
    return this.assignmentRepository.save(assignment);
  }

  async getAssignment(id: string) {
    const assignment = await this.assignmentRepository.findOne({
      where: { id },
      relations: ["company", "freelance"]
    });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    return assignment;
  }

  async updateQuotationApproveMessaging(id: string, updateQuotationDto: UpdateQuotationDto) {
    const quotation = await this.quotationRepository.findOne({ where: { id } });
    if (!quotation) {
      throw new NotFoundException('Quotation not found');
    }
    quotation.status = updateQuotationDto.status
    return this.quotationRepository.save(quotation);
  }

  async updateQuotationRejectMessaging(id: string, updateQuotationDto: UpdateQuotationDto) {
    const quotation = await this.quotationRepository.findOne({ where: { id } });
    if (!quotation) {
      throw new NotFoundException('Quotation not found');
    }
    quotation.status = updateQuotationDto.status
    return this.quotationRepository.save(quotation);
  }

  async getAcceptedAssignment(id: string) {
    const assignments = await this.assignmentRepository.createQueryBuilder('assignment')
      .select(['assignment.id', 'assignment.description', 'assignment.file', 'assignment.company', 'assignment.freelance', 'assignment.status'])
      .leftJoinAndSelect('assignment.company', 'company')
      .leftJoinAndSelect('assignment.freelance', 'freelance')
      .where('(assignment.freelance.id = :id OR assignment.company.id = :id) AND assignment.status = :status', { id, status: true })
      .getMany();
    return assignments;
  }

  async getMessagesPerAssignment(id: string) {
    try {
      const messages = await this.messageRepository.createQueryBuilder('message')
      .select(['message', 'message.transmitter', 'message.receiver', 'message.transmitterId', 'message.receiverId'])
      .leftJoinAndSelect('message.transmitter', 'transmitter')
      .leftJoinAndSelect('message.receiver', 'receiver')
      .where('message.assignment = :id', { id })
      .orderBy('message.timestamp', 'ASC')
      .getMany();

      return messages;
    } catch(error) {
      return new HttpException(error, 500);
    }
  }

  async refundAssignment(id: string, updateAssignmentRefundDto: UpdateAssignmentRefundDto) {
    const assignment = await this.assignmentRepository.findOne({ where: { id } });
    if (!assignment) {
      throw new NotFoundException('Assignment not found');
    }
    assignment.isRefund = updateAssignmentRefundDto.isRefund;
    return this.assignmentRepository.save(assignment);
  }

}
