import { Body, Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto, UpdateFreelanceDto } from '@challenge/user';
import { CreateAssignmentDto, UpdateAssignmentDto, UpdateAssignmentRefundDto } from '@challenge/assignment';
import { CreateQuotationDto, UpdateQuotationDto } from '@challenge/quotation';
import { CreateUserFreelanceDto } from '@challenge/user';
import { CreateMessageDto } from '@challenge/message';
import { UpdateClientDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('messaging_create_user_client')
  createUser(@Payload() createUserDTO : CreateUserClientDto){
    return this.appService.signup(createUserDTO);
  }

  @EventPattern('messaging_create_user_freelance')
  createUserFreelance(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    return this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('messaging_update_user')
  updateUser(@Payload() updateUserDto : UpdateClientDto){
    return this.appService.updateUser(updateUserDto);
  }

  @EventPattern('messaging_update_user_freelance')
  updateUserFreelance(@Payload() updateUserDto : UpdateFreelanceDto){
    return this.appService.updateFreelance(updateUserDto);
  }

  @EventPattern('messaging_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }

  @EventPattern('messaging_upload_img')
  async uploadImgTest(@Payload()  payload: { file, id: string }){
    const { file, id } = payload;
    return this.appService.uploadImgBase64(file, id);
  }

  @EventPattern('messaging_create_assignment')
  createAssignment(@Payload() createAssignmentDto: CreateAssignmentDto){
    return this.appService.createAssignment(createAssignmentDto);
  }

  @EventPattern('messaging_create_quotation')
  createQuotation(@Payload() createQuotationDto: CreateQuotationDto){
    return this.appService.createQuotation(createQuotationDto);
  }

  @EventPattern('messaging_send_message')
  sendMessage(@Payload() createMessageDto: CreateMessageDto){
    return this.appService.sendMessage(createMessageDto);
  }

  @EventPattern('messaging_get_assignment_per_user')
  getAssignmentPerUser(@Payload() id: string){
    return this.appService.getAssignmentPerUser(id);
  }

  @EventPattern('messaging_update_approve_assignment')
  messagingUpdateApproveAssignment(data: { id: string; updateAssignmentDto: UpdateAssignmentDto }) {
    const { id, updateAssignmentDto } = data;
    return this.appService.updateAssignmentApproveMessaging(id, updateAssignmentDto);
  }

  @EventPattern('messaging_update_reject_assignment')
  messagingUpdateRejectAssignment(data: { id: string; updateAssignmentDto: UpdateAssignmentDto }) {
    const { id, updateAssignmentDto } = data;
    return this.appService.updateAssignmentRejectMessaging(id, updateAssignmentDto);
  }

  @EventPattern('messaging_get_assignment')
  getAssignment(@Payload() id: string){
    return this.appService.getAssignment(id);
  }

  @EventPattern('messaging_update_approve_quotation')
  messagingUpdateApproveQuotation(data: { id: string; updateQuotationDto: UpdateQuotationDto }) {
    const { id, updateQuotationDto } = data;
    return this.appService.updateQuotationApproveMessaging(id, updateQuotationDto);
  }

  @EventPattern('messaging_update_reject_quotation')
  messagingUpdateRejectQuotation(data: { id: string; updateQuotationDto: UpdateQuotationDto }) {
    const { id, updateQuotationDto } = data;
    return this.appService.updateQuotationRejectMessaging(id, updateQuotationDto);
  }

  @EventPattern('messaging_get_accepted_assignment')
  messagingListAcceptedAssignment(@Payload() id: string){
    return this.appService.getAcceptedAssignment(id);
  }

  @EventPattern('messaging_get_messages_per_assignment')
  messagingListMessagesPerAssignment(id: string ){
    return this.appService.getMessagesPerAssignment(id);
  }

  @EventPattern('messaging_refund_assignment')
  messagingRefundAssignment(data: { id: string; updateAssignmentRefundDto: UpdateAssignmentRefundDto }){
    const { id, updateAssignmentRefundDto } = data;
    return this.appService.refundAssignment(id, updateAssignmentRefundDto);
  }
}
