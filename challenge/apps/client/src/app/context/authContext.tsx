import React from 'react';

export const authContext = {
        token: "",
        isAuthenticated: false,
        login: (token: string) => { },
        logout: () => { },
        user: {
                id: "",
                firstName: "",
                lastName: "",
                email: "",
                roles: [] as string[],
                picture: "",
                birthDate: null as Date | null | string,
                status: null as boolean | null,
                skills: [] as string[],
                job: "",
                description: null as string | null,
                city: "",
                hourlyRate: null as number | null,
        }
}

export const AuthContext = React.createContext(authContext);
