import { createContext } from 'react';

const datasFreelanceContext = {
    skills: [] as string[],
    cities: [] as string[],
    jobs: [] as string[]
}

export const DatasFreelanceContext = createContext(datasFreelanceContext);
