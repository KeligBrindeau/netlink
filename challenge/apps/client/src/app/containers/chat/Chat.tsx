import React, { useState, useRef, useEffect, useContext } from 'react';
import jwt_decode from 'jwt-decode';
import {
  Avatar,
  Box,
  Button,
  Container,
  IconButton,
  Tooltip,
  Typography
} from '@mui/material';
import axios from 'axios';
import SendIcon from '@mui/icons-material/Send';
import CloseIcon from '@mui/icons-material/Close';
import './chat.css'
import { AuthContext } from '../../context/authContext';

const Chat = () => {

  interface Assignment {
    id: number;
    company: number;
    description: string;
    file: string;
    // freelanceId: number;
    // status: boolean;
  }
  const [show, setShown] = useState(false);
  const [assignments, setAssignments] = useState<Assignment[]>([]);
  const [errorGet, setErrorGet] = useState<string | null>(null);
  const { token } = useContext(AuthContext);
  const [decodedToken, setDecodedToken] = useState('');

  useEffect(() => {
    setDecodedToken(jwt_decode(token));
  }, [token]);

  useEffect(() => {
    if (decodedToken) {
      loadAcceptedAssignment();
    }
  }, [decodedToken])

  const openChat = () => {
    setShown(!show);
  }

  const closeChat = () => {
    setShown(false);
  }

  const loadAcceptedAssignment = async () => {
    try {
      await axios.get(`https://samy-saberi.fr/api/get-accepted-assignment/${decodedToken.userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then((response) => {
          setAssignments(response.data);
        })
    }
    catch(error: any) {
      setErrorGet(error.message);
      console.error('Cannot get assignments:', error);
    }
  }

  return (
    <Box className='box-chat' display='flex' alignItems='flex-end' justifyContent='flex-end' style={{ position: 'fixed', bottom: 85, right: 20, padding: 5 }}>
    <Avatar>
      <IconButton
        onClick={openChat}
        className='chat_icon_button'
        style={{ color: '#fff', backgroundColor: '#164E87' }}
      >
        <SendIcon />
      </IconButton>
    </Avatar>
    {show && (
      <Container className='chat_box'>
        <Container className='chat_wrapper'>
          <Container>
            <Box className='chat_header'>
              <Typography variant='h6' ml={2}>Mes clients</Typography>
              <Box flex={1} />
              <IconButton onClick={closeChat}>
                <CloseIcon />
              </IconButton>
            </Box>
            {assignments && assignments.length > 0 ? (
              assignments.map((assignment, assignmentIndex) => (
                <Box key={assignmentIndex} display='flex' flexDirection='column' alignItems='center' justifyContent='space-around' marginBottom={2} borderBottom={1} paddingBottom={1} borderColor="#164E87">
                  <Box>
                    <Tooltip title="Envoyer message à mon client">
                    {decodedToken && decodedToken.roles.includes('CLIENT') ? (
                        <Button variant='text' href={`/messaging/${assignment.id}`} style={{ color: '#164E87' }}>{assignment.freelance.lastName} {assignment.freelance.firstName}</Button>
                    ) : (
                      <Button variant='text' href={`/messaging/${assignment.id}`} style={{ color: '#164E87' }}>{assignment.company.lastName} {assignment.company.firstName}</Button>
                    )}
                    </Tooltip>
                  </Box>
                </Box>
              ))
            ) : (
               <Box display='flex' flexDirection='column' alignItems='center' justifyContent='space-around' marginBottom={2} borderBottom={1} paddingBottom={1} borderColor="#164E87">
                <p>Toujours pas de client</p>
              </Box>
            )}
          </Container>
        </Container>
      </Container>
    )}
    </Box>
  )
}


export default Chat;