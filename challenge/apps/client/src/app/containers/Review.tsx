import { Card, CardContent, Typography, Rating } from '@mui/material';

const Review = ({ author, text, stars }) => {
    return (
        <Card>
            <CardContent>
                <Typography variant="h6">{author}</Typography>
                <Rating
                    name="no-value"
                    value={stars}
                    size="large"
                />
                <Typography>{text}</Typography>
            </CardContent>
        </Card>
    );
};

export default Review;
