import { Alert, Typography, Link } from '@mui/material';

const ProfileAlertSection = ({ hasNoDescription, hasNoProfilePicture }: { hasNoDescription: boolean; hasNoProfilePicture: boolean }) => {
    if (!hasNoDescription && !hasNoProfilePicture) {
        return null;
    }
    return (
        <Alert severity="warning">
            <Typography variant="h6">Votre profil est invisible dans les résultats de recherche</Typography>
            <Typography variant="body1">
                Afin d'être visible et de recevoir des missions sur Netlink, vous devez régler les points suivants :
            </Typography>
            {hasNoProfilePicture && (
                <ul>
                    <li>
                        <Typography variant="body1">
                            Ajoutez une photo de profil
                        </Typography>
                    </li>
                </ul>
            )}
            {hasNoDescription && (
                <ul>
                    <li>
                        <Typography variant="body1">
                            Ajoutez une description
                        </Typography>
                    </li>
                </ul>
            )}
        </Alert>
    );
};

export default ProfileAlertSection;
