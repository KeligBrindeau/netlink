import { Typography, Box, FormGroup, FormControlLabel, Switch } from '@mui/material';
import { useState, useEffect } from 'react';

const AvailabilityStatus = ({ isAvailable, onStatusChange }) => {
    const [status, setStatus] = useState(isAvailable || false);

    useEffect(() => {
        setStatus(isAvailable || false);
    }, [isAvailable]);

    const handleSwitchChange = () => {
        const newStatus = !status;
        setStatus(newStatus);
        onStatusChange(newStatus);
    };

    return (
        <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-start"
            color={status ? 'green' : 'red'}
            p={1}
        >
            <Typography variant="body1">
                <FormGroup>
                    <FormControlLabel
                        control={<Switch checked={status} onChange={handleSwitchChange} />}
                        label={status ? 'Disponibilité confirmée' : 'Disponibilité non confirmée'}
                    />
                </FormGroup>
            </Typography>
        </Box>
    );
};

export default AvailabilityStatus;
