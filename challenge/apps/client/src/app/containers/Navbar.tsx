import React, { useState, useRef, useEffect, useContext } from 'react';
import { Button, Box, AppBar, Avatar, IconButton, Toolbar, Typography, TextField, Menu, MenuItem, ClickAwayListener, Grow, Paper } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import axios from 'axios';
import { SearchContext } from '../pages/SearchContext';
import { Link, useNavigate } from 'react-router-dom';
import sha1 from 'js-sha1';
import './navbar.css';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import { AuthContext } from '../context/authContext';
import jwt_decode from 'jwt-decode';

const Navbar = () => {
  const { isAuthenticated, logout, token, user } = useContext(AuthContext);
  const [isOpen, setIsOpen] = useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);
  const [open, setOpen] = useState(false);
  const { setSearchData } = useContext(SearchContext);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchLocation, setSearchLocation] = useState('');
  const [searchTermError, setSearchTermError] = React.useState(false);
  const [searchLocError, setSearchLocError] = React.useState(false);
  const [decodedToken, setDecodedToken] = useState('');
  const navigate = useNavigate();
  var clickQueue: { clickId?: string; page: string; element: string; }[] = [];

  useEffect(() => {
    if (token) {
      setDecodedToken(jwt_decode(token));
    }
  }, [token]);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const displaySearch = () => {
    setIsOpen(!isOpen);
  };

  const handleClose = (event: any) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleLogout = async () => {
    const token = localStorage.getItem('jwt_token')
    try {
      await axios.post(
        `https://samy-saberi.fr/api/logout`,
        null,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch (error) {
      console.error
    }
    logout();
  }

  const search = (keywords: string) =>{
    axios.get('https://samy-saberi.fr/api/search', {
      params:{
        keywords: keywords
      }
    })
      .then(response => {
        setSearchData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }

  const handleSubmit = (event: any) => {
    event.preventDefault();

    if (searchTerm.trim() === '' || searchTerm.length > 80) {
      setSearchTermError(true);
      return;
    }
    else if (searchLocation.length > 80) {
      setSearchLocError(true);
      return;
    }
    setSearchTermError(false);

    const keywords = encodeURIComponent(searchTerm).replace(/%20/g, ' ') + " " + encodeURIComponent(searchLocation).replace(/%20/g, ' ');

    search(keywords);
    setIsOpen(false);
    navigate('/search');
  };

  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);


  useEffect(() => {
    const searchButton = document.querySelector('#searchNavbar');
    searchButton.addEventListener('click', (event) => {
      const timestamp = new Date().getTime();
      const uniqueId = generateUniqueId(timestamp);
      const page = "navbar"
      const element = "searchButtonNav"

      addClickToQueue(page, element);

    });
  })

  function generateUniqueId(timestamp: any) {
    const hash = sha1(timestamp.toString());
    const uniqueId = hash.substring(0, 8);

    return uniqueId;
  }

  function addClickToQueue(page: string, element: string) {
    const click = {
      page: page,
      element: element,
    };
    clickQueue.push(click);
  }


  function saveClick() {

    if (clickQueue.length > 0) {
      axios.post('https://samy-saberi.fr/api/saveClick', clickQueue)
        .then(response => {
          clickQueue = [];
        })
        .catch(error => {
          console.error('Erreur lors de l\'envoie des données:', error);
        });
    }
  }

  setInterval(saveClick, 10000);

  const closeModale = () => {
    setIsOpen(false);
  }

  return (
    <>
      <Box className="navbar">
        <Box flexGrow={1}>
          <AppBar position="fixed">
            <Toolbar>
              <Box className="flexMobileMenu">
                <Box className="menuMobile">
                  <div>
                    <IconButton
                      edge="start"
                      className="navbar-paper"
                      color="inherit"
                      aria-label="menu"
                      ref={anchorRef}
                      aria-controls={open ? 'menu-list-grow' : undefined}
                      aria-haspopup="true"
                      onClick={handleToggle}
                    >
                      <MenuIcon />
                    </IconButton>
                    <Menu
                      id="menu-list-grow"
                      anchorEl={anchorRef.current}
                      open={open}
                      onClose={handleClose}
                      TransitionComponent={Grow}
                      disablePortal
                    >
                      <ClickAwayListener onClickAway={handleClose}>
                        <Paper>
                          <MenuItem component="a" href="/">Accueil</MenuItem>
                          <MenuItem component="a" href="/freelance">Explorer les profils</MenuItem>
                          {!isAuthenticated ? (
                            <>
                              <MenuItem component="a" href="/register">Inscription</MenuItem>
                              <MenuItem component="a" href="/login">Connexion</MenuItem>
                            </>
                          ) : (

                            <>
                              {
                                user.roles.includes('CLIENT') && (
                                  <>
                                    <MenuItem component="a" href="/history-assignment">Missions</MenuItem>
                                    <MenuItem component="a" href="/quotation">Devis</MenuItem>
                                  </>
                                )
                              }
                              {
                                user.roles.includes('FREELANCE') && (
                                  <>
                                    <MenuItem component="a"  href="/assignment">Missions</MenuItem>
                                    <MenuItem component="a" href="/history-quotation">Devis</MenuItem>
                                  </>
                                )
                              }


                              {user.roles.includes('FREELANCE') ? (
                                <MenuItem component="a" href="/profile-freelance">Profil</MenuItem>
                              ) : (
                                <MenuItem component="a" href="/profile">Profil</MenuItem>
                              )
                              }

                              {
                                user.roles.includes('ADMIN') && (
                                  <>
                                    <MenuItem component="a" href="/admin/reviews">Commentaires</MenuItem>
                                    <MenuItem component="a" href="/admin/analytics">Données</MenuItem>
                                  </>
                                )
                              }
                              <MenuItem onClick={handleLogout} sx={{ textDecoration: 'underline', fontWeight: 'bold', color: 'black' }}>
                                <ExitToAppIcon />
                              </MenuItem>
                            </>
                          )}
                        </Paper>
                      </ClickAwayListener>
                    </Menu>
                  </div>
                </Box>

                <Box>
                  <Typography variant="h4" color="primary" className="mobileSiteName">
                    Netlink
                  </Typography>
                </Box>

                <Box>
                  <SearchIcon onClick={displaySearch} />
                </Box>
              </Box>

              <Box className="navbar-flexbox makeStyles-flexBox-4">
                <Link to="/">
                  <Avatar src="./netlink.png" className="navbar-logo" />
                </Link>
                <Button href="/freelance" style={{ color: 'black' }}>Explorer les profils</Button>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                <Button onClick={displaySearch} variant="outlined" className="desktopButtonSearch"><SearchIcon />Trouver un freelance</Button>
                {!isAuthenticated ? (
                  <>
                    <Button href="/register" variant="outlined" className="desktopButtonRegister">Inscription</Button>
                    <Button href="/login" style={{ textDecoration: 'underline', fontWeight: 'bold', color: 'black' }} className="connexion">Connexion</Button>
                  </>
                ) : (
                  <>
                    {
                      user.roles.includes('CLIENT') && (
                        <>
                          <Button style={{ color: 'black' }} href="/history-assignment">Missions</Button>
                          <Button style={{ color: 'black' }} href="/quotation">Devis</Button>
                        </>
                      )
                    }
                    {
                      user.roles.includes('FREELANCE') && (
                        <>
                          <Button style={{ color: 'black' }} href="/assignment">Missions</Button>
                          <Button style={{ color: 'black' }} href="/history-quotation">Devis</Button>
                        </>
                      )
                    }


                    {user.roles.includes('FREELANCE') ? (
                      <Button style={{ color: 'black' }} href="/profile-freelance">Profil</Button>
                    ) : (
                      <Button style={{ color: 'black' }} href="/profile">Profil</Button>
                    )
                    }

                    {
                      user.roles.includes('ADMIN') && (
                        <>
                          <Button style={{ color: 'black' }} href="/admin/reviews">Commentaires</Button>
                          <Button style={{ color: 'black' }} href="/admin/analytics">Données</Button>
                        </>
                      )
                    }
                    <Button onClick={handleLogout} sx={{ textDecoration: 'underline', fontWeight: 'bold', color: 'black' }}>
                      <ExitToAppIcon />
                    </Button>
                  </>
                )}
              </Box >

              <Box flexGrow={1} />
            </Toolbar >
          </AppBar >
        </Box >
        <Box className={`searchBox ${isOpen ? 'searchBoxOpen' : ''}`}>
          <Box className={`navbar-findFreelance makeStyles-findFreelance-7 ${isOpen ? 'findFreelanceOpen' : ''}`}>
            <Box className="findFreelance--title">
              <h1 style={{ textAlign: 'left', marginLeft: '20px' }}>Vous avez des projets, nous avons des experts.</h1>
            </Box>
            <form className="findFreelanceForm" noValidate autoComplete="off" onSubmit={handleSubmit}>
              <TextField
                id="outlined-basic"
                label='Essayez "développeur front-end", "html", "java"'
                variant="outlined" 
                className={searchTermError ? 'error' : 'findFreelanceForm--input skill-inputSearch'}
                onChange={(event) => setSearchTerm(event.target.value)}
                required
              />
              <TextField 
                id="outlined-basic" 
                label="Lieu de la mission (ex: Paris, Lyon)" 
                variant="outlined" 
                className={searchLocError ? 'error' : 'findFreelanceForm--input city-inputSearch'}
                onChange={(event) => setSearchLocation(event.target.value)} 
              />
              <Button type="submit" variant="contained" className="findFreelanceForm--submit" id="searchNavbar">Trouver un freelance</Button>
              <Button variant="contained" className="findFreelanceForm--close" onClick={closeModale}>Fermer</Button>
            </form>
          </Box>
        </Box>
      </Box >
    </>
  );
};

export default Navbar;
