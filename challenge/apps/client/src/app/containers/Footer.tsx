import { Box, Typography } from '@mui/material';
import '../styles/footer.css';


const Footer = () => {

  return (
    <Box className="footer makeStyles-footer-56">
      <Typography variant="subtitle1" align="center" color="textSecondary">
        <a href="http://netlink.com" target="_blank" className="copyright">Copyright © 2023 Netlink.com - Tous droits réservés</a>
      </Typography>
    </Box>
  );
};

export default Footer;
