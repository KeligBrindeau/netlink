import styles from './requests.module.scss';
import React, { useContext, useEffect, useState } from 'react';
import jwt_decode from 'jwt-decode';
import {
  Typography,
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel,
  TableContainer,
  Paper,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Alert,
  IconButton,
  TablePagination,
  tableCellClasses,
  Tooltip
} from '@mui/material';
import Chat from '../containers/chat/Chat';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';
import '../styles/requests/index.css';
import { styled } from '@mui/material/styles';
import { AuthContext } from '../context/authContext';

/* eslint-disable-next-line */
export interface RequestsProps {}

interface Assignment {
  id: string;
  company: string;
  description: string;
  file: string;
}

export function Requests(props: RequestsProps) {
  const [assignments, setAssignments] = useState<Assignment[]>([]);
  const [indexAssignment, setIndexAssignment] = useState<number>(0);
  const [getApproveAssignment, setGetApproveAssignment] = useState<Assignment>();
  const [getRejectAssignment, setGetRejectAssignment] = useState<Assignment>();
  const [openApprovementModal, setOpenApprovementModal] = useState(false);
  const [openRejectModal, setOpenRejectModal] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [errorGet, setErrorGet] = useState<string | null>(null);
  const [csrfToken, setCsrfToken] = useState('');
  const { token } = useContext(AuthContext);
  const [decodedToken, setDecodedToken] = useState('');

  useEffect(() => {
    setDecodedToken(jwt_decode(token));
  }, [token]);

  useEffect(() => {
    if (decodedToken) {
      loadAssignments();
      axios.get('https://samy-saberi.fr/api/token',
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(getToken => {
          setCsrfToken(getToken.data.token);
      })
      .catch (error => {
        console.error('Erreur lors de la récupération du token CSRF :', error);
      });
    }
  }, [decodedToken]);

  function base64ToArrayBuffer(base64: string) {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; ++i) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
  }

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const handleClickOpenApprovement = async (assignment: Assignment, index: number) => {
    setOpenApprovementModal(true);
    setGetApproveAssignment(assignment);
    setIndexAssignment(index);
  };

  const handleClickOpenRejection = async (assignment: Assignment, index: number) => {
    setOpenRejectModal(true);
    setGetRejectAssignment(assignment);
    setIndexAssignment(index);
  };

  const handleCloseApprovement = () => { 
    setOpenApprovementModal(false);
  }

  const handleCloseReject = () => { 
    setOpenRejectModal(false);
  }

  const handlePdfPreview = (assignment: Assignment) => {
    const pdf = base64ToArrayBuffer(assignment.file);
    const pdfBlob = new Blob([pdf], { type: 'application/pdf' });
    const pdfUrl = URL.createObjectURL(pdfBlob);
    window.open(pdfUrl);
  };

  const loadAssignments = async () => {
    try {
      await axios.get(`https://samy-saberi.fr/api/get-assignments-per-freelance/${decodedToken.userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then((response) => {
          setAssignments(response.data);
        })
    }
    catch(error: any) {
      setErrorGet(error.message);
      console.error('Cannot get assignments:', error);
    }
  }

  const approveAssignment = async (id: string | undefined) => {
    try {
      await axios.patch(`https://samy-saberi.fr/api/update-approve-assignment/${id}`,
        {
          status: true
        },
        {
          headers: {
            'x-csrf-token-approve-assignment': csrfToken,
            Authorization: `Bearer ${token}`
          }
        }
      )
      setOpenApprovementModal(false);
      loadAssignments();
    }
    catch(error: any) {
      setError(error.message);
      console.error('Cannot approve assignment:', error);
    }
  }

  const rejectAssignment = async (id: string | undefined) => {
    try {
      await axios.patch(`https://samy-saberi.fr/api/update-reject-assignment/${id}`,
        {
          status: false
        },
        {
          headers: {
            'x-csrf-token-reject-assignment': csrfToken,
            Authorization: `Bearer ${token}`
          }
        }
      )
      setOpenRejectModal(false);
      loadAssignments();
    }
    catch(error: any) {
      setError(error.message);
      console.error('Cannot reject assignment:', error);
    }
  }

  return (
    <div>
      <Grid container spacing={12}>
        <Grid item xs={12}>
          <Typography variant='h5' textAlign={'center'} mb={5}>Mes missions en attente de validation</Typography>
          {errorGet && (
            <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
          )}
          {assignments.length > 0 ? (
            <div>
              <TableContainer component={Paper}>
                  <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                      <TableRow>
                        <StyledTableCell align="center">N°</StyledTableCell>
                        <StyledTableCell align="center">Nom client</StyledTableCell>
                        <StyledTableCell align="center">Prénom client</StyledTableCell>
                        <StyledTableCell align="center">Description mission</StyledTableCell>
                        <StyledTableCell align="center">Actions</StyledTableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {assignments.map((assignment, assignmentIndex) => (
                        <StyledTableRow key={assignment.id}>
                          <StyledTableCell align="center">{assignmentIndex+1}</StyledTableCell>
                          <StyledTableCell align="center">{assignment.company.lastName}</StyledTableCell>
                          <StyledTableCell align="center">{assignment.company.firstName}</StyledTableCell>
                          <StyledTableCell align="center">{decodeURIComponent(assignment.description)}</StyledTableCell>
                          <StyledTableCell align="center">
                            <Tooltip title="Visualiser">
                              <IconButton color="primary" aria-label="eye" onClick={() => handlePdfPreview(assignment)}>
                                <RemoveRedEyeIcon/>
                              </IconButton>
                            </Tooltip>
                            <Tooltip title="Refuser">
                              <IconButton color="error" aria-label="close" onClick={() => handleClickOpenRejection(assignment, assignmentIndex+1)}>
                                <CloseIcon/>
                              </IconButton>
                            </Tooltip>
                            <Tooltip title="Accepter">
                              <IconButton color="success" aria-label="check" onClick={() => handleClickOpenApprovement(assignment, assignmentIndex+1)}>
                                <CheckIcon/>
                              </IconButton>
                            </Tooltip>
                          </StyledTableCell>
                        </StyledTableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
            </div>
          ) : (
            <div>
              <Typography variant='h5' textAlign={'center'}>Pas de données</Typography>
            </div>
          )}
          <div style={{display: "flex", justifyContent: "center", marginTop: "2%"}}>
            <Button variant="text" color="primary" href="/history-assignment">Voir les missions en cours</Button>
          </div>
        </Grid>
      </Grid>
        <Dialog open={openApprovementModal}>
          <DialogTitle sx={{textAlign: 'center'}}>Êtes-vous sûr de vouloir accepter la mission N° {indexAssignment} ?</DialogTitle>
          <DialogContent className='modal-box-assignment-pending'>
          <Alert severity="info">La messagerie pour cette mission sera accessible</Alert>
            {error && (
              <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
            )}
          </DialogContent>
          <DialogActions className='button-action-assignment-pending'>
            <Button onClick={handleCloseApprovement} color="inherit" variant="contained">Annuler</Button>
            <input type="hidden" name="x-csrf-token-approve-assignment" value={csrfToken} />
            <Button type="submit" variant="contained" color="success" onClick={() => approveAssignment(getApproveAssignment?.id)}>Valider</Button>
          </DialogActions>
        </Dialog>

        <Dialog open={openRejectModal}>
          <DialogTitle sx={{textAlign: 'center'}}>Êtes-vous sûr de vouloir refuser la mission N° {indexAssignment} ?</DialogTitle>
          <DialogContent className='modal-box-assignment-pending'>
            <Alert severity="info">Il ne vous sera pas possible de discuter avec le client pour cette mission</Alert>
            {error && (
              <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
            )}
          </DialogContent>
          <DialogActions className='button-action-assignment-pending'>
            <Button onClick={handleCloseReject} color="inherit" variant="contained">Annuler</Button>
            <input type="hidden" name="x-csrf-token-reject-assignment" value={csrfToken} />
            <Button type="submit" variant="contained" color="error" onClick={() => rejectAssignment(getRejectAssignment?.id)}>Refuser</Button>
          </DialogActions>
        </Dialog>
    </div>
  );
}

export default Requests;
