import { useContext } from "react";
import { AuthContext } from "../context/authContext";
import { Route, Routes } from "react-router-dom";
import Login from "../pages/auth/Login";
import Register from "../pages/auth/Register";
import RegisterClient from "../pages/auth/RegisterClient";
import RegisterFreelance from "../pages/auth/RegisterFreelance";
import Freelance from "../freelance/freelance";
import DetailFreelance from "../freelance/detailFreelance";
import ConfirmationPage from "../pages/auth/AccountConfirmation";
import SearchResult from "../pages/SearchResult";
import Home from "../pages/Home";
import { RouteGuard } from "../guards/RouteGuard";
import Layout from "../containers/Layout";
import Profile from "../pages/auth/Profile";
import PaymentForm from "../pages/payment";
import Messagerie from "../messagerie/messagerie";
import Requests from "../requests/requests";
import Quotations from "../quotations/quotations";
import History from "../history/history";
import HistoryQuotation from "../history-quotation/history-quotation";
import Kpi from "../pages/admin/kpi";
import ProfileFreelance from "../pages/auth/ProfileFreelance";
import Reviews from "../pages/admin/Reviews";
import Kanban from "../pages/kanban/kanban";

export const Router: React.FC = () => {
    const { isAuthenticated, user } = useContext(AuthContext);
    return (
        <Layout>
            <Routes>
                {/* Routes publiques */}
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/register-client" element={<RegisterClient />} />
                <Route path="/register-freelance" element={<RegisterFreelance />} />
                <Route path="/freelance" element={<Freelance />} />
                <Route path="/freelancer/:id" element={<DetailFreelance />} />
                <Route path="/confirm/:param" element={<ConfirmationPage />} />
                <Route path="/search" element={<SearchResult />} />
                <Route path="/" element={<Home />} />

                {/* Route ou l'user doit etre co*/}
                <Route
                    element={
                        <RouteGuard
                            isRouteAccessible={isAuthenticated}
                            redirectRoute={'/login'}
                        />
                    }
                >
                    <Route path="/messaging/:id" element={<Messagerie />} />
                    <Route path="/history-assignment" element={<History />} />
                    <Route path="/history-quotation" element={<HistoryQuotation />} />
                    <Route path="/kanban/:id" element={<Kanban />} />
                </Route>

                {/* User co et ADMIN ou CLIENT */}
                <Route
                    element={
                        <RouteGuard
                            isRouteAccessible={isAuthenticated && (user.roles.includes("ADMIN") || user.roles.includes("CLIENT"))}
                            redirectRoute={'/login'}
                        />
                    }
                >
                    <Route path="/profile" element={<Profile />} />
                </Route>
            
                {/* Role client */}
                <Route
                    element={
                        <RouteGuard
                            isRouteAccessible={isAuthenticated && user.roles.includes("CLIENT")}
                            redirectRoute={'/'}
                        />
                    }
                >
                    <Route path="/quotation" element={<Quotations />} />
                    <Route path="/payment/:id" element={<PaymentForm />} />
                </Route>

                {/* User co et freelance */}
                <Route
                    element={
                        <RouteGuard
                            isRouteAccessible={isAuthenticated && user.roles.includes("FREELANCE")}
                            redirectRoute={'/'}
                        />
                    }
                >
                    <Route path="/assignment" element={<Requests />} />
                    <Route path="/profile-freelance" element={<ProfileFreelance />} />
                </Route>

                {/* User co et ADMIN */}
                <Route
                    element={
                        <RouteGuard
                            isRouteAccessible={isAuthenticated && user.roles.includes("ADMIN")}
                            redirectRoute={'/'}
                        />
                    }
                >
                    <Route path="/admin/analytics" element={<Kpi />} />
                    <Route path="/admin/reviews" element={<Reviews />} />
                </Route>
                
            </Routes>

        </Layout>
    )
}
