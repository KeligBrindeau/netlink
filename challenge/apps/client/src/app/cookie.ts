import sha1 from "js-sha1";

export function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length === 2) {
    return parts.pop().split(";").shift();
  }
}

export function setCookie(name, value, hours) {
  var expires = "";
  if (hours) {
      var date = new Date();
      date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
      expires = "; expires=" + date.toString();
  }
  document.cookie = name + "=" + value + expires + "; path=/";
}

export function generateUniqueId(timestamp: any) {
  const hash = sha1(timestamp.toString());
  const uniqueId = hash.substring(0, 8); 

  return uniqueId;
}