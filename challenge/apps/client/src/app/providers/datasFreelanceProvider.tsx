import { ReactNode } from "react";
import { DatasFreelanceContext } from "../context/datasFreelanceContext";

interface DatasFreelanceProviderProps {
    children: ReactNode;
}

const DatasFreelanceProvider: React.FC<DatasFreelanceProviderProps> = ({ children }) => {
    const skills: string[] = [
        'php',
        'javascript',
        'symfony',
        'html',
        'css',
        'nest',
        'laravel',
        "c",
        "c++",
        "python",
        "threejs",
        "swift",
        "sql"
    ];

    const cities: string[] = [
        "paris",
        "montpellier",
        "lyon",
        "marseille",
        "annecy",
        "nice",
        "toulouse",
        "nantes",
        "strasbourg",
        "bordeaux",
        "lille",
        "rennes",
        "reims",
        "toulon",
        "grenoble",
        "angers",
        "quimper",
        "dijon",
    ];

    const jobs: string[] = [
        "front-end",
        "back-end",
        "fullstack",
        "devops",
        "mobile"
    ];

    const value = {
        skills,
        cities,
        jobs
    }

    return (
        <DatasFreelanceContext.Provider value={value}>
            {children}
        </DatasFreelanceContext.Provider>
    );
};

export default DatasFreelanceProvider;