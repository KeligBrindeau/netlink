import React from "react";
import { AuthContext } from "../context/authContext";
import { DecodedTokenType, UserContextType, AuthContextProps } from "../types/auth";
import jwt_decode from "jwt-decode"
import { useLocalStorage } from "react-use";
import { useNavigate } from "react-router-dom";

export const AuthContextProvider = ({ children }: AuthContextProps) => {
    const navigate = useNavigate();
    const [tokenLs, setToken, removeToken] = useLocalStorage("jwt_token", "", { raw: true });
    const token = tokenLs || "";
    const defaultUser = React.useMemo(() => {
        if (tokenLs) {
            const decodedToken = jwt_decode<DecodedTokenType>(tokenLs);
            return {
                id: decodedToken.userId,
                firstName: decodedToken.firstName,
                lastName: decodedToken.lastName,
                email: decodedToken.email,
                birthDate: decodedToken.birthDate || null,
                roles: decodedToken.roles,
                picture: decodedToken.picture || "",
                status: decodedToken.status || null,
                job: decodedToken.job || "",
                skills: decodedToken.skills || [],
                description: decodedToken.description || "",
                city: decodedToken.city || "",
                hourlyRate: decodedToken.hourlyRate || null
            };
        }
        return {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            birthDate: null,
            roles: [],
            picture: "",
            status: null,
            job: "",
            skills: [],
            description: "",
            city: "",
            hourlyRate: null
        }
    }, [tokenLs])
    const [user, setUser] = React.useState<UserContextType>(defaultUser);

    const handleUpdateUser = React.useCallback((token: string) => {
        setToken(token);
        const decodedToken = jwt_decode<DecodedTokenType>(token);
        setUser({
            id: decodedToken.userId,
            firstName: decodedToken.firstName,
            lastName: decodedToken.lastName,
            email: decodedToken.email,
            birthDate: decodedToken.birthDate || null,
            roles: decodedToken.roles,
            picture: decodedToken.picture || "",
            status: decodedToken.status || null,
            job: decodedToken.job || "",
            skills: decodedToken.skills || [],
            description: decodedToken.description || "",
            city: decodedToken.city || "",
            hourlyRate: decodedToken.hourlyRate || null
        })
    }, [setUser])

    const logout = React.useCallback(() => {
        removeToken();
        setUser({
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            birthDate: null,
            roles: [],
            picture: "",
            status: null,
            job: "",
            skills: [],
            description: "",
            city: "",
            hourlyRate: null
        })
        localStorage.clear();
        navigate("/");
    }, [setUser])

    const value = React.useMemo(() => ({
        token,
        isAuthenticated: user.id !== "",
        user,
        login: handleUpdateUser,
        logout
    }), [token, user, handleUpdateUser])

    return (
        <AuthContext.Provider value={value} >
            {children}
        </AuthContext.Provider>
    )
}