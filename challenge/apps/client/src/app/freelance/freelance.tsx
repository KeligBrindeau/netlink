import './freelance.scss';
import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Chat from '../containers/chat/Chat';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Box, Checkbox, FormControlLabel, Grid, TextField, Rating, Snackbar } from '@mui/material';
import DetailFreelance from './detailFreelance';
import { getCookie, setCookie, generateUniqueId } from '../cookie.js'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import '../styles/freelance/survey.scss';
import { useNavigate } from 'react-router-dom';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import Footer from '../containers/Footer';
import { AuthContext } from '../context/authContext';

/* eslint-disable-next-line */
export interface FreelanceProps { }

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export function Freelance(props: FreelanceProps) {
  const { token } = useContext(AuthContext);
  
  var cardStyle = {
    height: '20vw'
  }

  const [selectedFilter, setSelectedFilter] = useState("");
  const jwtToken = localStorage.getItem('jwt_token');
  const [freelancers, setFreelancers] = useState([]);
  let visitRecorded = false;
  var cookie = getCookie('pageVisit');
  let inactivityTimeout;
  const [open, setOpen] = React.useState(false);
  const [ratingValue, setRatingValue] = useState(null);
  var mouseMovementsQueue: { start_x: number; start_y: number; end_x: number; end_y: number; type: string; duration: number; }[] = [];
  var startPositionX : number | null = null;
  var startPositionY : number | null = null;
  var startTime : number | null = null;

  useEffect(() => {
    axios.get('https://samy-saberi.fr/api/getFreelancers')
      .then(response => {
        setFreelancers(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }, []);


  //ANALYTICS

  if(cookie === undefined){
    cookie = generateUniqueId(new Date().getTime());
    setCookie('pageVisit', cookie, 1);

    var startVisit = Date.now();
  }

  function resetInactivityTimer() {
    clearTimeout(inactivityTimeout);
    inactivityTimeout = setTimeout(handleInactivity, 900000);
  }

  function handleInactivity() {
    if(visitRecorded === false && startVisit){
      recordVisit()
    }
    
  }

  document.addEventListener('mousemove', resetInactivityTimer);
  document.addEventListener('click', resetInactivityTimer);

  window.addEventListener("beforeunload", (event) => {

    if (startVisit && visitRecorded === false) {
      recordVisit();
    }
  })

  visitRecorded = false;

  function recordVisit() {
    const endTime = Date.now();
    const visitDuration = Math.round((endTime - startVisit) / 1000);
    visitRecorded = true;

    axios.post('https://samy-saberi.fr/api/saveVisit',{
      page: "freelance",
      visitDuration: visitDuration,
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données:', error);
    });
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const submitSurvey = () => {

    axios.post('https://samy-saberi.fr/api/saveSurvey', {
      rate: ratingValue,
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données:', error);
    });

  }

  function addMouseMovementToQueue(startX: number, startY: number, endX: number, endY: number, type: string, duration: number) {
    const mouseMovement = {
      start_x: startX,
      start_y: startY,
      end_x: endX,
      end_y: endY,
      type: type,
      duration: duration,
    };
    mouseMovementsQueue.push(mouseMovement);
  }

  
  document.addEventListener('mousemove', (event) => {
    const { clientX, clientY } = event;
    const type = "mousemove";

    if (startPositionX === null && startPositionY === null) {
      // Premier mouvement de souris
      startPositionX = clientX;
      startPositionY = clientY;
      startTime = new Date().getTime();
    }

    const endPositionX = clientX;
    const endPositionY = clientY;

    const distanceThreshold = 40; 
    const distance = Math.sqrt(
      Math.pow(endPositionX - startPositionX!, 2) +
      Math.pow(endPositionY - startPositionY!, 2)
    );

    if (distance >= distanceThreshold) {
      const endTime = new Date().getTime();
      const duration = endTime - (startTime as number);

      
      if (duration < 200 || duration > 10000) {
        return;
      }

      addMouseMovementToQueue(startPositionX!, startPositionY!, endPositionX, endPositionY, type, duration);
  
      startPositionX = null;
      startPositionY = null;
      startTime = null;
      
    }
  });

  function saveMouseEvent(){

    if(mouseMovementsQueue.length > 0){
      axios.post('https://samy-saberi.fr/api/mouseEvent', mouseMovementsQueue)
      .then(response => {
        mouseMovementsQueue = [];
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });

    }
  }

  setInterval(saveMouseEvent, 30000);


  const [loading, setLoading] = useState(true);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios.get('https://samy-saberi.fr/api/getFreelancers', {
      headers: {
        Authorization: `Bearer ${jwtToken}`
      }
    })
    .then(response => {
      setFreelancers(response.data);
      setLoading(false);
    })
    .catch(error => {
      setLoading(false);
      console.error('Erreur lors de la récupération des utilisateurs:', error);
    });
  }, []);

  const skillsOptions = ['php', 'javascript', 'symfony', 'html', 'css', 'nest', 'laravel'];

  const citiesFilter = [
    "paris",
    "montpellier",
    "lyon",
    "marseille",
    "toulouse",
    "nantes",
    "bordeaux",
    "lille",
    "rennes",
    "reims",
  ]

  const search = (keywords: string) =>{
    axios.get('https://samy-saberi.fr/api/getFreelancers', {
      params:{
        keywords: keywords
      }
    })
    .then(response => {
      setFreelancers(response.data);
    })
    .catch(error => {
      console.error('Erreur lors de la récupération des utilisateurs:', error);
    });
  }

  const handleSubmit = (event: any) => {
    event.preventDefault(); 
    search(selectedFilter);
    setIsOpen(false);
  }

  const resetFilter = () => {
    setSelectedFilter("");
  }

  const displayFilter = () => {
    setIsOpen(!isOpen);
  }

  const closeModale = () => {
    setIsOpen(false);
  }

  if (loading) {
    return <img src="./home/loading.gif" className="loading-results"/>;
  }

  return (
    <div>
      <Button variant="contained" className="filter-button" onClick={displayFilter}>Filtres</Button>
      <div className="freelance-container">
        <div className="filter">
          <form className="" noValidate autoComplete="off">
            <h2>Filtrer par :</h2>
            <h3>Compétences :</h3>
            
            {skillsOptions.map((skill) => (
              <FormControlLabel
                  key={skill}
                  control={
                    <Checkbox
                      name="skills"
                      value={skill}
                      onChange={(e) => {
                        const { checked, value } = e.target;
                        if (checked) {
                          setSelectedFilter((prevFilter) => `${prevFilter} ${value}`);
                        } else {
                          setSelectedFilter((prevFilter) =>
                            prevFilter
                              .split(' ')
                              .filter((keyword) => keyword !== value)
                              .join(' ')
                          );
                        }
                      }}
                    />
                  }
                  label={skill}
              />
            ))}

            <hr />

            <h3>Ville : </h3>
            {citiesFilter.map((city) => (
              <FormControlLabel
                  key={city}
                  control={
                    <Checkbox
                      name="skills"
                      value={city}
                      onChange={(e) => {
                        const { checked, value } = e.target;
                        if (checked) {
                          setSelectedFilter((prevFilter) => `${prevFilter} ${value}`);
                        } else {
                          setSelectedFilter((prevFilter) =>
                            prevFilter
                              .split(' ')
                              .filter((keyword) => keyword !== value)
                              .join(' ')
                          );
                        }
                      }}
                    />
                  }
                  label={city}
              />
            ))}
            <Button type="submit" variant="contained" className="reset-filter"  color="error" onClick={resetFilter}>Effacer</Button>
            <Button type="submit" variant="outlined" className="submit-filter" onClick={handleSubmit}>Filtrer</Button>
          </form>
        </div>          

        <Grid container spacing={2} sx={{ margin: 'auto', maxWidth: 1200 }}>
          {freelancers.length != 0 ?
          freelancers.map(freelancer => (
            <Grid item key={freelancer.id} xs={12} sm={6} md={4} lg={3}>
              <Card sx={{ minWidth: 200, minHeight: {xs: '250px', md: 'auto'} }} style={cardStyle}>
                <CardContent>
                  <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    {freelancer.lastName} {freelancer.firstName}
                  </Typography>
                  <Typography variant="h5" component="div">
                    {freelancer.job}
                  </Typography>
                  <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    {freelancer.skills ? Object.keys(freelancer.skills).slice(0, 3).map(skillKey => (
                      <Button key={skillKey} color="primary" disabled>{freelancer.skills[skillKey]}</Button>
                    )) : ""}
                  </Typography>
                  <Typography variant="body2">
                    {freelancer.description ? freelancer.description.length < 100 ? freelancer.description: freelancer.description.slice(0, 35) + '...'  : "" }
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" href={`freelancer/${freelancer.id}`}>Voir plus</Button>
                </CardActions>
              </Card>
            </Grid>
          ))
          :
            <>
              <h1 className="search-result-error-title">Aucun résultat pour cette recherche !</h1>

              <img src="./search-error.webp" className="search-result-error-img"/>
            </>
          }
          {token && (
            <Chat />
          )}
        </Grid>

        <Box className={`searchBox ${isOpen ? 'searchBoxOpen' : ''}`}>
          <Box className={`filter-box ${isOpen ? 'findFreelanceOpen' : ''}`}>
          <Button type="submit" variant="contained" className="close-filter"  color="error" onClick={closeModale}>Fermer</Button>
            <form className="" noValidate autoComplete="off" onSubmit={handleSubmit}>
              <h2>Filtrer par :</h2>
              <h3>Compétences :</h3>
              
              {skillsOptions.map((skill) => (
                <FormControlLabel
                    key={skill}
                    control={
                      <Checkbox
                        name="skills"
                        value={skill}
                        onChange={(e) => {
                          const { checked, value } = e.target;
                          if (checked) {
                            setSelectedFilter((prevFilter) => `${prevFilter} ${value}`);
                          } else {
                            setSelectedFilter((prevFilter) =>
                              prevFilter
                                .split(' ')
                                .filter((keyword) => keyword !== value)
                                .join(' ')
                            );
                          }
                        }}
                      />
                    }
                    label={skill}
                />
              ))}

              <hr />

              <h3>Ville : </h3>
              {citiesFilter.map((city) => (
                <FormControlLabel
                    key={city}
                    control={
                      <Checkbox
                        name="skills"
                        value={city}
                        onChange={(e) => {
                          const { checked, value } = e.target;
                          if (checked) {
                            setSelectedFilter((prevFilter) => `${prevFilter} ${value}`);
                          } else {
                            setSelectedFilter((prevFilter) =>
                              prevFilter
                                .split(' ')
                                .filter((keyword) => keyword !== value)
                                .join(' ')
                            );
                          }
                        }}
                      />
                    }
                    label={city}
                />
              ))}
              <Button type="submit" variant="contained" className="reset-filter"  color="error" onClick={resetFilter}>Effacer</Button>
              <Button type="submit" variant="outlined" className="submit-filter" onClick={handleSubmit}>Filtrer</Button>
            </form>
          </Box>
        </Box>

        <button className="survey" onClick={handleClickOpen}>&#9998;</button>
        <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
        >
        <DialogTitle>{"Enquête de satisfaction"}</DialogTitle>
          <form noValidate autoComplete='off' onSubmit={submitSurvey}>
          <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Votre avis compte ! Partagez-le et aidez-nous à améliorer votre expérience sur notre site.
          </DialogContentText>
          <Typography component="legend" className="survey-legend">Votre note :</Typography>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Rating 
              name="no-value" 
              value={ratingValue} 
              onChange={(event, value) => setRatingValue(value)}
              className="survey-stars"
            />
          </div>
          </DialogContent>
          <DialogActions>
          <Button onClick={handleClose}>Annuler</Button>
          <Button type="submit">Soumettre</Button>
          </DialogActions>
          </form>
        </Dialog>
        </div>

        <Footer />
      </div>
  );
}

export default Freelance;

