import React, { useEffect, useState, ChangeEvent, useContext } from 'react';
import axios from 'axios';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Chat from '../containers/chat/Chat';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Box, Alert, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, List, ListItem, ListItemText, Stack, TextField, Rating, Slide, Avatar } from '@mui/material';
import { TransitionProps } from '@mui/material/transitions';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';
import Footer from '../containers/Footer';
import '../styles/freelance/index.css';
import { AuthContext } from '../context/authContext';
import Review from '../containers/Review';
import { UserType } from '../types/auth';

export interface FreelanceProps { }

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export function DetailFreelance(props: FreelanceProps) {
  const [freelancer, setFreelancer] = useState<UserType>();
  const [numberOfSkills, setNumberOfSkills] = useState(0);
  const [openAssignmentModal, setOpenAssignmentModal] = useState(false);
  const [descriptionAssignment, setDescriptionAssignment] = useState<string>('');
  const [uploadedFile, setUploadedFile] = useState<File>();
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isUnSubmitted, setIsUnSubmitted] = useState(false);
  const [formErrors, setFormErrors] = useState({ description: '', file: '' });
  const [csrfToken, setCsrfToken] = useState('');
  const { token, user } = useContext(AuthContext);
  const [open, setOpen] = React.useState(false);
  const [ratingValue, setRatingValue] = useState(null);
  const [reviews, setReviews] = useState<Reviews[]>([]);

  interface Reviews {
    id: string;
    description: string;
    ratedUser: UserType;
    ratingUser: UserType;
    rate: number;
    isDisplayed: boolean;
  }

  useEffect(() => {
    if (freelancer) {
      loadReviews(freelancer.id);
    }
  }, [freelancer]);

  useEffect(() => {
    //get id from url
    const id = window.location.pathname.split('/')[2];
    axios.get('https://samy-saberi.fr/api/getFreelancer/'+id)
      .then(response => {
        const freelancer = response.data;
        freelancer.picture = getUrlImg(freelancer);
        setFreelancer(freelancer);
        setNumberOfSkills(Object.keys(response.data.skills).length);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
    if(token) {
        axios.get('https://samy-saberi.fr/api/token',
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then(getToken => {
          setCsrfToken(getToken.data.token);
        })
        .catch(error => {
          console.error('Erreur lors de la récupération du token CSRF :', error);
        })
    }
  }, []);

  function base64ToArrayBuffer(base64: string) {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; ++i) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
  }

  const getUrlImg = (userData) => {
    if (userData.picture) {
      const img = base64ToArrayBuffer(userData.picture);
      const imgBlob = new Blob([img]);
      const imgUrl = URL.createObjectURL(imgBlob);
      return imgUrl;
    } else {
      return userData.picture;
    }
  };

  const loadReviews = async (idFreelance: string) => {
    try {
      await axios.get(`https://samy-saberi.fr/api/get-reviews/${idFreelance}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then((response) => {
          const reviews = response.data.datas
          setReviews(reviews);
        })
    }
    catch (error: any) {
      console.error('Cannot get reviews:', error);
    }
  }

  if (!freelancer) {
    return <div>Loading...</div>;
  }

  const handleOpenAssignmentModal = () => {
    setOpenAssignmentModal(true);
  }

  const handleCloseAssignmentModal = () => {
    setOpenAssignmentModal(false);
    setFormErrors({ description: '', file: '' });
    setDescriptionAssignment('');
    setUploadedFile(undefined);
  };

  const handleUploadAssignment = async (event: { preventDefault: () => void; }) => {
    event.preventDefault()
    const errors = {
      description: '',
      file: '',
    };

    if (!uploadedFile) {
      console.log("No file selected");
      return;
    }

    const encodeDescription = encodeURIComponent(descriptionAssignment).replace(/%20/g, ' ')

    setFormErrors(errors);

    try {
      const formData = new FormData();
      const headers = {
        'Content-Type': 'multipart/form-data',
        'x-csrf-token': csrfToken,
        Authorization: `Bearer ${token}`
      };
      formData.append('x-csrf-token', csrfToken);
      formData.append('description', encodeDescription);
      formData.append('file', uploadedFile);
      formData.append('company', user.id);
      formData.append('freelance', freelancer.id);
      const response = await axios.post(
        'https://samy-saberi.fr/api/create-assignment',
        formData,
        { headers },
      );
      setOpenAssignmentModal(false);
      setFormErrors({ description: '', file: '' });
      setDescriptionAssignment('');
      setUploadedFile(undefined);
      setIsSubmitted(true);
      setTimeout(() => {
        setIsSubmitted(false);
      }, 3000);
    } catch (error) {
      console.error("Cannot create an assignment: ", error);
    }
  };


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const submitSurvey = () => {

    axios.post('https://samy-saberi.fr/api/saveSurvey', {
      rate: ratingValue,
    })
      .then(response => {
      })
      .catch(error => {
        console.error('Erreur lors de l\'envoi des données:', error);
      });

  }


  return (
    <>
      <Container>
        {isSubmitted && (
          <Alert severity="success">La fiche de poste a été envoyée avec succès !</Alert>
        )}
        {isUnSubmitted && (
          <Alert severity="error">La fiche de poste n'a pas pu être envoyée ! Veuillez réessayer ultérieurement.</Alert>
        )}
        <Grid container spacing={3} justifyContent="center" alignItems="stretch">
          <Grid item xs={12}>
            <Grid container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={3} >
              <Grid item xs={12} sm={2}>
                <Avatar
                  alt="Profile Picture"
                  src={freelancer.picture}
                  sx={{ width: '100%', height: 'auto', aspectRatio: '1/1', margin: '20px auto' }}
                />
              </Grid>
              <Grid item xs={12} sm={6} >
                <Typography variant="h4" component="h2">
                  {freelancer.lastName} {freelancer.firstName}
                </Typography>
                <Typography variant="body1">
                  <Box display="flex" alignItems="center">
                    <WorkOutlineIcon sx={{ marginRight: '8px' }} />
                    {freelancer.job}
                  </Box>
                </Typography>

                <Typography variant="body1">
                  <Box display="flex" alignItems="center">
                    <LocationOnOutlinedIcon /> Localisé(e) à {freelancer.city}
                  </Box>
                </Typography>
                <Button variant="contained" size="large" sx={{ mt: 2, mb: 1 }}>
                  Tarif indicatif
                  <br />
                  {freelancer.hourlyRate}€/jour
                </Button>
              </Grid>
              {user && user.roles.includes('CLIENT') && (
                <Grid item xs={12} sm={2}>
                  <Button variant="contained" onClick={handleOpenAssignmentModal}>Proposer une mission</Button>
                  <Typography variant="body2" sx={{ mt: 1 }}>
                    La mission ne démarrera que si le freelance l'accepte.
                  </Typography>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <hr />
      <Container>
        <br />
        <Grid container spacing={3} justifyContent="flex-start" alignItems="stretch">
          <Grid item xs={12} sm={4}>
            <Card sx={{ minWidth: 200 }}>
              <CardContent>
                <Typography variant="h5" component="div">
                  Localisation et déplacement
                </Typography>
                <Typography variant="body2">
                  <Grid container direction="row" alignItems="center">
                    <Grid item xs={1} sm={1}>
                      <LocationOnOutlinedIcon />
                    </Grid>
                    <Grid item xs={10} sm={5}>
                      <b>Localisation</b>
                    </Grid>
                    <Grid item xs={1} sm={1} />
                    <Grid item xs={10} sm={5}>
                      {freelancer.city}
                    </Grid>
                  </Grid>
                  <br />
                  <br />
                  <Grid container direction="row" alignItems="center">
                    <Grid item xs={1} sm={1}>
                      <LocationOnOutlinedIcon />
                    </Grid>
                    <Grid item xs={10} sm={5}>
                      <b>Télétravail</b>
                    </Grid>
                    <Grid item xs={1} sm={1} />
                    <Grid item xs={10} sm={5}>
                      Effectue ses missions majoritairement à distance
                    </Grid>
                  </Grid>
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={12} sm={8}>
            <Card sx={{ minWidth: 200 }}>
              <CardContent>
                <Typography variant="h5" component="div">
                  Compétences ({numberOfSkills})
                </Typography>
                <Typography variant="body2" sx={{ mt: 2 }}>
                  <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                    {Object.keys(freelancer.skills).map(skillKey => (
                      <Grid key={skillKey} item xs={12} sm={6}>
                        <Button color="primary">{freelancer.skills[skillKey]}</Button>
                      </Grid>
                    ))}
                  </Grid>

                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <br></br>
        <Grid container spacing={3} justifyContent="flex-start" alignItems="stretch">
          <Grid item xs={12} sm={4} />
          <Grid item xs={12} sm={8}>
            <Card sx={{ minWidth: 200 }}>
              <CardContent>
                <Typography variant="h5" component="div">
                  {freelancer.firstName} en quelques mots
                </Typography>
                <Typography variant="body2" sx={{ mt: 2 }}>
                  {freelancer.description}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>

        <div>
          <Typography variant="h5" component="div">
            Commentaires
          </Typography>
          {reviews.map((review, index) => (
            <Box key={index} sx={{ marginBottom: '20px' }}>
              <Review key={index} author={`${review.ratingUser.firstName} ${review.ratingUser.lastName}`} text={review.description} stars={review.rate} />
            </Box>
          ))}
        </div>
      </Container>

      <Dialog open={openAssignmentModal} onClose={handleCloseAssignmentModal}>
        <DialogTitle sx={{ textAlign: 'center' }}>Envoyez la fiche de poste à {freelancer.lastName} {freelancer.firstName}</DialogTitle>
        <DialogContent className='modal-box-form'>
          <Alert severity="info">Le freelance verra votre fiche de poste et vous contactera directement s'il accepte la mission.</Alert>
          <TextField
            autoFocus
            margin="dense"
            id="outlined-textarea"
            label="Description"
            placeholder='Description de la mission'
            type="text"
            multiline
            variant="standard"
            style={{ width: '100%' }}
            required
            error={!descriptionAssignment || descriptionAssignment.length < 3 || descriptionAssignment.length > 100}
            helperText='La description doit avoir entre 3 et 100 caractères.'
            value={descriptionAssignment}
            onChange={(event: ChangeEvent<HTMLInputElement>) => setDescriptionAssignment(event.target.value)}
          />
          {formErrors.description && <Typography variant="subtitle1" className='error-value'>{formErrors.description}</Typography>}
          <TextField
            autoFocus
            margin="dense"
            id="file"
            label="Fichier"
            type="file"
            fullWidth
            variant="standard"
            required
            error={!uploadedFile || uploadedFile.type !== 'application/pdf'}
            helperText='Veuillez sélectionner un fichier PDF.'
            onChange={(event: ChangeEvent<HTMLInputElement>) => setUploadedFile(event.target.files![0])}
          />
          {formErrors.file && <Typography variant="subtitle1" className='error-value'>{formErrors.file}</Typography>}
        </DialogContent>
        <DialogActions className='button-action'>
          <Button variant="contained" onClick={handleCloseAssignmentModal}>Annuler</Button>
          <input type="hidden" name="x-csrf-token" value={csrfToken} />
          <Button type="submit" variant="contained" disabled={descriptionAssignment === '' || uploadedFile === undefined} onClick={handleUploadAssignment}>Envoyer</Button>
        </DialogActions>
      </Dialog>

      {token && (
        <Chat />
      )}

      <button className="survey" onClick={handleClickOpen}>&#9998;</button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Enquête de satisfaction"}</DialogTitle>
        <form noValidate autoComplete='off' onSubmit={submitSurvey}>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Votre avis compte ! Partagez-le et aidez-nous à améliorer votre expérience sur notre site.
            </DialogContentText>
            <Typography component="legend" className="survey-legend">Votre note :</Typography>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Rating
                name="no-value"
                value={ratingValue}
                onChange={(event, value) => setRatingValue(value)}
                className="survey-stars"
              />
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Annuler</Button>
            <Button type="submit">Soumettre</Button>
          </DialogActions>
        </form>
      </Dialog>
      {/* </Container > */}

      <Footer />
    </>
  )
}
export default DetailFreelance;
