import { render } from '@testing-library/react';

import Messagerie from './messagerie';

describe('Messagerie', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Messagerie />);
    expect(baseElement).toBeTruthy();
  });
});
