import React, { useEffect, useState, ChangeEvent, useContext } from 'react';
import { io, Socket } from 'socket.io-client';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { WebsocketContext } from '../context/WebsocketContext';
import moment, { Moment } from 'moment';
import '../styles/messagerie/index.css';
import {
  Typography,
  TextField,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Alert,
  Fab,
  Grid,
  Divider,
  List,
  ListItem,
  ListItemText
} from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import EuroIcon from '@mui/icons-material/Euro';
import { AuthContext } from '../context/authContext';
import { useNavigate } from 'react-router-dom';

export interface MessagerieProps {}

type Message = {
  content: string;
  transmitter: string;
  receiver: string;
  assignment: string;
  timestamp: Date;
  csrfTokenMessaging: string;
}

type MessagePayload = {
  content: string;
  msg: string;
  timestamp: Date;
};

export function Messagerie(props: MessagerieProps) {
  const [open, setOpen] = useState(false);
  const [assignmentObject, setAssignmentObject] = useState({} as any);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [descriptionQuotation, setDescriptionQuotation] = useState('');
  const [nbdayQuotation, setNbdayQuotation] = useState<number>(0);
  const [amountQuotation, setAmountQuotation] = useState<number>(0);
  const inputRef = React.useRef();
  const [formErrorsQuotation, setFormErrorsQuotation] = useState({description: '', nbday: '', amount: '' });
  const [csrfTokenQuotation, setCsrfTokenQuotation] = useState('');
  const [csrfTokenMessaging, setCsrfTokenMessaging] = useState('');
  const { token } = useContext(AuthContext);
  const [decodedToken, setDecodedToken] = useState('');
  const [value, setValue] = useState('');
  const [messages, setMessages] = useState<MessagePayload[]>([]);
  const [allMessages, setAllMessages] = useState<Message[]>([]);;
  const socket = useContext(WebsocketContext);
  const [errorGet, setErrorGet] = useState<string | null>(null);
  const navigate = useNavigate();
  let receiver = useState<String>('');

  useEffect(() => {
    socket.on('connect', () => {
      console.log('Connected!');
    });
    socket.on('clientIdToken', (csrfToken) => {
      setCsrfTokenMessaging(csrfToken.token);
    })
    socket.on('onMessage', (newMessage: MessagePayload) => {
      setMessages((prev) => [...prev, newMessage]);
    });

    const assignmentId = window.location.pathname.split('/')[2];

    setDecodedToken(jwt_decode(token));
    axios.get('https://samy-saberi.fr/api/token',
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    .then(getCsrfToken => {
      setCsrfTokenQuotation(getCsrfToken.data.token);
    })
    .catch (error => {
      console.error('Error retrieving CSRF token :', error);
    });
    axios.get(`https://samy-saberi.fr/api/get-assignment/${assignmentId}`,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    .then((response) => {
      setAssignmentObject(response.data);
    })
    reloadMessages(assignmentId);
    // axios.get(`https://samy-saberi.fr/api/get-messages-per-assignment/${assignmentId}`,
    //   {
    //     headers: {
    //       Authorization: `Bearer ${token}`
    //     }
    //   }
    // )
    // .then((response) => {
    //   setAllMessages(response.data);
    // })

    // return () => {
    //   console.log('Unregistering Events...');
    //   socket.off('connect');
    //   socket.off('onMessage');
    // };
  }, []);



  const reloadMessages = async (assignmentId: string) => {
    try {
      axios.get(`https://samy-saberi.fr/api/get-messages-per-assignment/${assignmentId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then((response) => {
        setAllMessages(response.data);
        if(response.data.status === 401 ) {
          navigate('/login')
        }
      })
      .catch(error => {
        alert('Cannot get messages:');
      })
    }
    catch(error: any) {
      setErrorGet(error.message);
      console.error('Cannot get assignments:', error);
    }
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => { 
    setOpen(false);
    setFormErrorsQuotation({description: '', nbday: '', amount: '' });
    setDescriptionQuotation('');
    setNbdayQuotation(0);
    setAmountQuotation(0);
  }

  const handleKeyUp = (event: { key: string; keyCode: number; }) => {
    if ((event.key === 'Enter' || event.keyCode === 13) && value.trim() !== '') {
      sendMessage();
    }
  };

  const sendMessage = async () => {
    try {
      if (decodedToken && decodedToken.roles.includes('FREELANCE')) {
        receiver = assignmentObject.company.id;
      } else {
        receiver = assignmentObject.freelance.id;
      }
      if (value.trim() !== '') {
        const escapedMessageValue = encodeURIComponent(value);
        const newMessage: Message = {
          content: escapedMessageValue,
          transmitter: decodedToken.userId,
          receiver: receiver.toString(),
          assignment: assignmentObject.id,
          timestamp: new Date(),
          csrfTokenMessaging: csrfTokenMessaging,
        };
        socket.emit('newMessage', newMessage);
        setValue('');
      }
    }
     catch (error) {
      console.error('Can t send message', error);
    }
  };

  const handleChangeFormDescriptionQuotation = (event: ChangeEvent<HTMLInputElement>) => {
    setDescriptionQuotation(event.target.value);
  }

  const handleChangeFormNbdayQuotation = (event: ChangeEvent<HTMLInputElement>) => {
    setNbdayQuotation(parseInt(event.target.value.replace(/[^\w\s]/gi, "")));
  }

  const handleChangeFormAmountQuotation = (event: ChangeEvent<HTMLInputElement>) => {
    setAmountQuotation(parseInt(event.target.value.replace(/[^\w\s]/gi, "")));
  }

  const sendQuotation = async (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    const errors =  {
      description: '',
      nbday: '',
      amount: ''
    }

    const encodeDescription = encodeURIComponent(descriptionQuotation).replace(/%20/g, ' ');

    setFormErrorsQuotation(errors);

    try {
        const headers = {
          'Content-Type': 'application/json',
          'x-csrf-token-quotation': csrfTokenQuotation,
          Authorization: `Bearer ${token}`
        };
        const response = await axios.post('https://samy-saberi.fr/api/create-quotation',
          {
            amount: amountQuotation,
            description: encodeDescription,
            nbDays: nbdayQuotation,
            freelance: decodedToken.userId,
            assignment: assignmentObject.id,
            company: assignmentObject.company.id,
            token: headers['x-csrf-token-quotation']
          },
          { headers }
        );
        setOpen(false);
        setFormErrorsQuotation({description: '', nbday: '', amount: '' });
        setDescriptionQuotation('');
        setNbdayQuotation(0);
        setAmountQuotation(0);
        setIsSubmitted(true);
        setTimeout(() => {
          setIsSubmitted(false);
        }, 3000);
    } catch (error) {
      console.error("Cannot create a quotation", error);
    }
  }

  return (
    <div>
      {isSubmitted && (
        <Alert sx={{zIndex: 2}} severity="success">Le devis a été envoyé avec succès !</Alert>
      )}
      <Grid container spacing={12}>
        <Grid item xs={12}>
          <Button href="/history-assignment">Retour</Button>
          {decodedToken && decodedToken.roles.includes('CLIENT') ? (
            assignmentObject && assignmentObject.freelance && assignmentObject.freelance.firstName && (
              <Typography variant='h4' textAlign={'center'} className='user-destinataire'>{assignmentObject.freelance.firstName} {assignmentObject.freelance.lastName}</Typography>
            )
          ) :
            assignmentObject && assignmentObject.company && assignmentObject.company.firstName && (
              <Typography variant='h4' textAlign={'center'} className='user-destinataire'>{assignmentObject.company.firstName} {assignmentObject.company.lastName}</Typography>
            )
          }
          {errorGet && (
            <Alert severity="error">Une erreur s'est produite, les messages ne peuvent s'afficher.</Alert>
          )}
          <List className='messageArea' style={{ height: '67vh', overflow: 'auto' }}>
            {messages.length === 0 && allMessages.length === 0 ? (
              <div>Commencez la discussion</div>
            ) : (
              <div>
                {allMessages && allMessages.map((listMessage, index) => (
                  <ListItem key={index}>
                    <Grid container>
                      <Grid item xs={12}>
                        <ListItemText 
                          primaryTypographyProps={{
                            align: listMessage.transmitter.id === decodedToken.userId ? 'right' : 'left',
                          }}
                          className={`chat-bubble ${listMessage.transmitter.id === decodedToken.userId ? 'user-bubble' : 'interlocutor-bubble'}`}
                          primary={<span>{decodeURIComponent(listMessage.content)}</span>}
                        >
                        </ListItemText>
                      </Grid>
                      <ListItemText 
                        secondaryTypographyProps={{
                          align: listMessage.transmitter.id === decodedToken.userId ? 'right' : 'left',
                        }}
                        secondary={moment(listMessage.timestamp).format('DD/MM/YYYY HH:mm')}
                      >
                      </ListItemText>
                    </Grid>
                  </ListItem>
                ))}
                {messages.map((msg, index) => (
                  <ListItem key={index}>
                    <Grid container>
                      <Grid item xs={12}>
                        <ListItemText 
                          primaryTypographyProps={{
                            align: msg.content.transmitter === decodedToken.userId ? 'right' : 'left',
                          }}
                          className={`chat-bubble ${msg.content.transmitter === decodedToken.userId ? 'user-bubble' : 'interlocutor-bubble'}`}
                          primary={<span>{decodeURIComponent(msg.content.content)}</span>}
                        >
                        </ListItemText>
                      </Grid>
                      <Grid item xs={12}>
                        <ListItemText 
                          secondaryTypographyProps={{
                            align: msg.content.transmitter === decodedToken.userId ? 'right' : 'left',
                          }}
                          secondary={moment(msg.content.timestamp).format('DD/MM/YYYY HH:mm')}
                        >
                        </ListItemText>
                      </Grid>
                    </Grid>
                  </ListItem>
                ))}
              </div>
            )}
          </List>
        </Grid>

        <Grid container>
          <Grid item xs={12}>
            <Divider />
          </Grid>
        </Grid>

        <Grid container style={{ position: 'fixed', bottom: 0, left: 0, right: 0, padding: '25px' }} spacing={3}>
        <Grid item xs={1}>
          {decodedToken && decodedToken.roles.includes('FREELANCE') && (
            <Fab aria-label="add" className='icon-send-quotation' sx={{backgroundColor: '#164E87', color: '#fff'}} onClick={handleClickOpen}><EuroIcon className='icon-send-quotation'/></Fab>
            )
          }
        </Grid>
          <Grid item xs={10}>
            <TextField
              id="standard-basic"
              variant="standard"
              placeholder="Entrer votre message"
              fullWidth
              margin="dense"
              type="text"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              onKeyUp={handleKeyUp}
            />
            <input type="hidden" name="x-csrf-token-messaging" value={csrfTokenMessaging} />
          </Grid>
          <Grid item xs={1}>
            <Fab aria-label="add" className='icon-send-message' disabled={value === ''} sx={{backgroundColor: '#164E87', color: '#fff', marginRight: '10%'}} onClick={sendMessage}><SendIcon className='icon-send-message' /></Fab>
          </Grid>
        </Grid>

        <Dialog open={open} onClose={handleClose}>
          <DialogTitle sx={{textAlign: 'center'}}>Envoyez le devis au client</DialogTitle>
          <DialogContent className='modal-box-form'>
          <Alert severity="info">Le client prendra connaissance de votre devis.</Alert>
            <TextField
              autoFocus
              margin="dense"
              id="outlined-textarea"
              label="Description"
              placeholder='Description de votre devis'
              type="text"
              multiline
              variant="standard"
              required
              error={!descriptionQuotation || descriptionQuotation.length < 3 || descriptionQuotation.length > 100}
              helperText='La description doit avoir entre 3 et 100 caractères.'
              value={descriptionQuotation}
              onChange={handleChangeFormDescriptionQuotation}
              style={{ width: '100%' }}
            />
            {formErrorsQuotation.description && <Typography variant="subtitle1" className='error-value'>{formErrorsQuotation.description}</Typography>}
            <TextField
              autoFocus
              margin="dense"
              id="outlined-textarea"
              label="Nombre de jour-homme"
              placeholder='Entrez le nombre de jour-homme'
              type="number"
              variant="standard"
              inputProps={{ min: 1 }}
              required
              error={!nbdayQuotation || typeof nbdayQuotation !== 'number'}
              helperText='Veuillez entrer un nombre.'
              onChange={handleChangeFormNbdayQuotation}
              style={{ width: '100%' }}
            />
            {formErrorsQuotation.nbday && <Typography variant="subtitle1" className='error-value'>{formErrorsQuotation.nbday}</Typography>}
            <TextField
              autoFocus
              margin="dense"
              id="outlined-textarea"
              label="Montant de votre mission"
              placeholder='Inscrivez votre montant'
              type="number"
              fullWidth
              variant="standard"
              inputProps={{ min: 1 }}
              required
              error={!amountQuotation || typeof amountQuotation !== 'number'}
              helperText='Veuillez entrer un montant.'
              onChange={handleChangeFormAmountQuotation}
            />
            {formErrorsQuotation.amount && <Typography variant="subtitle1" className='error-value'>{formErrorsQuotation.amount}</Typography>}
          </DialogContent>
          <DialogActions className='button-action'>
            <Button onClick={handleClose} variant="contained">Annuler</Button>
            <input type="hidden" name="x-csrf-token-quotation" value={csrfTokenQuotation} />
            <Button type="submit" disabled={descriptionQuotation === '' || nbdayQuotation === undefined || amountQuotation === undefined} variant="contained" onClick={sendQuotation}>Envoyer</Button>
          </DialogActions>
        </Dialog>
      </Grid>

    </div>

  );
}

export default Messagerie;
