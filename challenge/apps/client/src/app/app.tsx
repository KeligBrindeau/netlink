// eslint-disable-next-line @typescript-eslint/no-unused-vars
import Navbar from './containers/Navbar';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import { Routes } from 'react-router-dom';
import { AuthContextProvider } from './providers/authProvider';
import { Router } from './router/Router';
import DatasFreelanceProvider from './providers/datasFreelanceProvider';
import { useEffect } from 'react';
import jwt_decode from 'jwt-decode'

const stripePromise = loadStripe('pk_test_51NEBKpCy4jTzKN7NMwYiJx9k7ibALHKr3vQD0a0hZO9qz1gpwxPnqgOFJPFjq5W1FuBvyVt5syOKeFQpHcNZlDBI0000iuUqyQ');

export function App() {

  return (
    <div>
      <AuthContextProvider>
        <DatasFreelanceProvider>
          <Elements stripe={stripePromise}>
            <Router />
          </Elements>
        </DatasFreelanceProvider>
      </AuthContextProvider>

    </div>

  );
}
export default App;
