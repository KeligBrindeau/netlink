import { render } from '@testing-library/react';

import HistoryQuotation from './history-quotation';

describe('HistoryQuotation', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<HistoryQuotation />);
    expect(baseElement).toBeTruthy();
  });
});
