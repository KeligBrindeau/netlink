import React, { useContext, useEffect, useState } from 'react';
import styles from './history-quotation.module.scss';
import jwt_decode from 'jwt-decode';
import axios from 'axios';
import {
  Typography,
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel,
  TableContainer,
  Paper,
  Button,
  tableCellClasses,
  Tooltip,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Alert
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { AuthContext } from '../context/authContext';
import Chat from '../containers/chat/Chat';
import { useNavigate } from 'react-router-dom';

/* eslint-disable-next-line */
export interface HistoryQuotationProps { }

interface Quotations {
  id: string;
  freelance: string;
  assignmentId: string;
  assignment: string;
  description: string;
  amount: number;
  nbDays: number;
}

export function HistoryQuotation(props: HistoryQuotationProps) {
  const [acceptedQuotations, setAcceptedQuotations] = useState<Quotations[]>([]);
  const [errorGet, setErrorGet] = useState<string | null>(null);
  const [open, setOpen] = React.useState(false);
  const [getRefund, setGetRefund] = useState(false);
  const [openPayment, setOpenPayment] = useState(false);
  const [selectedId, setSelectedId] = useState('');
  const [selectedIdAssignment, setSelectedIdAssignment] = useState('');
  const { token } = useContext(AuthContext);
  const [decodedToken, setDecodedToken] = useState('');
  const [getPaymentIntent, setGetPaymentIntent] = useState('');
  const [getAssignmentRefund, setGetAssignmentRefund] = useState('');
  const [csrfToken, setCsrfToken] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    setDecodedToken(jwt_decode(token));
  }, [token]);

  useEffect(() => {
    if (decodedToken) {
      loadAcceptedQuotations();
      axios.get('https://samy-saberi.fr/api/token',
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then(getToken => {
          setCsrfToken(getToken.data.token);
        })
        .catch(error => {
          console.error('Erreur lors de la récupération du token CSRF :', error);
        });
    }
  }, [decodedToken]);

  const handleClickOpen = async (id: string, paymentIntent: string, assignmentId: string) => {
    setGetPaymentIntent(paymentIntent);
    setSelectedId(id);
    setGetAssignmentRefund(assignmentId);
    setOpen(true);
  };

  const handleClickOpenPayment = async (id: string, assignment ) => {
    setSelectedId(id);
    setSelectedIdAssignment(assignment);
    setOpenPayment(true);
  };

  const handleCloseModalPayment = () => {
    setOpenPayment(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleRedirectPayment = async (id: string | undefined, assignment) => {
    if(assignment.company.id !== decodedToken.userId) {
      navigate('/login');
    }
    else {
      window.location.href = `payment/${id}`;
    }
  }

  const handleCloseRefund = async (id: string | undefined, paymentIntent: string, assignmentId: string) => {
    try {
      await axios.post(`https://samy-saberi.fr/api/refund`,
        {
          payment_intent: paymentIntent
        },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then((response) => {
        axios.patch(`https://samy-saberi.fr/api/update-assignment-refund/${assignmentId}`,
          {
            isRefund: true
          },
          {
            headers: {
              'x-csrf-token-refund-assignment': csrfToken,
              Authorization: `Bearer ${token}`
            }
          }
          )
          setOpen(false);
          location.reload();
          setGetRefund(true);
          setTimeout(() => {
            setGetRefund(false);
          }, 5000);
        })
    }
    catch (error: any) {
      console.error('Cannot get assignment for refund:', error);
    }
  };

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const loadAcceptedQuotations = async () => {
    try {
      await axios.get(`https://samy-saberi.fr/api/get-accepted-quotation/${decodedToken.userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then((response) => {
          setAcceptedQuotations(response.data);
        })
    }
    catch (error: any) {
      setErrorGet(error.message);
      console.error('Cannot get accepted quotation:', error);
    }
  }

  return (
    <div>
      <Grid container spacing={12}>
        <Grid item xs={12}>
          <Typography variant='h5' textAlign={'center'} mb={5}>Mes devis en cours</Typography>
          {errorGet && (
            <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
          )}
          {getRefund && (
            <Alert severity="success">Le remboursement a été effectué avec succès. Vous le recevrez dans un délai de 2 à 3 jours.</Alert>
          )}
          {acceptedQuotations.length > 0 ? (
            <div>
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell align="center">N°</StyledTableCell>
                      <StyledTableCell align="center">Nom & prénom</StyledTableCell>
                      <StyledTableCell align="center">jour-homme - montant</StyledTableCell>
                      <StyledTableCell align="center">Description du devis</StyledTableCell>
                      {decodedToken && decodedToken.roles.includes('CLIENT') ? (
                        <StyledTableCell align="center">Actions</StyledTableCell>
                      ) :
                        <StyledTableCell align="center">Information</StyledTableCell>
                      }
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {acceptedQuotations.map((acceptedQuotation, acceptedQuotationIndex) => (
                      <StyledTableRow key={acceptedQuotation.id}>
                        <StyledTableCell align="center">{acceptedQuotationIndex + 1}</StyledTableCell>
                        {decodedToken && decodedToken.roles.includes('CLIENT') ? (
                          <StyledTableCell align="center">{acceptedQuotation.freelance.lastName} {acceptedQuotation.freelance.firstName}</StyledTableCell>
                        ) : (
                          <StyledTableCell align="center">{acceptedQuotation.company.lastName} {acceptedQuotation.company.firstName}</StyledTableCell>
                        )}
                        <StyledTableCell align="center">{acceptedQuotation.nbDays}jh - {acceptedQuotation.amount.toString().slice(0, -1)}€</StyledTableCell>
                        <StyledTableCell align="center">{decodeURIComponent(acceptedQuotation.description)}</StyledTableCell>
                        {decodedToken && decodedToken.roles.includes('CLIENT') && (
                          <StyledTableCell align="center">
                            {acceptedQuotation.assignment.updatedAt ? (
                              acceptedQuotation.assignment.isRefund === false ? (
                                <Tooltip title="Uniquement dans les 72 heures">
                                  <Button variant="outlined" style={{ color: '#164E87' }} onClick={() => handleClickOpen(acceptedQuotation.id, acceptedQuotation.assignment.paymentIntent, acceptedQuotation.assignment.id)}>Remboursement</Button>
                                </Tooltip>
                              ) : (
                                <Typography variant='body1' style={{ color: '#164E87' }} textAlign={'center'}>Remboursé</Typography>
                              )
                            ) :
                              (
                                <Tooltip title="Payer le freelance">
                                  <Button variant="contained" style={{ backgroundColor: '#164E87' }} onClick={() => handleClickOpenPayment(acceptedQuotation.id, acceptedQuotation)}>Paiment</Button>
                                  </Tooltip>
                              )}
                          </StyledTableCell>
                        )}

                        {decodedToken && decodedToken.roles.includes('FREELANCE') && (
                          acceptedQuotation.assignment.paymentIntent && acceptedQuotation.assignment.isRefund === false ? (
                            <StyledTableCell align="center" style={{ color: '#164E87' }}>Payé</StyledTableCell>
                          ) :
                            acceptedQuotation.assignment.isRefund === true ? (
                              <StyledTableCell align="center" style={{ color: '#164E87' }}>Remboursement demandé</StyledTableCell>
                            ) :
                              (
                                <StyledTableCell align="center" style={{ color: '#164E87' }}>En attente de paiement</StyledTableCell>
                              )
                        )}
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          ) : (
            <div>
              <Typography variant='h5' textAlign={'center'}>Pas de données</Typography>
            </div>
          )}
          {decodedToken && decodedToken.roles.includes('CLIENT') && (
            <div style={{ display: "flex", justifyContent: "center", marginTop: "2%" }}>
              <Button variant="text" color="primary" href="/quotation">Voir les devis en attente de validation</Button>
            </div>
          )}
          {token && (
            <Chat />
          )}
        </Grid>
      </Grid>

      {openPayment && (
        <Dialog
          open={openPayment}
          onClose={handleCloseModalPayment}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            Passer au paiment
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Vous serez redirigé vers la page de paiment stripe
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <input type="hidden" value={selectedId} />
            <input type="hidden" value={selectedIdAssignment} />
            <Button onClick={handleCloseModalPayment}>Annuler</Button>
            <Button autoFocus variant="contained" style={{ backgroundColor: '#164E87' }} onClick={() => handleRedirectPayment(selectedId, selectedIdAssignment)}>Payer</Button>
          </DialogActions>
        </Dialog>
      )}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Demander le remboursement
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Voulez-vous vraiment demander le remboursement ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <input type="hidden" value={selectedId} />
          <input type="hidden" value={getPaymentIntent} />
          <input type="hidden" value={getAssignmentRefund} />
          <input type="hidden" name="x-csrf-token-refund-assignment" value={csrfToken} />
          <Button onClick={handleClose}>Annuler</Button>
          <Button onClick={() => handleCloseRefund(selectedId, getPaymentIntent, getAssignmentRefund)} autoFocus variant="contained" style={{ backgroundColor: '#164E87' }}>Rembourser</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default HistoryQuotation;
