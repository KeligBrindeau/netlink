import React, { useContext, useEffect, useMemo, useState } from 'react';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Rating, Slide, TextField, Typography } from '@mui/material';
import Footer from '../containers/Footer';
import Chat from '../containers/chat/Chat';
import axios from 'axios';
import { SearchContext } from './SearchContext';
import { useNavigate } from 'react-router-dom';
import '../styles/homePage/index.css';
import { AuthContext } from '../context/authContext';
import sha1 from 'js-sha1';
import { TransitionProps } from '@mui/material/transitions';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});


const Home = () => {

  const { setSearchData } = useContext(SearchContext);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchLocation, setSearchLocation] = useState('');
  const [searchTermError, setSearchTermError] = React.useState(false);
  const [searchLocError, setSearchLocError] = React.useState(false);
  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const [open, setOpen] = React.useState(false);
  const [ratingValue, setRatingValue] = useState(null);

  let visitRecorded = false;

  var clickQueue: {clickId?: string; page: string; element: string;}[] = [];

  var OSName = "Unknown OS";
  var userAgent = window.navigator.userAgent;

  if (userAgent.indexOf("Win") !== -1) {
    OSName = "Windows";
  } else if (userAgent.indexOf("Mac") !== -1) {
    OSName = "MacOS";
  } else if (userAgent.indexOf("Linux") !== -1) {
    OSName = "Linux";
  } else if (/iPad|iPhone|iPod/.test(userAgent)) {
    OSName = "iOS";
  } else if (/Android/.test(userAgent)) {
    OSName = "Android";
  }

  function generateUniqueId(timestamp: any) {
    const hash = sha1(timestamp.toString());
    const uniqueId = hash.substring(0, 8); 
  
    return uniqueId;
  }

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) {
      return parts.pop().split(";").shift();
    }
  }

  function setCookie(name, value, hours) {
    var expires = "";
    if (hours) {
      var date = new Date();
      date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
      expires = "; expires=" + date.toString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
  }

  var uniqueId = getCookie('uniqueId');

  if(uniqueId === undefined){
    uniqueId = generateUniqueId(new Date().getTime());
    setCookie('uniqueId', uniqueId, 1);

    axios.post('https://samy-saberi.fr/api/saveOs',{
      osType: OSName
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données save os:', error);
    });

    var startVisit = Date.now();
  }

  const search = (keywords: string) =>{
    axios.get('https://samy-saberi.fr/api/search', {
      params:{
        keywords: keywords
      }
    })
      .then(response => {
        setSearchData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }


  const handleSubmit = (event: any) => {
    event.preventDefault();

    if (searchTerm.trim() === '' || searchTerm.length > 80){
      setSearchTermError(true); 
      return;
    }
    else if(searchLocation.length > 80){
      setSearchLocError(true);
      return;
    }
    setSearchTermError(false); 

    const keywords = encodeURIComponent(searchTerm).replace(/%20/g, ' ') + " " + encodeURIComponent(searchLocation).replace(/%20/g, ' ');
    search(keywords);
    navigate('/search');
  };
  

  useEffect(() => {
    const searchButton = document.querySelector('#submit-homepage');
    searchButton.addEventListener('click', (event) => {
      const timestamp = new Date().getTime();
      const uniqueId = generateUniqueId(timestamp);
      const page = "home"
      const element = "searchButton"

      addClickToQueue(uniqueId, page, element);
  
    });
  })
 

  function addClickToQueue(clickId: string, page: string, element: string) {
    const click = {
      page: page,
      element: element,
    };
    clickQueue.push(click);
  }


  function saveClick(){

    if(clickQueue.length > 0){
      axios.post('https://samy-saberi.fr/api/saveClick', clickQueue)
      .then(response => {
        clickQueue = [];
      })
      .catch(error => {
      console.error('Erreur lors de l\'envoie des données:', error);
      });
    }
  }
 
  setInterval(saveClick, 10000);

  let inactivityTimeout;

  function resetInactivityTimer() {
    clearTimeout(inactivityTimeout);
    inactivityTimeout = setTimeout(handleInactivity, 900000);
  }

  function handleInactivity() {
    if(visitRecorded === false && startVisit){
      recordVisit()
    }
    
  }

  document.addEventListener('mousemove', resetInactivityTimer);
  document.addEventListener('click', resetInactivityTimer);

  

  window.addEventListener("beforeunload", (event) => {

    if (startVisit && visitRecorded === false) {
      recordVisit();
    }
  })

  visitRecorded = false;

  function recordVisit() {
    const endTime = Date.now();
    const visitDuration = Math.round((endTime - startVisit) / 1000);
    visitRecorded = true;

    axios.post('https://samy-saberi.fr/api/saveVisit',{
      page: "home",
      visitDuration: visitDuration,
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données:', error);
    });
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const submitSurvey = () => {

    axios.post('https://samy-saberi.fr/api/saveSurvey', {
      rate: ratingValue,
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données:', error);
    });

  }
  
  return (
    <Box className="home">
      <img src="./home/geometry1.svg" className="home--geometry1"/>

      <Box className="home--introduction">
        <h1>Netlink, la plateforme qui vous met en  <strong>relation</strong> avec des Freelances pour propulser vos projets.</h1>
        <img src="./home/team-esgi.jpg" className="home--teamImg"/>
      </Box>

      <Box className="home--search">
        <h1 className="home--search--title--mobile">Rechercher un freelance</h1>
        <form className="home--search--form" noValidate autoComplete="off" onSubmit={handleSubmit}>
          <TextField 
            id="outlined-basic" 
            label='Essayez "développeur front-end", "html", "java"' 
            variant="outlined" 
            className={searchTermError ? 'error' : 'HomeSearch-skill'}
            onChange={(event) => setSearchTerm(event.target.value)}
            required
          />
          <TextField 
            id="outlined-basic" 
            label="Lieu de la mission (ex: Paris, Lyon)" 
            variant="outlined" 
            className={searchLocError ? 'error' : 'HomeSearch-city'} 
            onChange={(event) => setSearchLocation(event.target.value)} 
          />
          <Button type="submit" variant="contained" className="homeSearch-submit" id="submit-homepage">Trouver un freelance</Button>
        </form>
      </Box>

      <Box className="home--community">
        <h1 className="home--community--title">Netlink c'est avant tout une communauté</h1>
        <p className="home--community--accroche">C'est aussi collaborer en toute simplicité</p>

        <Box className="home--community--icons">
          <Box className="home--community--icons--boxes">
            <img src="./home/handshake.png" className="home--community--icons--hands"/>
            <p className="home--community--icons--title">50K entreprises </p>
            <p className="home--community--icons-desc">À la recherche de freelances</p>
          </Box>

          <Box className="home--community--icons--boxes">
            <img src="./home/rocket.png" className="home--community--icons--rocket"/>
            <p className="home--community--icons--title">+40K freelances</p>
            <p className="home--community--icons-desc">Aux multiples compétences</p>
          </Box>

          <Box className="home--community--icons--boxes">
            <img src="./home/conversation.png" className="home--community--icons--conv"/>
            <p className="home--community--icons--title">1 solution dédiée</p>
            <p className="home--community--icons-desc">Pensée et conçue pour collaborer</p>
          </Box>
        </Box>
      </Box>
      <button className="survey" onClick={handleClickOpen}>&#9998;</button>
        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleClose}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Enquête de satisfaction"}</DialogTitle>
          <form noValidate autoComplete='off' onSubmit={submitSurvey}>
            <DialogContent>
              <DialogContentText id="alert-dialog-slide-description">
                Votre avis compte ! Partagez-le et aidez-nous à améliorer votre expérience sur notre site.
              </DialogContentText>
              <Typography component="legend" className="survey-legend">Votre note :</Typography>
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                <Rating 
                  name="no-value" 
                  value={ratingValue} 
                  onChange={(event, value) => setRatingValue(value)}
                  className="survey-stars"
                />
              </div>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Annuler</Button>
              <Button type="submit">Soumettre</Button>
            </DialogActions>
          </form>
        </Dialog>
      {token && (
        <Chat />
      )}
      <Footer />
    </Box>
  );
};

export default Home;
