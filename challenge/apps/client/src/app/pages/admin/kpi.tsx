import { useContext, useEffect, useRef, useState } from 'react';
import axios from 'axios';
import "../../styles/admin/kpi.scss";
import HeatMap from 'heatmap-ts'
import Chart from 'chart.js/auto';


export function Kpi() {

    const [heatMapData, setHeatmapData] = useState([]);
    const [clickData, setClickData] = useState([]);
    const [osData, setOsData] = useState([]);
    const [visitData, setVisitData] = useState([]);
    const [surveyData, setSurveyData] = useState([]);

    const imageRef = useRef<HTMLImageElement>(null);
    const chartRef = useRef();
    const canvas = document.getElementById('stat-search') as HTMLCanvasElement;
    var chartInstance = useRef(null);
    var osChartInstance = useRef(null);
    const osChartRef = useRef();
   
    useEffect(() => {
      axios.get('https://samy-saberi.fr/api/mouseEvent')
      .then(response => {
        setHeatmapData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des données:', error);
      });

      axios.get('https://samy-saberi.fr/api/getClick')
      .then(response => {
        setClickData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des données:', error);
      });

      axios.get('https://samy-saberi.fr/api/getOs')
      .then(response => {
        setOsData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des données:', error);
      });

      axios.get('https://samy-saberi.fr/api/getVisit')
      .then(response => {
        setVisitData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des données:', error);
      });

      axios.get('https://samy-saberi.fr/api/getSurvey')
      .then(response => {
        setSurveyData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des données:', error);
      });

    }, [])
    

    const screenWidth = window.innerWidth;
    const screenHeight = window.innerHeight;

    const minValue = 0;
    const maxValue = Math.max(screenWidth, screenHeight);

    const heatMap = new HeatMap({
      container: document.getElementById('kpi-container'),
      maxOpacity: .6,
      radius: 90,
      blur: 0.6,
    })

    heatMap.setData({
      max: 100,
      min: 2,
      data: []
    })

    heatMapData.forEach((dataPoint) => {
      const { start_x, end_x, start_y, end_y, duration } = dataPoint;

      const x = (start_x + end_x) / 2;
      const y = (start_y + end_y) / 2;

      const value = 30;

      heatMap.addData({ x, y, value });
    })

    heatMap.repaint();


    
    if(chartInstance.current != null){
      chartInstance.current.destroy();
    }

    if(canvas instanceof HTMLCanvasElement ){
      const ctx = canvas.getContext('2d');

      const clickNavbarSearchButton = clickData.filter(obj => obj.page !== 'home');
      const clickHomeSearchButton = clickData.filter(obj => obj.page !== 'navbar');
      const totalClick = clickData.length;

      const percentClickNavbar = (clickNavbarSearchButton.length / totalClick) * 100;
      const percentClickHome = (clickHomeSearchButton.length / totalClick) * 100;

      const data = {
        labels: ['Taux de click Navbar: '+ percentClickNavbar.toFixed(2)+'%', 'Taux de click HomePage: '+ percentClickHome.toFixed(2)+'%'],
        datasets: [{
          data: [percentClickNavbar, percentClickHome],
          backgroundColor: ['blue', 'red']
        }]
      };

      chartInstance.current = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: {
          responsive: true,
          plugins: {
            legend: {
              display: true,
              labels: {
                font: {
                  size: 20
                }
              }
            }
          }
        }
      });
      
    }

    var countByOsType = {};

    osData.forEach(function(item) {
      if (countByOsType[item.osType]) {
        countByOsType[item.osType]++;
      } else {
        countByOsType[item.osType] = 1;
      }
    });

    var totalElements = osData.length;

    var osPercent = {};

    for (var osType in countByOsType) {
      if (countByOsType.hasOwnProperty(osType)) {
        var count = countByOsType[osType];
        var percentage = (count / totalElements) * 100;
        osPercent[osType] = percentage.toFixed(2) + '%';
      }
    }

    var osChartCanvas = document.getElementById('stat-os') as HTMLCanvasElement;
    

    if(osChartInstance.current != null){
      osChartInstance.current.destroy();
    }

    if (osChartCanvas instanceof HTMLCanvasElement){
      var osChartCanvasCtx = osChartCanvas.getContext('2d');

      var osChartLabels = Object.keys(osPercent);
      var osChartData = Object.values(osPercent).map(parseFloat);

      var xAxisLabels = osChartLabels.map(function(label, index) {
        var percentage = osChartData[index].toFixed(2);
        return label + ' (' + percentage + '%)';
      });

      var backgroundColors = ['rgba(75, 192, 192, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'];

      osChartInstance.current = new Chart(osChartCanvasCtx, {
        type: 'bar',
        data: {
          labels: xAxisLabels,
          datasets: [{
            label: 'Pourcentage d\'os utilisés',
            data: osChartData,
            backgroundColor: backgroundColors,
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 0.5
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
              max: 100,
              ticks: {
                callback: function(value) {
                  return value + '%';
                }
              }
            },
          },
          plugins: {
            tooltip: {
              callbacks: {
                label: function(context) {
                  return context.label + ': ' + context.parsed.y + '%';
                }
              }
            }
          }
        }
      });
    }

    interface VisitData {
      id: string;
      visitorId: string;
      page: string;
      visitDuration: number;
    }
    
    function calculateAverageDuration(visits: VisitData[]): Map<string, number | string> {
      const pageDurations: Map<string, number[]> = new Map();
    
      for (const visit of visits) {
        const { page, visitDuration } = visit;
    
        if (!pageDurations.has(page)) {
          pageDurations.set(page, []);
        }
    
        const durations = pageDurations.get(page);
        durations?.push(visitDuration);
      }
    
      const averageDurations: Map<string, number | string> = new Map();
      for (const [page, durations] of pageDurations.entries()) {
        const totalDuration = durations.reduce((sum, duration) => sum + duration, 0);
        const averageDuration = parseFloat((totalDuration / durations.length).toFixed(2));
    
        if (averageDuration < 60) {
          averageDurations.set(page, `${averageDuration} sec`);
        } else {
          const averageInMinutes = Math.floor(averageDuration / 60);
          const remainingSeconds = Math.round(averageDuration % 60);
          if (remainingSeconds === 0) {
            averageDurations.set(page, `${averageInMinutes} minute(s)`);
          } else {
            averageDurations.set(page, `${averageInMinutes} minute(s) ${remainingSeconds} seconde(s)`);
          }
        }
      }
    
      return averageDurations;
    }
    
    const averageDurations = calculateAverageDuration(visitData);

    const data = Array.from(averageDurations.entries()).map(([page, duration]) => {
      return { page, duration };
    });

    const sommeRates = surveyData.reduce((somme, objet) => somme + objet.rate, 0);

    const nombreObjets = surveyData.length;

    const tauxSatisfactionMoyen = ((sommeRates / (nombreObjets * 100)) * 100).toFixed(2);


  return (
    <>
      <div className="header-analytics">
        <img src="./admin/line-chart.png" />
        <h1 className="analytics-title">Analyses du site</h1>
        <img src="./admin/camembert.png" />
      </div>

      <h1 className="os-title">Systèmes d'exploitation utilisés :</h1>
      <canvas id="stat-os" ref={osChartRef} className=""></canvas>

      <h1 className="duration-title">Durée moyenne de visite par page :</h1>

      <div className="duration-container">
        {data.map((item) => (
          <div key={item.page}>
            <p className="duration-value">{item.duration}</p>
            <p className="duration-page">Page: {item.page}</p>
          </div>
        ))}
      </div>

      <div className="satisfaction-container">
        <h1 className="satisfaction-title">Taux de satisfaction :</h1>
        <p>{tauxSatisfactionMoyen} %</p>
      </div>

      <h1 className="click-title">Taux de clicks recherche Navbar/Homepage :</h1>
      <canvas id="stat-search" ref={chartRef} className="searchClickGraph"></canvas>
    
      <h1 className="heatmap-title">HEATMAP :</h1>
      <div className="kpi-container" id="kpi-container">
        <img
          ref={imageRef}
          src="./heatmap-freelance.png"
          alt="Image"
        />
      </div>
        
    </>
    
  );
}

export default Kpi;
