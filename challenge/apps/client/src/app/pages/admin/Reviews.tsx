import styles from './quotations.module.scss';
import React, { useContext, useEffect, useState } from 'react';
import jwt_decode from 'jwt-decode';
import {
    Alert,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
    tableCellClasses
} from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';
// import '../styles/quotations/index.css';
import { styled } from '@mui/material/styles';
import { AuthContext } from '../../context/authContext';

interface Client {
    lastName: string,
    firstName: string
}

interface Reviews {
    id: string;
    description: string;
    ratedUser: Client;
    ratingUser: Client;
    rate: number;
    isDisplayed: boolean;
}

export function Reviews() {
    const [reviews, setReviews] = useState<Reviews[]>([]);
    const [indexReview, setIndexReview] = useState<number>(0);
    const [getApproveReview, setGetApproveReview] = useState<Reviews>();
    const [getRejectReview, setGetRejectReview] = useState<Reviews>();
    const [openApprovementModal, setOpenApprovementModal] = useState(false);
    const [openRejectModal, setOpenRejectModal] = useState(false);
    const [error, setError] = useState<string | null>(null);
    const [errorGet, setErrorGet] = useState<string | null>(null);
    const [csrfToken, setCsrfToken] = useState('');
    const { token, user } = useContext(AuthContext);

    useEffect(() => {
        if (user) {
            loadReviews();
        }
    }, []);

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));
    
    const loadReviews = async () => {
        try {
            await axios.get(`https://samy-saberi.fr/api/admin/reviews`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
                .then((response) => {
                    setReviews(response.data.datas);
                })
        }
        catch (error: any) {
            setErrorGet(error.message);
            console.error('Cannot get reviews:', error);
        }
    }

    const handleClickOpenApprovement = async (review: Reviews, index: number) => {
        setOpenApprovementModal(true);
        setGetApproveReview(review);
        setIndexReview(index);
    };

    const handleClickOpenRejection = async (review: Reviews, index: number) => {
        setOpenRejectModal(true);
        setGetRejectReview(review);
        setIndexReview(index);
    };

    const handleCloseApprovement = () => {
        setOpenApprovementModal(false);
    }

    const handleCloseReject = () => {
        setOpenRejectModal(false);
    }

    const approveReview = async (id: string | undefined) => {
        try {
            await axios.patch(`https://samy-saberi.fr/api/admin/publish-review/${id}`,
                {
                    isDisplayed: true
                },
                {
                    headers: {
                        // 'x-csrf-token-approve-quotation': csrfToken,
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            setOpenApprovementModal(false);
            loadReviews();
        }
        catch (error: any) {
            setError(error.message);
            console.error('Cannot approve quotation:', error);
        }
    }

    const rejectReview = async (id: string | undefined) => {
        console.log("ID " + id)
        try {
            await axios.delete(
                `https://samy-saberi.fr/api/admin/delete-review/${id}`,
                {
                    headers: {
                        // 'x-csrf-token-reject-quotation': csrfToken,
                        Authorization: `Bearer ${token}`
                    }
                }
            );

            setOpenRejectModal(false);
            loadReviews();
        }
        catch (error: any) {
            setError(error.message);
            console.error('Cannot reject quotation:', error);
        }
    }
    
    return (
        <div>
            <Grid container spacing={12}>
                <Grid item xs={12}>
                    <Typography variant='h5' textAlign={'center'} mb={5}>Commentaires</Typography>
                    {errorGet && (
                        <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
                    )}
                    {reviews.length > 0 ? (
                        <Paper sx={{ width: '100%', mb: 2 }}>
                            <TableContainer>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell align="center">Numéro</StyledTableCell>
                                            <StyledTableCell align="center">Commentaire</StyledTableCell>
                                            <StyledTableCell align="center">Note</StyledTableCell>
                                            <StyledTableCell align="center">Freelance</StyledTableCell>
                                            <StyledTableCell align="center">Client</StyledTableCell>
                                            <StyledTableCell align="center">Actions</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {reviews.map((review, reviewIndex) => (
                                            <StyledTableRow key={review.id}>
                                                <StyledTableCell align="center">{reviewIndex + 1}</StyledTableCell>
                                                <StyledTableCell align="center">{review.description}</StyledTableCell>
                                                <StyledTableCell align="center">{review.rate} étoiles </StyledTableCell>
                                                <StyledTableCell align="center">{review.ratedUser.firstName} {review.ratedUser.lastName} </StyledTableCell>
                                                <StyledTableCell align="center">{review.ratingUser.firstName} {review.ratingUser.lastName} </StyledTableCell>
                                                <StyledTableCell align="center">
                                                    <Tooltip title="Supprimer">
                                                        <IconButton color="error" aria-label="close" onClick={() => handleClickOpenRejection(review, reviewIndex + 1)}>
                                                            <CloseIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title="Publier">
                                                        <IconButton color="success" aria-label="check" onClick={() => handleClickOpenApprovement(review, reviewIndex + 1)}>
                                                            <CheckIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                </StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Paper>
                    ) : (
                        <div>
                            <Typography variant='h5' textAlign={'center'}>Pas de données</Typography>
                        </div>
                    )}
                </Grid>
            </Grid>
            <Dialog open={openApprovementModal}>
                <DialogTitle sx={{ textAlign: 'center' }}>Êtes-vous sûr de vouloir publiler le commentaire n° {indexReview} ?</DialogTitle>
                <DialogContent className='modal-box-quotation-pending'>
                    {error && (
                        <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
                    )}
                </DialogContent>
                <DialogActions className='button-action-quotation-pending'>
                    <Button onClick={handleCloseApprovement} color="inherit" variant="contained">Supprimer</Button>
                    {/* <input type="hidden" name="x-csrf-token-approve-quotation" value={csrfToken} /> */}
                    <Button type="submit" variant="contained" color="success" onClick={() => approveReview(getApproveReview?.id)}>Valider</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openRejectModal}>
                <DialogTitle sx={{ textAlign: 'center' }}>Êtes-vous sûr de vouloir supprimer le commentaire N° {indexReview} ?</DialogTitle>
                <DialogContent className='modal-box-quotation-pending'>
                    {error && (
                        <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
                    )}
                </DialogContent>
                <DialogActions className='button-action-quotation-pending'>
                    <Button onClick={handleCloseReject} color="inherit" variant="contained">Annuler</Button>
                    {/* <input type="hidden" name="x-csrf-token-reject-quotation" value={csrfToken} /> */}
                    <Button type="submit" variant="contained" color="error" onClick={() => rejectReview(getRejectReview?.id)}>Supprimer</Button>
                </DialogActions>
            </Dialog>
            
        </div>
    );
}

export default Reviews;
