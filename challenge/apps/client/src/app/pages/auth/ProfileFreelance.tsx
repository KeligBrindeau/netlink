import React, { useState, useContext, useEffect } from 'react';
import {
    Container,
    Grid,
    TextField,
    Button,
    Snackbar,
    FormControl,
    InputLabel,
    Select,
    Alert,
    Avatar,
    FormControlLabel,
    Checkbox,
    MenuItem
} from '@mui/material';
import AvailabilityStatus from '../../containers/profile/AvailabilityStatus';
import ProfileAlertSection from '../../containers/profile/ProfileAlert';
import { AuthContext } from '../../context/authContext';
import axios from 'axios';
import { DatasFreelanceContext } from '../../context/datasFreelanceContext';
import useUserData from './useUserData';
import { UserType } from '../../types/auth';

const ProfileFreelance = () => {
    const { skills, cities, jobs } = useContext(DatasFreelanceContext);

    const { user, token } = useContext(AuthContext);
    const userData = useUserData(user?.id);
    const [error, setError] = useState("");
    const [success, setSuccess] = useState("");


    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [birthdate, setBirthdate] = useState('');
    const [profilePicture, setProfilePicture] = useState('');
    const [hourlyRate, setHourlyRate] = useState('');
    const [description, setDescription] = useState('');
    const [job, setJob] = useState('');
    const [city, setCity] = useState('');
    const [skillsUser, setSkillsUser] = useState('');
    const [status, setStatus] = useState(userData?.status);

    const [selectedFile, setSelectedFile] = useState<File | null>(null);

    useEffect(() => {
        if (userData) {
            const urlImg = getUrlImg(userData);

            setFirstName(userData.firstName);
            setLastName(userData.lastName);
            setEmail(userData.email);
            setBirthdate(userData.birthDate);
            setDescription(userData.description);
            setJob(userData.job);
            setCity(userData.city);
            setSkillsUser(userData.skills);
            setHourlyRate(userData.hourlyRate);
            setProfilePicture(urlImg);
        }
    }, [userData]);

    if (!userData) {
        return <div>Chargement...</div>;
    }

    function base64ToArrayBuffer(base64: string) {
        const binaryString = window.atob(base64);
        const len = binaryString.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; ++i) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes.buffer;
    }

    const getUrlImg = (userData: UserType) => {
        if (userData.picture) {
            const img = base64ToArrayBuffer(userData.picture);
            const imgBlob = new Blob([img]);
            const imgUrl = URL.createObjectURL(imgBlob);
            return imgUrl;
        } else {
            return userData.picture;
        }
    };

    const handleSkillsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value, checked } = event.target;
        setSkillsUser((prevSkills) => {
            if (checked) {
                return [...prevSkills, value];
            } else {
                return prevSkills.filter((skill) => skill !== value);
            }
        });
    };

    const handleSubmit = async (event: React.FormEvent) => {
        const updatedUser = {
            id: user.id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            birthDate: birthdate,
            hourlyRate: hourlyRate,
            description: description,
            job: job,
            city: city,
            skills: skillsUser,
            status: status
        };

        if (description === "") updatedUser.description = null;
        if (birthdate === "") updatedUser.birthDate = null;

        event.preventDefault();
        try {
            const response = await axios.patch('https://samy-saberi.fr/api/update-user-freelance', updatedUser, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });

            const data = response.data;
            if (data.status === 200) {

                setSuccess(data.message);
            } else if (data.status === 500) {
                setError('Il y a eu un un problème durant la mise à jours de l\'utilisateur');
            } else {
                setError(data.message);
            }
        } catch (error) {
            console.error;
        }

    };

    const handleCloseSnackbar = () => {
        setError('');
        setSuccess('');
    };

    const handleStatusChange = (newStatus) => {
        setStatus(newStatus);
    };

    const handleProfilePictureChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {

            const reader = new FileReader();
            reader.onload = () => {
                const dataURL = reader.result as string;
                setProfilePicture(dataURL);
            };
            reader.readAsDataURL(file);
            setSelectedFile(file);
        }
    };

    const uploadProfilePicture = async () => {
        const formData = new FormData();
        if (selectedFile) {
            formData.append('file', selectedFile);
        }
        formData.append('id', user.id)

        const dataProfile = await axios.post('https://samy-saberi.fr/api/upload-img', formData, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'multipart/form-data',
            }
        });
        if (dataProfile.data.status === 200) {
            setSuccess(dataProfile.data.message)
        } else {
            setError(dataProfile.data.message);
        }
    }

    return (
        <Container>
            <AvailabilityStatus isAvailable={userData.status} onStatusChange={handleStatusChange} />
            <ProfileAlertSection hasNoDescription={!userData.description} hasNoProfilePicture={!userData.picture} />

            <div style={{ textAlign: 'center', margin: '20px auto' }}>
                <label htmlFor="profile-picture-upload">
                    <input
                        id="profile-picture-upload"
                        type="file"
                        accept="image/*"
                        style={{ display: 'none' }}
                        onChange={handleProfilePictureChange}
                    />
                    <Avatar
                        alt="Profile Picture"
                        src={profilePicture}
                        style={{ width: 150, height: 150, cursor: 'pointer', margin: '20px auto' }}
                    />
                </label>
                <Button
                    variant="contained"
                    component="label"
                    onClick={uploadProfilePicture}
                >
                    Changer l'image
                </Button>
            </div>

            <Grid container justifyContent="center" spacing={2}>
                <Grid item xs={12} md={6}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                label="Prénom"
                                variant="outlined"
                                fullWidth
                                value={firstName}
                                onChange={(e) => setFirstName(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Nom"
                                variant="outlined"
                                fullWidth
                                value={lastName}
                                onChange={(e) => setLastName(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Email"
                                variant="outlined"
                                fullWidth
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                type="password"
                                label="Mot de passe"
                                variant="outlined"
                                fullWidth
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                type="date"
                                label={birthdate ? 'Date de naissance' : ''}
                                variant="outlined"
                                fullWidth
                                value={birthdate}
                                onChange={(e) => {
                                    setBirthdate(e.target.value);
                                }}
                            />
                        </Grid>

                    </Grid>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FormControl variant="outlined" fullWidth style={{ marginBottom: '1rem' }}>
                                <InputLabel>Métier</InputLabel>
                                <Select
                                    value={job == null ? '' : job}
                                    // value={job}
                                    onChange={(e) => setJob(e.target.value)}
                                    label="Métier"
                                >
                                    {jobs.map(job => (
                                        <MenuItem key={job} value={job}>{job}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl variant="outlined" fullWidth>
                                <InputLabel>Ville</InputLabel>
                                <Select
                                    value={city == null ? '' : city}
                                    // value={city}
                                    onChange={(e) => setCity(e.target.value)}
                                    label="Ville"
                                >
                                    {cities.map(city => (
                                        <MenuItem key={city} value={city}>{city}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>

                        <Grid item xs={12}>
                            <p>Compétences :</p>
                            {skills.map((skill) => (
                                <FormControlLabel
                                    key={skill}
                                    control={
                                        <Checkbox
                                            name="skills"
                                            value={skill == null ? '' : skill}
                                            // value={skill}
                                            checked={skillsUser.includes(skill)}
                                            onChange={handleSkillsChange}
                                        />
                                    }
                                    label={skill}
                                />
                            ))}
                        </Grid>

                        <Grid item xs={12}>
                            <TextField
                                type="number"
                                label="Taux horaire"
                                variant="outlined"
                                fullWidth
                                value={hourlyRate}
                                inputProps={{
                                    max: 5000,
                                    min: 125,
                                }}
                                onChange={(e) => setHourlyRate(Number(e.target.value))}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        label="Description"
                        variant="outlined"
                        fullWidth
                        multiline
                        rows={8}
                        value={description}
                        inputProps={{
                            maxLength: 2000
                        }}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </Grid>
            </Grid>

            <div style={{ display: 'flex', justifyContent: 'center', marginTop: 20 }}>
                <Button variant="contained" color="primary" onClick={handleSubmit}>
                    Mettre à jour
                </Button>
            </div>

            <Snackbar open={success !== ""} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert severity="success" onClose={handleCloseSnackbar}>
                    {success}
                </Alert>
            </Snackbar>

            <Snackbar open={error !== ""} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert severity="error" onClose={handleCloseSnackbar}>
                    {error}
                </Alert>
            </Snackbar>
        </Container>
    );
};

export default ProfileFreelance;
