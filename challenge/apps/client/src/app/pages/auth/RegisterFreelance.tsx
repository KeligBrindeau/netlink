import React, { useState, ChangeEvent, FormEvent, useContext } from 'react';
import {
    TextField,
    Checkbox,
    FormControlLabel,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Button,
    Container,
    Grid,
    Snackbar,
    Alert,
    SelectChangeEvent
} from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { DatasFreelanceContext } from '../../context/datasFreelanceContext';

interface FormData {
    email: string;
    password: string;
    lastName: string;
    firstName: string;
    jobs: string[];
    skills: string[];
    hourlyRate: number;
    cities: string[];
}

interface FormDataToSubmit {
    email: string;
    password: string;
    lastName: string;
    firstName: string;
    job: string;
    skills: string[];
    hourlyRate: number;
    city: string;
}

const RegisterFreelance: React.FC = () => {
    const { skills, cities, jobs } = useContext(DatasFreelanceContext);
    const initialFormState: FormData = {
        email: '',
        password: '',
        lastName: '',
        firstName: '',
        jobs: jobs,
        skills: [],
        hourlyRate: 125,
        cities: cities
    };

    const [formData] = useState<FormData>(initialFormState);

    const formStateToSubmit: FormDataToSubmit = {
        email: '',
        password: '',
        lastName: '',
        firstName: '',
        job: formData.jobs[0],
        skills: [],
        hourlyRate: 125,
        city: formData.cities[0]
    };
    const [formDataToSubmit, setFormDataToSubmit] = useState<FormDataToSubmit>(formStateToSubmit)

    const [error, setError] = useState("");

    const handleChange = <T extends keyof FormDataToSubmit>(field: T) => (
        event: React.ChangeEvent<
            HTMLInputElement | HTMLTextAreaElement | { value: unknown }
        >
    ) => {
        let value: FormDataToSubmit[T];
        if ('value' in event.target) {
            value = event.target.value as FormDataToSubmit[T];
        } else {
            value = (event.target as { value: FormDataToSubmit[T] }).value;
        }
        setFormDataToSubmit((prevData) => ({
            ...prevData,
            [field]: value,
        }));
    };

    const handleCloseSnackbar = () => {
        setError('');
    };

    const navigate = useNavigate();

    const handleSkillsChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value, checked } = event.target;
        setFormDataToSubmit((prevData) => {
            if (checked) {
                return {
                    ...prevData,
                    skills: [...prevData.skills, value]
                };
            } else {
                return {
                    ...prevData,
                    skills: prevData.skills.filter((skill) => skill !== value)
                };
            }
        });
    };

    const handleTabsChange = <T extends keyof FormDataToSubmit>(field: T) => (
        event: SelectChangeEvent<string>
    ) => {
        const value = event.target.value;
        setFormDataToSubmit((prevData) => ({
            ...prevData,
            [field]: value,
        }));
    };


    const handleSubmit = async (event: FormEvent) => {
        event.preventDefault();
        try {
            const response = await axios.post("https://samy-saberi.fr/api/register-freelance", {
                
                    ...formDataToSubmit,
                    hourlyRate: parseInt(formDataToSubmit.hourlyRate)
                
            });
            const data = response.data;
            if (data.status === 201) {
                navigate('/login', { state: { successMessage: data.message } });
            } else if (data.status === 500) {
                setError('Il y a eu un un problème durant la création du compte');
            } else {
                setError(data.message)
            }
        }
        catch (error) {
            console.error;
        }
    };

    return (
        <Container maxWidth="lg" style={{ display: 'flex', alignItems: 'center', height: '100vh' }}>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <FormControl variant="outlined" fullWidth style={{ marginBottom: '1rem' }}>
                            <InputLabel>Métier</InputLabel>
                            <Select
                                value={formDataToSubmit.job}
                                onChange={handleTabsChange('job')}
                                label="Métier"
                            >
                                {jobs.map(job => (
                                    <MenuItem key={job} value={job}>{job}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>

                        <TextField
                            label="Taux horaire"
                            name="hourlyRate"
                            type="number"
                            value={formDataToSubmit.hourlyRate}
                            onChange={handleChange('hourlyRate')}
                            required
                            fullWidth
                            style={{ marginBottom: '1rem' }}
                            inputProps={{
                                max: 5000,
                                min: 125,
                            }}
                            className="register-freelance-hourlyRate"
                        />

                        <FormControl variant="outlined" fullWidth style={{ marginBottom: '1rem' }}>
                            <InputLabel>Ville</InputLabel>
                            <Select
                                value={formDataToSubmit.city}
                                onChange={handleTabsChange('city')}
                                label="Ville"
                            >
                                {cities.map(city => (
                                    <MenuItem key={city} value={city}>{city}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>

                        <div style={{ marginBottom: '1rem' }}>
                            <p>Compétences :</p>
                            {skills.map((skill) => (
                                <FormControlLabel
                                    key={skill}
                                    control={
                                        <Checkbox
                                            name="skills"
                                            value={skill}
                                            checked={formDataToSubmit.skills.includes(skill)}
                                            onChange={handleSkillsChange}
                                            className="register-freelance-skills"
                                        />
                                    }
                                    label={skill}
                                />
                            ))}
                        </div>

                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            label="Nom"
                            name="lastName"
                            value={formDataToSubmit.lastName}
                            onChange={handleChange('lastName')}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                            className="register-freelance-lastname"
                        />
                        <TextField
                            label="Prénom"
                            name="firstName"
                            value={formDataToSubmit.firstName}
                            onChange={handleChange('firstName')}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                            className="register-freelance-firstname"
                        />
                        <TextField
                            label="Email"
                            name="email"
                            value={formDataToSubmit.email}
                            onChange={handleChange('email')}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                            className="register-freelance-email"
                        />
                        <TextField
                            label="Mot de passe"
                            type="password"
                            name="password"
                            value={formDataToSubmit.password}
                            onChange={handleChange('password')}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                            className="register-freelance-password"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button type="submit" variant="contained" color="primary" className="submit-freelance-register">
                            Inscription
                        </Button>
                    </Grid>
                </Grid>
            </form>

            <Snackbar open={!!error} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} severity='error'>
                    {error.includes('\n') ? (
                        error.split('\n').map((err, index) => (
                            <div key={index}>{err}</div>
                        ))
                    ) : (
                        <div>{error}</div>
                    )}

                </Alert>
            </Snackbar>
        </Container>

    );
};

export default RegisterFreelance;
