import { useState, useEffect, useCallback, useContext } from 'react';
import axios from 'axios';
import { AuthContext } from '../../context/authContext';
import { UserType } from '../../types/auth';

const useUserData: UserType = (userId : string) => {

    const {token} = useContext(AuthContext)

    const [userData, setUserData] = useState(null);

    const fetchUserData = useCallback(async () => {
        try {
            const response = await axios.get(`https://samy-saberi.fr/api/me/${userId}`,
                {
                    headers: {Authorization: `Bearer ${token}`}
                }
            );
            setUserData(response.data.user);
        } catch (error) {
            console.error('Erreur lors de la récupération des données utilisateur :', error);
        }
    }, []);

    useEffect(() => {
        fetchUserData();
    }, [fetchUserData]);
    return userData;
};

export default useUserData;