import React, { DragEvent, ChangeEvent, useState, useEffect } from 'react';
import './kanban.scss';
import axios from 'axios';
import { Button } from '@mui/material';

interface Task {
  name: string;
  category: string;
}

interface DragAndDropAppState {
  tasks: Task[];
}

const Kanban: React.FC = () => {

    const [tasks, setTasks] = useState<Task[]>([]);
    const assignmentId = window.location.pathname.split('/')[2];
    const [loading, setLoading] = useState(true);

    const getTickets = async () => {
      try{
        axios.get(`https://samy-saberi.fr/api/getTickets/${assignmentId}`)
        .then(response => {
          setTasks(response.data);
        })
        .catch(error => {
          console.error('Erreur lors de la récupération des tickets:', error);
        });
      } catch (error) {
        console.error("erreur");
      } finally{
        setLoading(false);
      }
        
    }

    useEffect(() => {
        getTickets();
    }, []);

    if (loading) {
      return <div>Loading...</div>;
    }

  const onDragOver = (ev: DragEvent<HTMLDivElement>) => {
    ev.preventDefault();
  };

  const onDragStart = (ev: DragEvent<HTMLDivElement>, name: string) => {
    ev.dataTransfer.setData("id", name);
  };

  const onDrop = (ev: DragEvent<HTMLDivElement>, cat: string) => {
    const id = ev.dataTransfer.getData("id");
    let taskToUpdate;

    let updatedTasks = tasks.map(task => {
      if (task.id === id) {
        task.statut = cat;
        taskToUpdate = task.statut;
      }
      return task;
    });

    axios.patch(`https://samy-saberi.fr/api/updateTicket/${id}`, 
    {
        statut: taskToUpdate
    })
    .catch(error => {
        console.error('Erreur lors de la modification du ticket:', error);
    });

    setTasks(updatedTasks);
  };

  const handleKeyPress = (ev: React.KeyboardEvent<HTMLInputElement>) => {
    if (ev.key === "Enter" && ev.currentTarget.value !== "") {
      const newTask: Task = {
        description: ev.currentTarget.value,
        statut: "todo"
      };

      axios.post("https://samy-saberi.fr/api/saveTicket", 
      {
        title: "title", 
        description: ev.currentTarget.value,
        statut: "todo",
        assignment: assignmentId
      })
      .then(response => {
          getTickets();
      })
      .catch(error => {
          console.error('Erreur lors de la modification du ticket:', error);
      });

      setTasks(prevTasks => [...prevTasks, newTask]);

      ev.currentTarget.value = "";
    }
  };

  const renderedTasks: { [category: string]: JSX.Element[] } = {
    todo: [],
    working: [],
    complete: [],
    trash: []
  };

  if(tasks.length > 0){
    tasks.forEach((t, index) => {
        renderedTasks[t.statut].push(
          <div
            className="item-containerKanban"
            key={index}
            draggable
            onDragStart={e => onDragStart(e, t.id)}
          >
            {t.description}
          </div>
        );
      });
  }

  return (
    <div>
      <Button href="/history-assignment">Retour</Button>
      <div id='background-image'></div>
      <div className="containerKanban">
        <div
          className="drop-area"
          onDragOver={e => onDragOver(e)}
          onDrop={e => onDrop(e, "todo")}
        >
          <h1 className="kanbanTitle">Todo</h1>
          {renderedTasks.todo}
        </div>
        <div
          className="drop-area"
          onDragOver={e => onDragOver(e)}
          onDrop={e => onDrop(e, "working")}
        >
          <h1 className="kanbanTitle">Working</h1>
          {renderedTasks.working}
        </div>
        <div
          className="drop-area"
          onDragOver={e => onDragOver(e)}
          onDrop={e => onDrop(e, "complete")}
        >
          <h1 className="kanbanTitle">Complete</h1>
          {renderedTasks.complete}
        </div>
      </div>
      <div>
        <input
          onKeyPress={e => handleKeyPress(e)}
          className="input-kanban"
          type="text"
          placeholder="Description du ticket"
        />

        <div
          className="trash-drop"
          onDrop={e => onDrop(e, "trash")}
          onDragOver={e => onDragOver(e)}
        >
          Drop ici pour supprimer
        </div>
      </div>
    </div>
  );
};

export default Kanban;