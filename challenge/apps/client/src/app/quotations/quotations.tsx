import styles from './quotations.module.scss';
import React, { useContext, useEffect, useState } from 'react';
import jwt_decode from 'jwt-decode';
import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  tableCellClasses
} from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';
import '../styles/quotations/index.css';
import { styled } from '@mui/material/styles';
import { AuthContext } from '../context/authContext';


export interface QuotationsProps {}

interface Quotations {
  id: string;
  freelanceId: string;
  assignmentId: string;
  description: string;
  amount: number;
  nbDays: number;
}

export function Quotations(props: QuotationsProps) {
  const [quotations, setQuotations] = useState<Quotations[]>([]);
  const [indexQuotation, setIndexQuotation] = useState<number>(0);
  const [getApproveQuotation, setGetApproveQuotation] = useState<Quotations>();
  const [getRejectQuotation, setGetRejectQuotation] = useState<Quotations>();
  const [openApprovementModal, setOpenApprovementModal] = useState(false);
  const [openRejectModal, setOpenRejectModal] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [errorGet, setErrorGet] = useState<string | null>(null);
  const [csrfToken, setCsrfToken] = useState('');
  const { token } = useContext(AuthContext);
  const [decodedToken, setDecodedToken] = useState('');

  useEffect(() => {
    setDecodedToken(jwt_decode(token));
  }, [token]);

  useEffect(() => {
    if (decodedToken) {
      loadQuotations();
      axios.get('https://samy-saberi.fr/api/token',
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(getToken => {
          setCsrfToken(getToken.data.token);
      })
      .catch (error => {
        console.error('Erreur lors de la récupération du token CSRF :', error);
      });
    }
  }, [decodedToken]);

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const loadQuotations = async () => {
    try {
      await axios.get(`https://samy-saberi.fr/api/get-quotations-per-company/${decodedToken.userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then((response) => {
          setQuotations(response.data);
        })
    }
    catch(error: any) {
      setErrorGet(error.message);
      console.error('Cannot get assignments:', error);
    }
  }

  const handleClickOpenApprovement = async (quotation: Quotations, index: number) => {
    setOpenApprovementModal(true);
    setGetApproveQuotation(quotation);
    setIndexQuotation(index);
  };

  const handleClickOpenRejection = async (quotation: Quotations, index: number) => {
    setOpenRejectModal(true);
    setGetRejectQuotation(quotation);
    setIndexQuotation(index);
  };

  const handleCloseApprovement = () => { 
    setOpenApprovementModal(false);
  }

  const handleCloseReject = () => { 
    setOpenRejectModal(false);
  }

  const approveQuotation = async (id: string | undefined) => {
    try {
      await axios.patch(`https://samy-saberi.fr/api/update-approve-quotation/${id}`,
        {
          status: true
        },
        {
          headers: {
            'x-csrf-token-approve-quotation': csrfToken,
            Authorization: `Bearer ${token}`
          }
        }
      )
      setOpenApprovementModal(false);
      loadQuotations();
    }
    catch(error: any) {
      setError(error.message);
      console.error('Cannot approve quotation:', error);
    }
  }

  const rejectQuotation = async (id: string | undefined) => {
    try {
      await axios.patch(`https://samy-saberi.fr/api/update-reject-quotation/${id}`,
        {
          status: false
        },
        {
          headers: {
            'x-csrf-token-reject-quotation': csrfToken,
            Authorization: `Bearer ${token}`
          }
        }
      )
      setOpenRejectModal(false);
      loadQuotations();
    }
    catch(error: any) {
      setError(error.message);
      console.error('Cannot reject quotation:', error);
    }
  }

  return (
    <div>
      <Grid container spacing={12}>
        <Grid item xs={12}>
          <Typography variant='h5' textAlign={'center'} mb={5}>Mes devis en attente</Typography>
          {errorGet && (
            <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
          )}
            {quotations.length > 0 ? (
              <Paper sx={{ width: '100%', mb: 2 }}>
                <TableContainer>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <StyledTableCell align="center">N°</StyledTableCell>
                        <StyledTableCell align="center">Nom & Prénom</StyledTableCell>
                        <StyledTableCell align="center">jour-homme - montant</StyledTableCell>
                        <StyledTableCell align="center">Description devis</StyledTableCell>
                        <StyledTableCell align="center">Actions</StyledTableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {quotations.map((quotation, quotationIndex) => (
                        <StyledTableRow key={quotation.id}>
                          <StyledTableCell align="center">{quotationIndex+1}</StyledTableCell>
                          <StyledTableCell align="center">{quotation.freelance.lastName} {quotation.freelance.firstName}</StyledTableCell>
                          <StyledTableCell align="center">{quotation.nbDays}jh - {quotation.amount.toString().slice(0, -1)}€</StyledTableCell>
                          <StyledTableCell align="center">{decodeURIComponent(quotation.description)}</StyledTableCell>
                          <StyledTableCell align="center">
                            <Tooltip title="Refuser">
                              <IconButton color="error" aria-label="close" onClick={() => handleClickOpenRejection(quotation, quotationIndex+1)}>
                                <CloseIcon/>
                              </IconButton>
                            </Tooltip>
                            <Tooltip title="Accepter">
                              <IconButton color="success" aria-label="check" onClick={() => handleClickOpenApprovement(quotation, quotationIndex+1)}>
                                <CheckIcon/>
                              </IconButton>
                            </Tooltip>
                          </StyledTableCell>
                        </StyledTableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Paper>
          ) : (
            <div>
              <Typography variant='h5' textAlign={'center'}>Pas de données</Typography>
            </div>
          )}
          <div style={{display: "flex", justifyContent: "center", marginTop: "2%"}}>
            <Button variant="text" color="primary" href="/history-quotation">Voir les devis en cours</Button>
          </div>
        </Grid>
      </Grid>
      <Dialog open={openApprovementModal}>
        <DialogTitle sx={{textAlign: 'center'}}>Êtes-vous sûr de vouloir accepter le devis N° {indexQuotation} ?</DialogTitle>
        <DialogContent className='modal-box-quotation-pending'>
          <Alert severity="info">Vous pouvez par la suite passer au paiement</Alert>
          {error && (
            <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
          )}
        </DialogContent>
        <DialogActions className='button-action-quotation-pending'>
          <Button onClick={handleCloseApprovement} color="inherit" variant="contained">Annuler</Button>
          <input type="hidden" name="x-csrf-token-approve-quotation" value={csrfToken} />
          <Button type="submit" variant="contained" color="success" onClick={() => approveQuotation(getApproveQuotation?.id)}>Valider</Button>
        </DialogActions>
      </Dialog>

        <Dialog open={openRejectModal}>
          <DialogTitle sx={{textAlign: 'center'}}>Êtes-vous sûr de vouloir refuser le devis N° {indexQuotation} ?</DialogTitle>
          <DialogContent className='modal-box-quotation-pending'>
            <Alert severity="info">Vous pouvez tout de même négocier via la messagerie</Alert>
            {error && (
              <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
            )}
          </DialogContent>
          <DialogActions className='button-action-quotation-pending'>
            <Button onClick={handleCloseReject} color="inherit" variant="contained">Annuler</Button>
            <input type="hidden" name="x-csrf-token-reject-quotation" value={csrfToken} />
            <Button type="submit" variant="contained" color="error" onClick={() => rejectQuotation(getRejectQuotation?.id)}>Refuser</Button>
          </DialogActions>
        </Dialog>
    </div>
  );
}

export default Quotations;
