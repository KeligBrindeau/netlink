import { render } from '@testing-library/react';

import Quotations from './quotations';

describe('Quotations', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Quotations />);
    expect(baseElement).toBeTruthy();
  });
});
