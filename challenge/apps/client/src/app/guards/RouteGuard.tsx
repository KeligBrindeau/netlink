import { Navigate, Outlet } from "react-router-dom";

interface GuardedRouteProps {
	/**
	 * Permission check for route
	 * @default false
	 */
	isRouteAccessible: boolean;
	/**
	 * Route to be redirected to
	 * @default '/'
	 */
	redirectRoute: string;
}   

export const RouteGuard = ({ isRouteAccessible, redirectRoute }: GuardedRouteProps): JSX.Element => 
    isRouteAccessible ? <Outlet/> : <Navigate to={redirectRoute} replace />;
