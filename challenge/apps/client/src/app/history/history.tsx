import styles from './history.module.scss';
import React, { useContext, useEffect, useState } from 'react';
import jwt_decode from 'jwt-decode';
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  tableCellClasses,
  Dialog,
  DialogTitle,
  Slide,
  DialogContent,
  DialogContentText,
  Rating,
  DialogActions,
  TextField,
  Snackbar
} from '@mui/material';
import Chat from '../containers/chat/Chat';
import { styled } from '@mui/material/styles';
import axios from 'axios';
import SendIcon from '@mui/icons-material/Send';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { AuthContext } from '../context/authContext';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { TransitionProps } from "@mui/material/transitions";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

/* eslint-disable-next-line */
export interface HistoryProps { }

interface Assignment {
  id: number;
  company: string;
  freelance: string;
  description: string;
  file: string;
}

export function History(props: HistoryProps) {
  const [assignments, setAssignments] = useState<Assignment[]>([]);
  const [errorGet, setErrorGet] = useState<string | null>(null);
  const { token } = useContext(AuthContext);
  const [decodedToken, setDecodedToken] = useState('');

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const handleCloseSnackbar = () => {
    setError('');
    setSuccess('');
  };

  useEffect(() => {
    setDecodedToken(jwt_decode(token));
  }, [token]);

  useEffect(() => {
    if (decodedToken) {
      loadAcceptedAssignment();
    }
  }, [decodedToken])

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const loadAcceptedAssignment = async () => {
    try {
      await axios.get(`https://samy-saberi.fr/api/get-accepted-assignment/${decodedToken.userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
        .then((response) => {
          setAssignments(response.data);
        })
    }
    catch (error: any) {
      setErrorGet(error.message);
      console.error('Cannot get assignments:', error);
    }
  }

  function base64ToArrayBuffer(base64: string) {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; ++i) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
  }
  const [openModal, setOpenModal] = useState(false);
  // const [currentAssignment, setCurrentAssignment] = useState(null);
  const [commentValue, setCommentValue] = useState('');
  const [ratingValue, setRatingValue] = useState(0);
  const [assignment, setAssignment] = useState<Assignment>();

  const handleOpenModal = (assignment: Assignment) => {
    setOpenModal(true);
    setAssignment(assignment)
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    setCommentValue('');
    setRatingValue(null);
  };

  const handlePdfPreview = (assignment: Assignment) => {
    const pdf = base64ToArrayBuffer(assignment.file);
    const pdfBlob = new Blob([pdf], { type: 'application/pdf' });
    const pdfUrl = URL.createObjectURL(pdfBlob);
    window.open(pdfUrl);
  };

  const submitReview = async (event, assignment: Assignment) => {
    event.preventDefault();
    try {
      const response = await axios.post(`https://samy-saberi.fr/api/create-review`, {
        rate: ratingValue,
        description: commentValue,
        ratedUser: assignment.freelance.id,
        ratingUser: assignment.company.id
      },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )

      const data = response.data;
      if (data.status === 201) {
        handleCloseModal()
        setSuccess(data.message);
      } else {
        handleCloseModal()
        setError(data.message);
      }

    } catch (error) {
      console.error
    }
  }

  return (
    <div>
      <Grid container spacing={12}>
        <Grid item xs={12}>
          <Typography variant='h5' textAlign={'center'} mb={5}>Mes missions en cours</Typography>
          {errorGet && (
            <Alert severity="error">Une erreur s'est produite, veuillez réessayer ultérieurement.</Alert>
          )}
          {assignments.length > 0 ? (
            <div>

              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell align="center">N°</StyledTableCell>
                      <StyledTableCell align="center">Nom</StyledTableCell>
                      <StyledTableCell align="center">Prénom</StyledTableCell>
                      <StyledTableCell align="center">Description mission</StyledTableCell>
                      <StyledTableCell align="center">Action</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {assignments.map((assignment, assignmentsIndex) => (
                      <StyledTableRow key={assignment.id}>
                        <StyledTableCell align="center">{assignmentsIndex + 1}</StyledTableCell>
                        {decodedToken && decodedToken.roles.includes('CLIENT') ? (
                          <>
                            <StyledTableCell align="center">{assignment.freelance.lastName}</StyledTableCell>
                            <StyledTableCell align="center">{assignment.freelance.firstName}</StyledTableCell>
                          </>
                        ) :
                          <>
                            <StyledTableCell align="center">{assignment.company.lastName}</StyledTableCell>
                            <StyledTableCell align="center">{assignment.company.firstName}</StyledTableCell>
                          </>
                        }
                        <StyledTableCell align="center">{decodeURIComponent(assignment.description)}</StyledTableCell>
                        <StyledTableCell align="center">
                          <Tooltip title="Visualiser">
                            <IconButton color="default" aria-label="eye" onClick={() => handlePdfPreview(assignment)}>
                              <RemoveRedEyeIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Accéder à la messagerie">
                            <IconButton color="primary" aria-label="chat" href={`/messaging/${assignment.id}`}>
                              <SendIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Notez le freelance">
                            <IconButton color="primary" aria-label="star" onClick={() => handleOpenModal(assignment)}>
                              <StarBorderIcon />

                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Voir le projet">
                            <Button href={`/kanban/${assignment.id}`}>Voir le projet</Button>
                        </Tooltip>
                      </StyledTableCell>


                      </StyledTableRow>

                    ))}
                  </TableBody>


                </Table>
              </TableContainer>
            </div>
          ) :
            <div>
              <Typography variant='h5' textAlign={'center'}>Pas de données</Typography>
            </div>
          }
          {decodedToken && decodedToken.roles.includes('FREELANCE') && (
            <div style={{ display: "flex", justifyContent: "center", marginTop: "2%" }}>
              <Button variant="text" color="primary" href="/assignment">Voir les missions en attente de validation</Button>
            </div>
          )}
        </Grid>
      </Grid>

      <Dialog
        open={openModal}
        TransitionComponent={Transition}
        keepMounted

        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Enquête de satisfaction"}</DialogTitle>
        <form noValidate autoComplete="off" onSubmit={(event) => submitReview(event, assignment)}>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Votre avis compte ! Partagez-le et aidez-nous à améliorer votre expérience sur notre site.
            </DialogContentText>
            <TextField
              autoFocus
              multiline
              rows={4}
              margin="dense"
              id="comment"
              label="Commentaire"
              type="text"
              fullWidth
              value={commentValue}
              onChange={(event) => setCommentValue(event.target.value)}
            />
            <Typography component="legend" className="survey-legend">
              Votre note :
            </Typography>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <Rating
                name="no-value"
                value={ratingValue}
                onChange={(event, value) => setRatingValue(value)}
                className="survey-stars"
              />
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseModal}>Annuler</Button>
            <Button type="submit">Soumettre</Button>
          </DialogActions>
        </form>
      </Dialog>

      <Snackbar open={success !== ""} autoHideDuration={6000} onClose={handleCloseSnackbar}>
        <Alert severity="success" onClose={handleCloseSnackbar}>
          {success}
        </Alert>
      </Snackbar>

      <Snackbar open={!!error} autoHideDuration={6000} onClose={handleCloseSnackbar}>
        <Alert onClose={handleCloseSnackbar} severity='error'>
          {error.includes('\n') ? (
            error.split('\n').map((err, index) => (
              <div key={index}>{err}</div>
            ))
          ) : (
            <div>{error}</div>
          )}

        </Alert>
      </Snackbar>
    </div>

  );
}

export default History;
