export interface AuthContextProps {
    children: React.ReactNode;
}
export interface UserType {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    roles: string[];
    picture: string,
    birthDate: Date | null | string;
    status: boolean | null;
    skills: string[];
    job: string;
    description: string | null;
    city: string;
    hourlyRate: number | null;
    createdAt: string;
    updatedAt: string;
}

export type UserContextType = Omit<UserType, 'createdAt' | 'updatedAt'>

export type DecodedTokenType = Omit<UserContextType, 'id'> & {
    userId: string
    iat: number
    exp: number
}
