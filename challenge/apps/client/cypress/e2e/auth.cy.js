describe('Register as a client', () => {
    it('Vérifie que l\'inscription en tant que client fonctionne', () => {
      cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/');
      cy.get('.connexion').click();
      cy.get('.register').click();
      cy.get('.register-client').click();
      cy.get('.register-client-lastname').type('user1');
      cy.get('.register-client-firstname').type('user1');
      cy.get('.register-client-email').type('user1@gmail.com');
      cy.get('.register-client-password').type('password');
      cy.get('.submit-client-register').click();
      cy.url().should('eq', 'https://netlink-git-main-keligbrindeau.vercel.app/login');
    })
})

describe('Register as a freelance', () => {
  it('Vérifie que l\'inscription en tant que freelance fonctionne', () => {
    cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/');
    cy.get('.connexion').click();
    cy.get('.register').click();
    cy.get('.register-freelance').click();
    cy.get('.register-freelance-lastname').type('user2');
    cy.get('.register-freelance-firstname').type('user2');
    cy.get('.register-freelance-email').type('user2@gmail.com');
    cy.get('.register-freelance-password').type('password');
    // cy.get('.register-freelance-city').type('paris');
    // cy.get('.register-freelance-job').type('front-end');
    cy.get('.submit-freelance-register').click();
    cy.get('input[type="checkbox"][value="php"]').check();
    cy.get('input[type="checkbox"][value="javascript"]').check();
    cy.get('.submit-freelance-register').click();
    cy.url().should('eq', 'https://netlink-git-main-keligbrindeau.vercel.app/login');
  })
})

describe('Login', () => {
  it('Vérifie que la connexion fonctionne', () => {
    cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/');
    cy.get('.connexion').click();
    cy.get('.login-email').type('user1@gmail.com');
    cy.get('.login-password').type('password');
    cy.get('.login-submit').click();
    cy.get('.login-error').should('exist');
  })
})