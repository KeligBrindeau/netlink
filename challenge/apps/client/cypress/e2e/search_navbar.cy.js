describe('Navbar Search job + skill', () => {
    it('Vérifie que la recherche depuis la navbar fonctionne', () => {
        cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/')
        cy.get('.desktopButtonSearch').click()
        cy.get('.skill-inputSearch').type('java, developpeur front-end')
        cy.get('.findFreelanceForm--submit').click()

        cy.get('.freelancerJob-search').should('contain', 'front-end')
        cy.get('a[class*="more-"]').eq(0).click();
    })
})

describe('Navbar Search job + city', () => {
    it('Vérifie que la recherche depuis la navbar fonctionne avec la ville', () => {
        cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/')
        cy.get('.desktopButtonSearch').click()
        cy.get('.skill-inputSearch').type('developpeur front-end')
        cy.get('.city-inputSearch').type('rennes')
        cy.get('.findFreelanceForm--submit').click()

        cy.get('.freelancerJob-search').should('contain', 'front-end')
        cy.get('a[class*="more-"]').eq(0).click();
    })
})

