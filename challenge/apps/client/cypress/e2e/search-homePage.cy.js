describe('Home Search job + skill', () => {
    it('Vérifie que la recherche depuis la homePage fonctionne avec une compétence', () => {
        cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/')
        cy.get('.HomeSearch-skill').type('java')
        cy.get('.homeSearch-submit').click()

        let foundJavaButton = false;

        cy.get('.card-skills').each(($button) => {
            const buttonText = $button.text();

            if (buttonText.includes('java')) {
                foundJavaButton = true;
                return false;
            }
        }).then(() => {
            expect(foundJavaButton).to.be.true;
        });
        cy.get('a[class*="more-"]').eq(1).click();
    });
});

describe('Home Search Job', () => {
    it('Vérifie que la recherche depuis la homePage fonctionne avec un job', () => {
        cy.visit('https://netlink-git-main-keligbrindeau.vercel.app/')
        cy.get('.HomeSearch-skill').type('développeur front-end')
        cy.get('.homeSearch-submit').click()
        cy.get('.freelancerJob-search').should('contain', 'front-end')
        cy.get('a[class*="more-"]').eq(1).click()
    });
});