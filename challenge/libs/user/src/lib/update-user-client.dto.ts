import { IsBoolean, IsEmail, IsOptional, IsString, IsArray, Length, IsDateString, isNumber, IsInt, Min, Max, IsNotEmpty, ArrayNotEmpty  } from 'class-validator';

export class UpdateClientDto {
    @IsString()
    readonly id: string;

    @IsEmail(undefined, {
        message: 'Veuillez fournir une adresse e-mail valide'
    })
    email?: string;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    @Length(8, 255, { message: 'Le mot de passe doit comporter 8 caractères minimum.' })
    password?: string;
    
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @Length(2, 20, { message: 'Le nom de famille doit comporter entre 2 et 20 caractères.' })
    lastName?: string;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    @Length(2, 20, { message: 'Le prénom doit comporter entre 2 et 20 caractères.' })
    firstName?: string;

    @IsDateString({}, {message: "La date n'est pas au bon format"})
    @IsOptional()
    birthDate?: Date;

}