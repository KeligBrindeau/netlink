import { IsBoolean, IsEmail, IsOptional, IsString, IsArray, Length, IsDateString, isNumber, IsInt, Min, Max, IsNotEmpty, ArrayNotEmpty  } from 'class-validator';

export class UpdateFreelanceDto {
    @IsString()
    readonly id: string;

    @IsEmail(undefined, {
        message: 'Veuillez fournir une adresse e-mail valide'
    })
    email?: string;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    @Length(8, 255, { message: 'Le mot de passe doit comporter 8 caractères minimum.' })
    password?: string;
    
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @Length(2, 20, { message: 'Le nom de famille doit comporter entre 2 et 20 caractères.' })
    lastName?: string;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    @Length(2, 20, { message: 'Le prénom doit comporter entre 2 et 20 caractères.' })
    firstName?: string;

    @IsBoolean()
    @IsOptional()
    status?: boolean;

    @IsArray()
    @ArrayNotEmpty({message: "Veuillez sélectionner au moins une compétence"})
    @IsNotEmpty()
    @IsOptional()
    skills?: Array<string>;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    city?: string;

    @IsDateString({}, {message: "La date n'est pas au bon format"})
    @IsOptional()
    birthDate?: Date;

    @IsInt({message: 'Veuillez saisir un nombre entier compris entre 125 et 5000 pour le taux horaire'})
    @Min(125)
    @Max(5000)
    @IsNotEmpty()
    @IsOptional()
    hourlyRate?: number;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    job?: string;

    @IsString()
    @IsOptional()
    @Length(100, 2000, { message: 'La description doit comporter entre 100 et 2000 caractères' })
    description?: string;

}