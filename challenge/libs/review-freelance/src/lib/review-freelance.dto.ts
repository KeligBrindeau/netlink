import {
  IsInt,
  IsNotEmpty,
  IsString,
  Length,
  IsBoolean
} from "class-validator";
import { UserEntity } from "apps/messaging/src/app/user.entity";

export class CreateReviewFreelanceDto {
  readonly id?: string;

  @IsNotEmpty()
  @IsInt()
  rate?: number;

  @IsString()
  @Length(3, 200, { message: 'Le commentaire doit comporter entre 3 et 200 caractères.' })
  description?: string;

  @IsString()
  ratedUser?: UserEntity

  @IsString()
  ratingUser?:UserEntity

  @IsBoolean()
  isDisplayed: boolean = false;

}
