export class CreateMessageDto {
    content?: string;
    transmitter!: () => string;
    receiver!: () => string;
    assignment!: () => string;
    timestamp!: Date;
}