export class SurveyDto {
    visitorId?: string;
    page?: string;
    rate?: number;
}