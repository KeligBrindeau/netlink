export class VisitDto {
    visitorId?: string;
    page?: string;
    visitDuration?: number;
}