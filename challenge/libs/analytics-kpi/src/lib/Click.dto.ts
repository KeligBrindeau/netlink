export class ClickDto {
    clickId?: string;
    page?: string;
    element?: string;
}