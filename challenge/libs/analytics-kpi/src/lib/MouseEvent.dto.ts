export class MouseEventDto {
   start_x?: number;
   start_y?: number;
   end_x?: number;
   end_y?: number;
   type?: string;
   duration?: number;
}