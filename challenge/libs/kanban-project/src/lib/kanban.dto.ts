export class KanbanDto {
    title?: string;
    description?: string;
    statut?: string;
    assignmentId?: string;
}