import {
    IsBoolean,
    IsDefined,
    IsOptional,
    IsString,
    Length
} from "class-validator";
import { UserEntity } from "../../../../apps/authentification/src/app/user.entity";
export class CreateAssignmentDto {
    id!: string;

    @IsDefined()
    @IsString()
    @Length(3, 100)
    description!: string;

    status: boolean | null = null;
    file!: string;
    company?: UserEntity;
    freelance?: UserEntity;
    isRefund?: boolean;
}

export class UpdateAssignmentDto {
    status!: boolean;
}

export class UpdateAssignmentRefundDto {
    isRefund!: boolean;
}