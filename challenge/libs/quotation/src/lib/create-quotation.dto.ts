import {
    IsDefined,
    IsInt,
    IsNotEmpty,
    IsNumber,
    IsString,
    Length
} from "class-validator";
import { AssignmentEntity } from "../../../../apps/messaging/src/app/assignment.entity";
import { UserEntity } from "apps/messaging/src/app/user.entity";
export class CreateQuotationDto {
    id?: string;

    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    @IsInt()
    amount?: number;

    status: boolean | null = null;

    @IsDefined()
    @IsString()
    @Length(3, 100)
    description?: string;

    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    @IsInt()
    nbDays?: number;

    freelance?: UserEntity;
    company?: UserEntity;
    assignment?: AssignmentEntity;
}

export class UpdateQuotationDto {
    status!: boolean;
}