#!/bin/bash

microservices=("gateway" "authentification" "messaging" "analytics" "review" "payment" "search")

for service in "${microservices[@]}"
do
  (cd "./apps/$service" && nx serve --args="seed") &
done

wait